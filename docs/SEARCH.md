# Search

Rideshare implements search capability for Rideshares, Airports, Cities and Locations using 
[Elasticsearch](https://www.elastic.co/).

1. By default it attempts to be intelligent by limiting cities and airports to the same continent on 
   which an event is taking place.  This may not be exactly what you want if you are having
   an event that people are driving to or from a different continent. Yes, that could happen.

1. You can specify what countries you want included in Airport and City searches.  This is
   on a per-event basis via Django admin (*recommended*).  It’s an extra setup step, but should only
   take a few seconds, and will yield better results.

1. Behind the scenes, the city typeahead is returning both Locations *and* Cities. This is
   a fuzzy match so it will accept a limited number of typos. 

    1. The Locations (if any) are at the top of the list.

        1. The suggester *suggests* Locations that have been used before as a Rideshare terminus for
	   that event.
           The more Rideshares the higher the ranking.  This ensures that the most likely ones are
           presented first.  This only works after some Rideshares have been created; if there are
           no Rideshares it’s just alphabetical.

    1. The Cities (if any) come second.  They are weighted by population,
       so bigger cities that match will appear at the top.

* Locations, Airports and Cities use a completion suggester.
  * Airports works slightly differently in that it also matches on airport codes, and city names,
    and is not fuzzy.  For example,
    * KSFO yields San Francisco International Airport
    * San Jose includes Norman Y. Mineta San Jose International Airport
    * Las Vegas includes McCarran International Airport, Las Vegas, Nevada
* Rideshares use a match query on the title, description and contact-name fields.


We used [django-elasticsearch-dsl](https://github.com/sabricot/django-elasticsearch-dsl).  It
supports a variety of Elasticsearch versions.  We are using it with version 6.  It relies on
[elasticsearch-dsl](https://github.com/elastic/elasticsearch-dsl-py)

Thus, in the requirements.py file, we have

```
elasticsearch-dsl>=6.0,<6.2
django-elasticsearch-dsl
```

There are *lots* of slightly different packages and package versions out there, so starting with
the right ones saves some confusion.

### Elasticsearch Server

Running [Elasticsearch](https://www.elastic.co/) locally for development is quite easy.

Running Elasticsearch in the cloud (e.g. AWS Elasticsearch) requires merely
changing the URL in the `ELASTICSEARCH_DSL` configuration.

Either way, just modify the configurations settings and you’re good to go.

### Building Indexes

After you have configured the connection to the Elasticsearch server, rebuild all indexes using

```
./manage.py search_index --rebuild
```

> **NOTE**: When the DEBUG flag is True, Rideshare and Location indexes are named
`rideshares-test` and `locations-test`, respectively, so they don’t interfere
with the existing production versions.

You can temporarily disable automatic indexing that gets performed when
a model is saved.  That may speed up city and airport imports.  In the `settings.py`
file:
```
ELASTICSEARCH_DSL_AUTO_REFRESH = False
```

The airports and cities indexes only need to be built once, since they don’t change.

```
./manage.py search_index --models=cities.City --rebuild
./manage.py search_index --models=airports.Airport --rebuild
```

The Rideshare index is automatically updated as Rideshares are created and deleted.
However, if necessary, it can be rebuilt, same as above.

```
./manage.py search_index --models=rideshare.Rideshare --rebuild
```

Similarly, the Locations index is automatically updated.  There should never be a need
to rebuild the index, but to update manually it would be,

```
./manage.py search_index --models=rideshare.Location --rebuild
```

### Testing

See [Testing](./TESTING.md).

