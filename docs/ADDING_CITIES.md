# Adding Cities and Locations

Rideshare has a database of cities around the world we get via
[django-cities](https://github.com/coderholic/django-cities) which gets them from
[geonames](http://geonames.org).  Cities are the endpoints of rides.

The `cities` database contains City, Region and Country tables, as well as some other stuff
we’re not using like postal codes and alternate names for places.

In a similar way, we have a table of airports. The `airports` database references the `cities`
database, so you have to have imported cities prior to importing airports.

A Location is an abstract object that can be either a city or an airport.  Normally, Locations
are added automatically when someone creates a Rideshare from or to a city or airport.

### Adding a Location

> **NOTE**: If all you need to do is make an existing city a Location so you can use it when
creating a new Event, then you can just do it from the command line:

``` ./manage.py add_location city Pittsboro US --region=NC ```

Alternatively, you may use the Django Admin.

1. Go to the Location creation page
1. Enter a city in the autocomplete field
1. Save the Location

_You don’t have to enter the other information_.  It will fill in the other values like 
location, region, country and name. You can then edit those if you like, for example, to
modify the name or the exact coordinates.

For example, the city of Pittsboro, NC already exists in the `cities` database, and we are going to be
locating an Event there, so we want to make sure it has a Location.

The instructions below are for when the city doesn’t already exist in the database, probably
because it has a very small population, or only exists for the duration of the event.

### Adding a New City

Sometimes it’s necesary to add a city *not* contained in our "world cities with population > 1,000"
database. This is required if an event takes place somewhere out in the boonies.  (On the other
hand, it may be enough to pick the closest city, and then change details in the Location object.)

Find your target city on [geonames](http://www.geonames.org/) and copy the information, paying
particular attention to the geonames id and point.  It’s important to get the correct id, since
a future import might overwrite it.  You should **not** just make something up.  (However, I *did*
make up the id in the Black Rock City example below, but usually that will not be necessary.)

> **NOTE**: Point coordinates are Point(longitude, latitude), not the reverse!

Adding and editing cities should automatically update the Elasticsearch index.

After creating the city, you will probably want to create a Location for it, as described above.

### Examples

This is how you can add a city manually.  
```python
from django.contrib.gis.geos import Point
from cities.models import City, Country, Region, Subregion

brc = City()
brc.id = 1
brc.name = 'Black Rock City'
brc.country = Country.objects.get(code='US')
brc.region = Region.objects.get(country__code='US', code='NV')
brc.subregion = Subregion.objects.get(region__code='NV', name='Washoe County')
brc.location = Point(-119.222367, 40.781581)
brc.population = 70000
brc.timezone = 'America/Los_Angeles'
brc.kind = 'PPL'
brc.name_std = "Black Rock City"
brc.save()
```

At least, that’s the manual way of doing it.  For Burning Man there is a fixture that
contains our added cities [here](../rideshare/fixtures/burning_man_cities.json).  You can add them all at once by executing

```
./manage.py loaddata rideshare/fixtures/burning_man_cities.json
```

If you add a city, remember that if you ever re-import the geonames cities database you will need to recreate it.
You can do that as above, or by creating a fixture using fixture_magic.
