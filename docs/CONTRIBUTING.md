# Contributing to Rideshare

Contributions are welcome, and they are greatly appreciated! Every
little bit helps, and credit will always be given.

### Types of Contributions

#### Report Bugs

Report issues [here](https://gitlab.com/liamk7/rideshare/issues).

If you are reporting a bug, please include:

* Your operating system name and version.
* Any details about your local setup that might be helpful in troubleshooting.
* Detailed steps to reproduce the bug.

#### Fix Bugs

Look through the GitLab issues for bugs. Anything tagged with “bug”
is open to whoever wants to implement it.

#### Implement Features

Look through the GitLab issues for features and the [Roadmap](./ROADMAP.md) document. 
Anything tagged with “feature” is open to whoever wants to implement it.



#### Write Documentation

Rideshare could use more documentation, whether as part of the
official docs, in docstrings, or even on the web in blog posts,
articles, and such.

#### Submit Feedback

The best way to send feedback is to [file an issue](https://gitlab.com/liamk7/rideshare/issues).
However, before filing an issue, you may want to [check with the developers](./SUPPORT.md).
If you are proposing a feature:

* Explain in detail how it would work.
* Keep the scope as narrow as possible, to make it easier to implement.
* Remember that this is a volunteer-driven project, and that contributions
  are welcome :)

### Get Started!

Ready to contribute? Here's how to set up Rideshare for local development.

1. Fork the [Rideshare repo](https://gitlab.com/liamk7/rideshare) on GitLab.
1. Clone your fork locally:
    ```shell
        git clone git@gitlab.com:your_name_here/rideshare.git
    ```
1. Install your local copy into a virtualenv. Assuming you have virtualenvwrapper installed, 
this is how you set up your fork for local development:
    ```shell
        mkproject -p python3 rideshare
        pip install -r requirements
    ```
1. Create a branch for local development:
    ```shell
        git checkout -b name-of-your-bugfix-or-feature
    ```
   Now you can make your changes locally.
   
   See [Installation](./INSTALLATION.md) and [Settings](../project/settings/README.md)
   for details.

1. When you're done making changes, check that your changes pass tests:
    ```shell
        ./manage.py test
   ```

1. Commit your changes and push your branch to GitLab::
    ```shell script
        git add .
        git commit -m "Your detailed description of your changes."
        git push origin name-of-your-bugfix-or-feature
   ```

1. Submit a pull request through the GitLab website.

#### Pull Request Guidelines

Before you submit a pull request, check that it meets these guidelines:

1. The pull request should include tests.
1. If the pull request adds functionality, the docs should be updated. Put
   your new functionality into a function with a docstring, if possible.
1. The pull request must work for in the supported versions of Django and Python.


