# Production Deployment

* [NGINX](#nginx)
* [Supervisor](#supervisor)
* [WSGI Container](#wsgi-container)
* [Django](#django)
* [Elasticsearch](#elasticsearch)
* [Memcache](#memcache)
* [Logging](#logging)

### NGINX

The server is running on EC2 behind an Elastic Load Balancer (ELB) which
provides SSL termination.  Port 80 is redirected to the ELB port 443.
The login urls for the API and Django Admin are rate limited to discourage
brute force attacks.

You **do** need to use HTTPS in production, because authentication
credentials are passed between the frontend client and the backend.


For sample configuration see [nginx.production.conf](../deploy/nginx.production.conf).

#### Maintenance

When doing site maintenance, display a “temporarily
unavailable” message by un-commenting the redirect in
`/etc/nginx/sites-enabled/rideshare.conf`.  You should check that the
maintenance html file is in NGINX’s default root directory.  You can
copy it there from
[maintenance.html](../deploy/maintenance.html).

#### Access

Limit access by ip address using the allow/deny directives, if necessary.

### Supervisor

The `DJANGO_SETTINGS` environment variable must be set.  In the
`/etc/supervisor.d/rideshare.conf` file make sure it’s correct:

```
environment=DJANGO_SETTINGS_MODULE="project.settings.production"
```
For sample configuration see 
[supervisor.production.conf](../deploy/supervisor.production.conf).


Useful supervisorctl commands
```shell
sudo supervisorctl
```
* `status`
* `restart rideshare`

For example, 
```shell
sudo supervisorctl restart rideshare
```

### WSGI Server

You can use any WSGI server, e.g. Uwsgi or Green Unicorn.
This is run by supervisor or systemd.  However, it can be useful to run manually for debugging sometimes.
See `/etc/supervisor.d/rideshare.conf`, particularly the command and environment values.

#### Green Unicorn

Refer to this link for configuring [number of workers](https://docs.gunicorn.org/en/latest/design.html#how-many-workers).

See [sample config file](../deploy/local_gunicorn_config.py)

#### UWSGI

See [sample config file](../deploy/uwsgi.production.ini)

### Django

If you are also serving the [Rideshare Frontend](https://gitlab.com/liamk7/rideshare-frontend),
you should add that application to your Django project.
It’s convenient to simply make a link to it, and then run its gulp command to create the static css
and javascipt bundles, then run collectstatic to place them in the static directory.

```shell
git clone rideshare-frontend.git
cdproject
ln -s /path/to/rideshare-frontend/rideshare_frontend .
```

Run the Django `collectstatic` command to place the static files where they can be served directly by NGINX.
That will normally be `<project root>/static`.

```shell
./manage.py collectstatic
```

> **NOTE**: Django generates warning messages during this process that can be ignored.
> They are the result of using customized Django Admin functionality from Grappelli.
> ```
> Found another file with the destination path 'admin/js/inlines.min.js'. It
> will be ignored since only the first encountered file is collected. If
> this is not what you want, make sure every static file has a unique path.
> ...
> ```

Sometimes it’s useful to use the Django shell.  Make sure to do it as user “django” using
the virtual environment.

```shell
sudo -i -u django
workon rideshare
./manage.py shell_plus
```

### Elasticsearch

See [Search](./SEARCH.md)

### Memcache

The site will function fine without it.  It just makes certain things faster.
```shell
sudo apt-get install memcached -y
```

### Logging

Log files are rotated automatically by Django.

They are located in `/var/log/django`

