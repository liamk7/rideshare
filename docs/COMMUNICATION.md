# Communication

Rideshare allows user to communicate with each other, and with the organization’s staff.

### Responding to a Rideshare

When a user responds to a Rideshare, their message is saved in the system and can be viewed
by the Rideshare creator once they have logged in.

The Rideshare creator also is notified that they have a message via

* Email. The text for email notification messages can be found in `project/templates/rideshare` 
in the  two `rideshare_email.[txt,html]` files.  They say the same thing, in `text/plain` and
`text/html` formats. A confirmation email is sent to the sender.
* SMS with the message **Rideshare message! Click to view** and a link. (If they have entered a
mobile phone number.)


### Help Facility

The Help page offers a simple form that sends emails to the site administrators set 
in the `settings.HELP_ADMINS` list.
It also sends a receipt to the user, promising a response should one be necessary.

See `settings.HELP_SUBJECT` and `settings.HELP_RECEIPT` for other values that can be configured.

### Infrastructure

#### Email

The Email subsystem is very flexible.  You could use the built-in 
[Django email system](https://docs.djangoproject.com/en/2.2/topics/email/).

You can also use [Django Anymail](https://github.com/anymail/django-anymail) to connect to 
an Email Service Provider (ESP).

> Anymail integrates several transactional email service providers (ESPs) into Django, with a consistent API that lets 
> you use ESP-added features without locking your code to a particular ESP.
> It currently fully supports **Amazon SES, Mailgun, Mailjet, Postmark, SendinBlue, SendGrid**, 
> and **SparkPost**, and has limited support for **Mandrill**.

Currently Rideshare is configured to use Anymail[Mandrill] and does not use any 
provider-specific features.

#### SMS

Rideshare is configured to use [Twilio](https://www.twilio.com/) via the
[twilio-python](https://github.com/twilio/twilio-python/) package.

However, it would be relatively easy to use some other text messaging service.

#### Testing

There are separate tests for Email and SMS delivery.  See [Testing](./TESTING.md).