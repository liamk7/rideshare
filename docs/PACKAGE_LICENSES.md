# Package Licenses

Package listing generated with

```bash
pip-licenses --order=license --format=markdown
```

### Packages used by Rideshare and their Licenses

| Name                     | Version  | License                                                      |
|--------------------------|----------|--------------------------------------------------------------|
| requests                 | 2.21.0   | Apache 2.0                                                   |
| elasticsearch            | 6.3.1    | Apache License, Version 2.0                                  |
| elasticsearch-dsl        | 6.1.0    | Apache License, Version 2.0                                  |
| django-elasticsearch-dsl | 0.5.1    | Apache Software License 2.0                                  |
| text-unidecode           | 1.2      | Artistic License                                             |
| Django                   | 2.2.10   | BSD                                                          |
| PySocks                  | 1.6.8    | BSD                                                          |
| django-debug-toolbar     | 1.11     | BSD                                                          |
| django-filter            | 2.1.0    | BSD                                                          |
| django-fixture-magic     | 0.1.4    | BSD                                                          |
| django-grappelli         | 2.12.2   | BSD                                                          |
| django-model-utils       | 3.1.2    | BSD                                                          |
| django-storages          | 1.7.1    | BSD                                                          |
| djangorestframework      | 3.9.2    | BSD                                                          |
| djangorestframework-gis  | 0.14     | BSD                                                          |
| drf-extensions           | 0.4.0    | BSD                                                          |
| shortuuid                | 0.5.0    | BSD                                                          |
| sqlparse                 | 0.3.0    | BSD                                                          |
| djrill                   | 2.1.0    | BSD License                                                  |
| idna                     | 2.8      | BSD-like                                                     |
| python-dateutil          | 2.8.0    | Dual License                                                 |
| Pillow                   | 6.2.0    | HPND                                                         |
| chardet                  | 3.0.4    | LGPL                                                         |
| validate-email           | 1.3      | LGPL                                                         |
| psycopg2-binary          | 2.8      | LGPL with exceptions or ZPL                                  |
| PyJWT                    | 1.7.1    | MIT                                                          |
| boto                     | 2.49.0   | MIT                                                          |
| django-airports          | 0.6      | MIT                                                          |
| django-cities            | 0.5.0.6  | MIT                                                          |
| factory-boy              | 2.11.1   | MIT                                                          |
| gunicorn                 | 19.9.0   | MIT                                                          |
| pytz                     | 2019.1   | MIT                                                          |
| six                      | 1.12.0   | MIT                                                          |
| swapper                  | 1.1.0    | MIT                                                          |
| urllib3                  | 1.24.1   | MIT                                                          |
| Faker                    | 1.0.4    | MIT License                                                  |
| colorlog                 | 4.0.2    | MIT License                                                  |
| django-cors-headers      | 2.5.2    | MIT License                                                  |
| django-extensions        | 2.1.6    | MIT License                                                  |
| certifi                  | 2019.3.9 | MPL-2.0                                                      |
| tqdm                     | 4.31.1   | MPLv2.0, MIT Licences                                        |
| ipaddress                | 1.0.22   | Python Software Foundation License                           |
| feedparser               | 5.2.1    | UNKNOWN                                                      |
| pkg-resources            | 0.0.0    | UNKNOWN                                                      |
| python-dotenv            | 0.10.1   | UNKNOWN                                                      |
| python-memcached         | 1.59     | UNKNOWN                                                      |
| twilio                   | 6.26.0   | UNKNOWN                                                      |
| docutils                 | 0.14     | public domain, Python, 2-Clause BSD, GPL 3 (see COPYING.txt) |

