# Testing

Rideshare incorporates a number of unittests.

Django creates a new test database for each test.  The test database
is populated with data from `rideshare/fixtures/api_test_data.json`.
Additionally, some objects are created via `factory_boy`.

> **NOTE**: there is no simlilar test database creation/deletion for
Elasticsearch.  Those tests are run against the existing Elasticsearch
data.  If the DEBUG flag is set, then Locations and Rideshares use
indexes named ```locations-test``` and ```rideshares-test```.


To run *all* tests

```
./manage.py test
```

### API Tests

```
./manage.py test api.tests
```

### Rideshare Tests

```
./manage.py test rideshare.tests
```

### Elasticsearch Tests

```
./manage.py test rideshare.tests.test_elasticsearch
```

### Notify Tests

```shell
./manage.py test rideshare.tests.test_notify
```

This tests the automatic generation of notifications that are sent when
someone contacts a Rideshare poster.  Django test behavior is to suppress
email delivery.  However, SMS messages sent via Twilio are not suppressed.

Note the use of the following test values in `local_settings.py`.

```python
SMS_TEST_NUMBER = '+14151112222'   # Your number, just for testing!
TWILIO_FROM_VALID = '+15005550006' # Passes validation, no SMS sent, for tests
```

```shell
./manage.py test rideshare.tests.test_notify.NotifySystemTestCase
```

These test the underlying email and SMS systems, independent of Django.  They send a single
SMS and email message, which will be received at the `SMS_TEST_NUMBER` and the
`EMAIL_TEST_ADDR`.