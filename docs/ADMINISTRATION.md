# Site Administration

Most management functions are provided by the Django Admin which is located at the `/admin` url.
A login with staff and administrative privileges is required.  Some administrative functions are
available via the command line.

### Django Administration

This includes operations on users, groups and sites.

If you need to reset a user’s password you would do it here.  Find the user by searching for
their username, email address, or first name or last name.  Where you see Password, click on
the link that says “change the password using *this form*.”

> In the case of HIDS authentication, if they have input multiple Rideshares (or Events, or...),
> there may be more than one, with a username like 12504bbe73b6825eaf7b3594b5afb4.  In that case,
> you need to figure out which one was associated with the Rideshare in question, which means
> you probably need to search for the Rideshare first.

You may also create administrative users here.  Make sure to use good passwords!

Adding new sites requires some stuff to happen in the code, so it is not supported at this time.
You would want to add a new site if you wanted your installation to server multiple different
“regionals.”

### Rideshare Administration

The main things to do here are add and edit Events, and possibly edit/delete Rideshares.

#### Adding Events

To add an Event, go to the Rideshare Administration and click on Events, then the “Add Event”
button in the top right corner.

The name of the Event will be converted into a slug, and must be unique.
For example, “Burning Man 2020” is converted to burning-man-2020.  If you really must have a
name without a date, for example, if you have an event that recurs more often and it doesn’t
make sense to give it a different name, then you should rename the slug to something that is
unique for that event.  This will not be an issue if you remove expired events.

If you omit the Image and Header Image fields, a default will be used.  It is recommended that
you upload suitable images. Make sure they are optimized, and the correct dimensions.

The dates must include the time.  Enter the date/time _in the locale of the_ *server*. (They
are converted to UTC when stored in the database, and will be displayed correctly in browsers,
according to the browser locale.)

* *Active*  can be used to turn the Event on and off.  For example you could create the Event,
   but wait to activate it until later.  Or, you could deactivate it if the event is canceled.
* *Featured* results in the Event being displayed prominently with its image on the landing page.
   There may be **only one** featured Event per region.  **Setting an Event to *Featured* un-sets the
   *Current* setting.**
* *Current* also makes the Event display on the landing page with its image.  There may be
   multiple current Events per region, however, 3 is optimal from a layout perspective.
* *Airport* indicates that an airport is available at the event.  This should probably only be
   selected if it’s possible for private pilots to use the airport, i.e. it may not be advisable
   for events near large international airports that don’t accommodate private planes.
* *Official* also must be true for the Event to show up.  This may be deprecated in the future.
* *Authentication Type* select *playa*.  Other types (django and oauth) have incomplete
   implementations.

*Regional* is a required value.  It is possible to host multiple “regional” events, which is a
way of segmenting the site so you can give them different landing pages.  However that starts
getting into other issues with how the Regional is going to be hosted.  Multiple regionals
should be considered experimental at the moment.  If you are only dealing with one entity,
then the Regional is that entity: the name of your organization.

*Location* is required.  It is the Location (City) where the Event is being held.  

1. Look for the Location in the pulldown, and select it if it’s there.
1. If it’s not there, click on the plus sign.
  1. In the *Add Location* page, select the City where the event is located from the City
     typeahead box.  You do not need to fill in any of the other fields.  They will be autopopulated
     when you click on Save. (After you save it you can go back and customize fields, as needed.)
  1. If you cannot find the City, it may be because it is too small (i.e. population < 1,000),
     or because it only exists for the duration of the event.  In that case, you must [add it
     by hand](./ADDING_CITIES.md).

### City/Airport Administration

This is there mainly for the curious.

## Support

Django Documentation is just that.

Rideshare API is an HTML browser for the API.  This will probably be disabled in production.

## Usage Statistics

Pretty, interactive **D3 charts** of your favorite Event.  The numbers charted are total Rideshares,
which is the sum of Ridehshare offers and requests.
This is basically experimental.

## Recent Rideshares (production only)

**RSS feed** of recent Rideshares, by Event.  Does not work on development site, only production.

## Command Line Operations

These commands should be all be executed as user “django” or whatever user owns the files. 

```shell
sudo -i -u django
```

### Archiving

Archive Events with the following command, where **event-id** is the unique id of the event you
are archiving.  This uses a package called django-fixture-magic.  See its documentation if you
want to archive other things.

```shell
workon rideshare
./manage.py dump_object -k rideshares.Event event-id  > *event-slug*-archive-fixture.json
```

### Deleting

You may permanently delete an Event from the database, together with its Rideshares, and their
Contacts.  If the Event uses “playa” authentication the Rideshares’ users will also be deleted.
*Make sure you archive it first!*

```shell
workon rideshare
./manage.py delete <event-slug>
```

The delete command requires an additional --delete argument, just to make sure you know what
you’re doing.  Also, it won’t allow you to delete an Event that hasn’t started or ended yet.
If you are sure you want to delete a current Event, use --force.  That might happen if an Event
is scheduled and then canceled before it has taken place.

### Search Indexing

See [Search](./SEARCH.md).

### Miscellaneous

You can access the Django shell like this, which allows all kinds of useful operations if you know a little Python.

```shell
workon rideshare
./manage.py shell_plus
```

## Django Settings

There are some values that may be adjusted in the `project/settings/local_settings.py` file.  
That requires shell access to the production server, and appropriate permissions.  
They are self-explanatory.

**EVENT_VIABILITY_EXTENSION** Number of days after an Event’s end date to continue showing event 
(default = 5).

If you make changes in the local_settings.py or production.py file, you must restart the application.

```shell
sudo supervisorctl restart rideshare
```

