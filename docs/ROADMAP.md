# Rideshare Roadmap

## Backend

### Improve Rideshare completion

After making an initial Rideshare contact, users continue their
conversation via other means, like telephone, email, etc.  For that
reason, they may not return to the site to delete their Rideshare entries,
which means that Rideshare efficacy is underestimated.  Also, a user may
cancel their trip to the event, or find a ride/rider via some other means.
That information would be useful and is not currently being captured.

This mainly involves modifying the front end so that the user can report
what happened with their Rideshare.  On the backend, database fields
for their reasons already exist.

Additionally, changes could be made on the backend, e.g. to ping users to
determine whether their listing is still active, with a link to update it.

Requires changes to frontend.

### Add real-time notifications

Currently, when a user responds to a Rideshare the Rideshare creator is
notified via SMS (Twilio) and Email (currently Mandrill).  The creator
then must reload the Rideshare app (or do something that triggers an AJAX
call) to retrieve the new message(s).  If they click on the email link,
they are taking directly to the message.

However, users these days expect that if they have the application open
it will immediately notify them that they have recieved a message.

It could also update other things, like the number of Rideshares currently
displayed on the event’s home page.

Requires changes to frontend.

Kind of a different topic, but it might also be nice to notify users
when a new Rideshare is created that matches what they are looking for.
If they are *seeking* a ride from a certain city, and someone creates a
Rideshare *offering* a ride from that city, then they could be notified
in the app, via SMS and/or email.  This might improve user engagement.
Maybe make this notification optional.  This part does not *require*
changes to the frontend, and can be implemented separately.

### Message queueing

Currently, emails are sent synchronously, i.e. when a user does
something that generates an email message (contacts a user, or use
the Help facility) there is a slight delay while contacting the email
service provider.  This can be improved by using message queueing.  A
[summary](https://anymail.readthedocs.io/en/stable/tips/transient_errors/)
here.

### Routing

Add ability to search for riders along route.

Currently, the user can search for riders or rides originating within
some radius of a target city.  In most cases that makes sense, but an
improvement would be to select riders along a given route.  That way, if
you are driving from SF you could also view riders in Sacramento or Reno.
OpenStreetMap has a routing package that can be used.

### Add link(s) to social media platforms

Sharing a ride with a stranger requires a great deal of trust. Users
should be able to access the social networks the Rideshare creator
belongs to, to help establish trust.

The theory is that social media links would increase engagement by
offering some “social proof,” reassuring users that the person they
are dealing with is somewhat reliable, or at least that other people
think so.

For users that choose to use a social media link, we could pull their
avatar on that platform, and use it instead of the Gravatar.

Requires changes to frontend.

### Implement GraphQL API

Can be parallel to current DRF API.

This should be easy, and would facilitate development of frontend.

### Time windows

Currently, the user specifies a time they are leaving, or want to leave.
We already have a “time window” represented in the database,
designating the earliest and latest time the user can leave.  However,
it is not being utilized.  This requires changes on the backend and
the frontend.

### Use geonames “alt names” for city lookup

We are currently using the name or ```std_name``` city name for
lookups. In some cases, cities have different names depending on your
language.  E.g. London would be Londres if you are searching in Spanish
or French.  Nice to have, not a high priority.

Airport names are all the English versions, however airports can also
be searched for by their abbreviation, e.g. SFO for San Francisco.

### Translation support

Currently the frontend handles all the translations except for:

1. Email and SMS notification messages
1. Event description text

These two things are independent of one another.

The Event.description field can be broken out into a 1–to–many
relationship to provide multiple translations, which the user must
enter themselves.  These can then be provided in the JSON returned when
querying the event API endpoint.

The Email notification templates can be translated using the standard
Django translation facility.  The SMS notification message can be extended
in the same way.

Then, when the frontend initiates a mutation (when creating the Rideshare)
it can sniff the browser’s language preferences and include that in
the mutation request.  The language preference is then saved with the
Rideshare (or with the user profile) and checked when it’s time to
send the user a notification.  Additionally, or alternatively, if the
user has selected a frontend language, we could record that preference
for later use.

Requires frontend and backend changes.

### Interface for adding new cities

Rideshare incorporates a list of cities with populations greater than
1,000 from [geonames](https://www.geonames.org/).  If an event takes
place in a city with a
 population less than 1,000, or the city is ephemeral and exists in some
 remote region, the database will not have a city at which to locate
 the event.  In these cases, users must currently add cities manually.
 This procedure is explained [here](./ADDING_CITIES.md).

It might be nice to provide users with an interface that facilitated
the addition of cities.

Requires frontend and backend changes.

### Ingress/Egress dates

Currently, an event’s beginning and end are represented as *datetime*
objects
 that are considered identical to the earliest time participants can
 arrive, and latest time by which they must leave.

Additional, distinct ingress/egress dates would be useful in instances
when
 official event times and arrival/departure times are different.

## Frontend

### Adding languages

Currently, ```Rideshare_frontend``` is translated into Spanish and French.
The framework exists to add other languages, which is probably not
strictly necessary given how many people speak English, but in any case
will have to be left to others who speak those languages.

### Flagging

Allow users to flag a Rideshare for being **Spam**, or otherwise
inappropriate.  Could also flag duplicate rideshares.

Hook this into the notification system so that admins are immediately
notified.

If we abandon the HIDS authentication system, we could allow users to
have lists of “favorite” Rideshares (like they can have a list of
favorite PlayaEvents).

