#!/usr/bin/env python
import os
import boto3
from dotenv import load_dotenv
from pathlib import Path

env_path = Path('project/settings') / '.env'
load_dotenv(dotenv_path=env_path, verbose=True)

AWS_ACCESS_KEY_ID = os.getenv('AWS_ACCESS_KEY_ID') 
AWS_SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY')
AWS_STORAGE_BUCKET_NAME = os.getenv('AWS_STORAGE_BUCKET_NAME')

filename = 'test_file.txt'
path = str(Path('scripts') / filename)

def run():
    try:
        s3 = boto3.resource('s3')
        s3.meta.client.upload_file(path, AWS_STORAGE_BUCKET_NAME, filename)
        s3.Bucket(AWS_STORAGE_BUCKET_NAME).download_file(filename, '/tmp/' + filename)
        s3.Object(AWS_STORAGE_BUCKET_NAME, filename).delete()
    except Exception as e:
        print(e)

