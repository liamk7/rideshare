import logging
import re
import pprint

from django.conf import settings
from django.urls import resolve
from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.shortcuts import render_to_response, get_object_or_404
from rest_framework.authentication import BaseAuthentication, TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import AuthenticationFailed
from rest_framework import authentication, permissions
from rest_framework.response import Response
from rest_framework.reverse import reverse

from rideshare.models import Rideshare

log = logging.getLogger(__name__)

# Post-save signal for User.save() creates an auth Token for user.
@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

class PlayaAuthentication(TokenAuthentication):
    """ This is a hack to support the minimalist PlayaAuthentication method
        which requires only a password, which is matched against the password
        of the user owning the object being modified.  (Obviously, the 
        object is required, as well.)

        PlayaAuthentication should probably go last in a view's list of 
        authentication_classes.  That way, the 401 Authenticate status code
        will be returned, and the client can return what it's got, depending on
        the event's authentication class -- either just the password
        (PlayaAuthentication) or the username/password combo (Standard).

        PlayaAuthentication sets the Token, so subsequent authentication 
        checks will just see the Token and authenticate based on that.
    """
    @staticmethod
    def _get_object2(request):
        # path_info is something like '/api/rideshares/7700/'
        # or '/api/rideshares/'
        #log.debug('PATH_INFO: %s', request.META['PATH_INFO'])
        path_info = request.META['PATH_INFO']
        resolved_func, unused_args, resolved_kwargs = resolve(path_info)
        try:
            return resolved_func.cls.model.objects.get(pk=resolved_kwargs['pk'])
        except KeyError: # perhaps no 'pk' supplied
            return None

    def authenticate(self, request):
        """ Note that returning None indicates authentication was not
            attempted, which means that other authentication methods
            will be tried.  If authentication was attempted and failed
            then we raise AuthenticationFailed, and no more auth methods
            are tried.
        """
        #log.debug('request: %s', pprint.pprint(request.META))


        if request.method == "POST":
            # Get the object being modified here. If there is no
            # object then we can't continue.
            try:
                obj = self._get_object2(request) 
                assert obj is not None
            except Exception as e:
                return None

            # get the password in either "form" or json format
            password = (request.POST.get('password',
                        request.DATA.get('password', None)))
            if not password:
                log.debug('PlayaAuthentication not attempted, no password')
                return None

            # authenticate the owner of this obj with supplied password
            user = authenticate(username=obj.user.username, password=password)
            if not user:
                raise AuthenticationFailed('authentication failure (bad password?)')
            if not user.is_active:
                raise AuthenticationFailed('user is inactive or deleted')

            # if user was authenticated, lookup their token
            auth = user.auth_token

            log.debug('PlayaAuthentication succeeded: %s %s', user, auth)
            return (user, auth)

        return None


class ContactAuthentication(BaseAuthentication):
    """ This is a hack to support the minimalist playa authentication method
        which requires only a password, which is matched against the password
        of the user owning the object being modified.

        PlayaAuthentication should probably go last in a view's list of 
        authentication_classes.

        PlayaAuthentication sets the Token, so subsequent authentication 
        checks will just see the Token and authenticate based on that.
    """
    @staticmethod
    def _get_object(request):
        # path_info is something like '/api/rideshares/7700/'
        #log.debug('PATH_INFO: %s', request.META['PATH_INFO'])
        path_info = request.META['PATH_INFO']
        p = re.compile('([^/]+)/(\d+)/?$')
        match = p.search(path_info)
        if match:
            cls, pk = match.group(1, 2)
            cls = cls.capitalize().rstrip('s')
            # TODO Make the eval safe!  Import other required classes above
            cls = eval(cls)
        else:
            cls = pk = None
        return cls, pk

    @staticmethod
    def _get_object2(request):
        # path_info is something like '/api/rideshares/7700/'
        #log.debug('PATH_INFO: %s', request.META['PATH_INFO'])
        path_info = request.META['PATH_INFO']
        resolved_func, unused_args, resolved_kwargs = resolve(path_info)
        log.debug('resolved_func: %s', resolved_func)
        return resolved_func.cls.model.objects.get(pk=resolved_kwargs['pk'])

    def authenticate(self, request):
        """ Note that returning None indicates authentication was not
            attempted, which means that other authentication methods
            will be tried.  If authentication was attempted and failed
            then we raise AuthenticationFailed, and no more auth methods
            are tried.
        """
        #log.debug('request: %s', pprint.pprint(request.META))
        try:
            # get the object being modified here

            #cls, pk = self._get_object(request)
            #assert cls is not None and pk is not None
            #obj = cls.objects.get(id=pk)
            #log.debug('obj: %s', obj)
            obj = _get_object2(request)
            log.debug('Obj2: %s', obj)
        except:
            return None

        if request.method == "POST":
            # get the password in either "form" or json format
            password = (request.POST.get('password',
                        request.DATA.get('password', None)))
            if not password:
                return None

            # authenticate the user of this obj with supplied password
            user = authenticate(username=obj.user.username, password=password)
            if not user:
                raise AuthenticationFailed('authentication failure (bad password?)')
            if not user.is_active:
                raise AuthenticationFailed('user is inactive or deleted')

            # if user was authenticated, lookup their token
            auth = Token.objects.get(user=user)

            log.debug('PlayaAuthentication: %s %s', user, auth)
            return (user, auth)

        return None

## Based on rest_framework.authentication.TokenAuthentication
#class TokenAuthentication(BaseAuthentication):
#    """
#    Simple token based authentication.
#
#    Clients should authenticate by passing the token key in the "Authorization"
#    HTTP header, prepended with the string "Token ".  For example:
#
#        Authorization: Token 401f7ac837da42b97f613d789819ff93537bee6a
#    """
#
#    model = Token
#    """
#    A custom token model may be used, but must have the following properties.
#
#    * key -- The string identifying the token
#    * user -- The user to which the token belongs
#    """
#
#    def authenticate(self, request):
#        auth = get_authorization_header(request).split()
#
#        if not auth or auth[0].lower() != b'token':
#            return None
#
#        if len(auth) == 1:
#            msg = 'Invalid token header. No credentials provided.'
#            raise exceptions.AuthenticationFailed(msg)
#        elif len(auth) > 2:
#            msg = 'Invalid token header. Token string should not contain spaces.'
#            raise exceptions.AuthenticationFailed(msg)
#
#        return self.authenticate_credentials(auth[1])
#
#    def authenticate_credentials(self, key):
#        try:
#            token = self.model.objects.get(key=key)
#        except self.model.DoesNotExist:
#            raise exceptions.AuthenticationFailed('Invalid token')
#
#        if not token.user.is_active:
#            raise exceptions.AuthenticationFailed('User inactive or deleted')
#
#        return (token.user, token)
#
#    def authenticate_header(self, request):
#        return 'Token'
#
from rest_framework.authentication import SessionAuthentication

class SuperUserSessionAuthentication(SessionAuthentication):
    """
    Use Django's session framework for authentication of super users.
    """

    def authenticate(self, request):
        """
        Returns a `User` if the request session currently has a logged in user.
        Otherwise returns `None`.
        """

        # Get the underlying HttpRequest object
        request = request._request
        user = getattr(request, 'user', None)

        # Unauthenticated, CSRF validation not required
        if not user or not user.is_active or not user.is_superuser:
            return None

        self.enforce_csrf(request)

        # CSRF passed with authenticated user
        return (user, None)
