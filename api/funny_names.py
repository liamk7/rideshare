#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random
import time
import sys

first_names = """
    Aaron
    Adam
    Adrian
    Aiden
    Alex
    Alexander
    Andrew
    Anthony
    Asher
    Austin
    Benjamin
    Bentley
    Blake
    Brayden
    Brody
    Caden
    Caleb
    Camden
    Cameron
    Carson
    Carter
    Charlie
    Chase
    Christian
    Christopher
    Cole
    Colin
    Colton
    Connor
    Cooper
    Daniel
    David
    Declan
    Dominic
    Dylan
    Eli
    Elijah
    Elliot
    Ethan
    Evan
    Gabriel
    Gavin
    Grayson
    Henry
    Hudson
    Hunter
    Ian
    Isaac
    Isaiah
    Jack
    Jackson
    Jacob
    Jake
    James
    Jason
    Jayce
    Jayden
    Jeremiah
    John
    Jonathan
    Jordan
    Joseph
    Joshua
    Josiah
    Julian
    Landon
    Leo
    Levi
    Liam
    Lincoln
    Logan
    Lucas
    Luke
    Mason
    Mateo
    Matthew
    Max
    Micah
    Michael
    Miles
    Muhammad
    Nathan
    Nathaniel
    Nicholas
    Noah
    Nolan
    Oliver
    Owen
    Parker
    Riley
    Ryan
    Samuel
    Sebastian
    Thomas
    Tristan
    Tyler
    William
    Wyatt
    Xavier
    Zachary

    Aaliyah
    Abigail
    Adalyn
    Addison
    Adeline
    Alaina
    Alexandra
    Alexis
    Alice
    Allison
    Alyssa
    Amelia
    Anna
    Annabelle
    Aria
    Arianna
    Aubrey
    Audrey
    Ava
    Avery
    Bailey
    Bella
    Brooklyn
    Callie
    Camilla
    Caroline
    Charlie
    Charlotte
    Chloe
    Claire
    Elena
    Eliana
    Elizabeth
    Ella
    Ellie
    Emily
    Emma
    Eva
    Evelyn
    Gabriella
    Gianna
    Grace
    Hailey
    Hannah
    Harper
    Isabella
    Isabelle
    Jasmine
    Jordyn
    Julia
    Juliana
    Kaelyn
    Kaitlyn
    Katherine
    Kayla
    Kaylee
    Keira
    Kennedy
    Kylie
    Lauren
    Layla
    Leah
    Lila
    Liliana
    Lillian
    Lily
    London
    Lucy
    Mackenzie
    Madelyn
    Madison
    Makayla
    Maria
    Maya
    Mia
    Mila
    Molly
    Natalie
    Nora
    Olivia
    Penelope
    Peyton
    Reagan
    Riley
    Ruby
    Sadie
    Samantha
    Sarah
    Savannah
    Scarlett
    Skyler
    Sophia
    Sophie
    Stella
    Sydney
    Taylor
    Victoria
    Violet
    Vivian
    Zoe
""".split()

last_names = """
    Ant
    Antelope
    Ape
    Ass
    Badger
    Bat
    Bear
    Beaver
    Bird
    Boar
    Camel
    Canary
    Cat
    Cheetah
    Chicken
    Chimpanzee
    Chipmunk
    Cow
    Crab
    Crocodile
    Deer
    Dog
    Dolphin
    Donkey
    Duck
    Eagle
    Elephant
    Ferret
    Fish
    Fox
    Frog
    Gecko
    Giraffe
    Goat
    Goose
    Groundhog
    Hamster
    Hare
    Hawk
    Heron
    Herring
    Hyena
    Horse
    Ibex
    Ibis
    Iguana
    Impala
    Jackal
    Jaguar
    Kangaroo
    Kinkajou
    Koala
    Kudu
    Lemming
    Lemur
    Leopard
    Lion
    Lizard
    Llama
    Meerkat
    Mole
    Mongoose
    Monkey
    Moose
    Mouse
    Mule
    Nighthawk
    Numbat
    Ocelot
    Orca
    Ostrich
    Otter
    Owl
    Panda
    Parrot
    Peccary
    Pig
    Polecat
    Porcupine
    Possum
    Quail
    Quetzal
    Rabbit
    Raccoon
    Rat
    Rattlesnake
    Raven
    Rhinoceros
    Salmon
    Seal
    Shark
    Sheep
    Skunk
    Sloth
    Snake
    Spider
    Squirrel
    Stork
    Swan
    Tapir
    Tarantula
    Toad
    Tiger
    Tortoise
    Toucan
    Turkey
    Urial
    Vicuna
    Vulture
    Wallaby
    Walrus
    Warthog
    Whale
    Wolf
    Wombat
    Xantis
    Yak
    Zebra
    Zorilla
    """.split()

adjectives = """
    Amusing
    Ancient
    Antsy
    Anxious
    Arctic
    Awful
    Bad
    Beautiful 
    Benevolent
    Big
    Bitter
    Blistering
    Blithe
    Bored
    Brave
    Brawny
    Brazen
    Brilliant
    Bucolic
    Bulky 
    Calm
    Carefree
    Caring
    Challenging
    Charming
    Cheerful
    Chilly
    Cold
    Cold
    Compassionate
    Contrary
    Courageous
    Cowardly
    Crawling
    Creative
    Creeping
    Crispy
    Cruel 
    Curious
    Dangerous
    Dawdling
    Decrepit
    Delicate
    Delighted
    Delinquent
    Demanding
    Difficult
    Diminutive
    Dreadful
    Dreadful 
    Dynamite
    Easy
    Ecstatic
    Effortless
    Enchanting
    Endless
    Enormous
    Entertaining
    Eternal
    Exquisite
    Fabulous
    Fair
    Famous
    Fantastic
    Fast
    Fetid
    Filthy
    Fluttering
    Foul
    Freezing
    Frosty
    Generous
    Gentle 
    Ghastly
    Ghastly 
    Gigantic
    Gilded
    Glowing
    Good
    Gorgeous
    Grave
    Great
    Greedy
    Grimy
    Gullible
    Gullible
    Happy
    Hard 
    Hardworking
    Hasty
    Heavy
    Hideous
    Hopeful
    Horrid
    Hot
    Huge
    Hulking
    Hungry
    Icy
    Immense
    Impatient
    Infamous
    Intense
    Intimidating
    Itty-bitty
    Joyful
    Kind
    Laconic
    Lazy
    Lively
    Lousy
    Lovely
    Marvelous
    Massive 
    Meandering
    Menacing 
    Miniscule
    Minute
    Miserable
    Monstrous
    Muffled
    Muggy
    Mysterious
    Nasty
    Nervous
    Obnoxious
    Oppressive
    Ornery
    Petite
    Picturesque
    Polite
    Profound
    Puny
    Quick
    Quiet
    Rambunctious 
    Rancid
    Rapid
    Relaxed
    Repulsive
    Revolting
    Ridiculous
    Rotten
    Rude
    Rusty
    Scorching
    Scrumptious
    Selfish
    Serious
    Severe
    Shining
    Shocking
    Silly
    Simple
    Sizzling
    Slick
    Slight
    Slimy
    Sloppy
    Slow
    Sluggish
    Small
    Smelly
    Soaring
    Sparkling
    Speedy
    Spiky
    Splendid
    Spoiled
    Steaming
    Stifling
    Striking
    Stunning
    Subtle
    Sultry
    Superb
    Sweltering
    Swift
    Terrible
    Thrifty
    Tiny
    Towering
    Tranquil
    Tremendous
    Trying
    Ugly
    Vast
    Verbose
    Vile
    Whopping
    Wild
    Wild
    Zippy
    """.split()

funny_quotes =  [
'Always remember that you are absolutely unique. Just like everyone else.\n-- Margaret Mead', 'Do not take life too seriously. You will never get out of it alive.\n-- Elbert Hubbard', 'People who think they know everything are a great annoyance to those of us who do.\n-- Isaac Asimov', 'I believe that if life gives you lemons, you should make lemonade... And try to find somebody whose life has given them vodka, and have a party.\n-- Ron White', 'Get your facts first, then you can distort them as you please.\n-- Mark Twain', "Age is something that doesn't matter, unless you are a cheese.\n-- Luis Bunuel", 'A successful man is one who makes more money than his wife can spend. A successful woman is one who can find such a man.\n-- Lana Turner', "When you are courting a nice girl an hour seems like a second. When you sit on a red-hot cinder a second seems like an hour. That's relativity.\n-- Albert Einstein", 'Procrastination is the art of keeping up with yesterday.\n-- Don Marquis', 'Too much agreement kills a chat.\n-- Eldridge Cleaver'
]


class FunnyQuote(object):
    """ Create fanciful random quotes for testing """
    def __init__(self):
        self.quote = random.choice(funny_quotes)

    def __str__(self):
        return '{}'.format(self.quote)

class FunnyName(object):
    """ Create fanciful random names for testing """
    def __init__(self):
        self.first_name = random.choice(first_names).capitalize()
        self.last_name = random.choice(
            [n for n in last_names
               if n.startswith(self.first_name[0].upper())]).capitalize()

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)

class FunnyFestival(object):
    """ Create fanciful random festival names for testing """
    def __init__(self):
        self.adjective = random.choice(adjectives).capitalize()
        self.animal = random.choice(
            [n for n in last_names
               if n.startswith(self.adjective[0].upper())]).capitalize()

    def __str__(self):
        return '{} {}'.format(self.adjective, self.animal)

class FunnyPassword(object):
    """ Create fanciful random password """
    def __init__(self):
        self.adjective1 = random.choice(adjectives).lower()
        self.adjective2 = random.choice(adjectives).lower()
        self.animal = random.choice(
            [n for n in last_names
               if n.startswith(self.adjective2[0].upper())]).lower()

    def __str__(self):
        return '{}-{}-{}'.format(
                self.adjective1,
                self.adjective2,
                self.animal
            )

if __name__ == '__main__':
    while True:
        fn = FunnyName()
        ff = FunnyFestival()
        fp = FunnyPassword()
        print('{} is going to {} password = {}'.format(fn, ff, fp))
        #time.sleep(1)

