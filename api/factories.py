import logging
import pytz
import random
import random
import string
from datetime import datetime, date, timedelta
from functools import partial

from django.conf import settings
from django.contrib.auth.models import User
import factory
import factory.fuzzy

from api.funny_names import FunnyName, FunnyQuote, FunnyFestival
from rideshare.models import (
    City,
    Contact,
    Direction,
    Event,
    Hauling,
    Location,
    Preference,
    Regional,
    Ride,
    Rideshare,
    RideType,
    RoundTrip,
    Site,
)

log = logging.getLogger(__name__)

DEFAULT_PASSWORD = 'password'

TIMEDELTA = timedelta(days=2) 
EVENT_TIMEDELTA = timedelta(days=40) 


def _random_datetime(obj, field=None):
    """Create a random datetime for the Rideshare
    """
    target_date = getattr(obj.event, field)
    start_dt = pytz.utc.localize(datetime.combine(target_date, datetime.min.time())-TIMEDELTA)
    end_dt = pytz.utc.localize(datetime.combine(target_date, datetime.min.time())+TIMEDELTA)
    delta = end_dt - start_dt
    microseconds = delta.microseconds + 1000000 * (delta.seconds + (delta.days * 86400))

    offset = random.randint(0, microseconds)
    result = start_dt + timedelta(microseconds=offset)
    return result

# Use these to get a random datetime via the LazyAttribute method.
# This is a workaround/replacement for FuzzyDateTime because we 
# want to access the instance.event object but it has not been created
# yet when we want to access it.
random_start_date = partial(_random_datetime, field='start_date')
random_end_date = partial(_random_datetime,  field='end_date')


class UserFactory(factory.django.DjangoModelFactory):
    """ Create Django User objects
    """
    class Meta:
        model = User
        exclude = ('funny_name',)

    @factory.sequence
    def funny_name(n):
        return FunnyName()

    username = factory.Sequence(lambda n: 'testuser%s' % n)
    email = factory.LazyAttribute(lambda o: '{}.{}@example.org'.format(
            o.funny_name.first_name.lower(),
            o.funny_name.last_name.lower()))
    first_name = factory.LazyAttribute(lambda o: o.funny_name.first_name)
    last_name = factory.LazyAttribute(lambda o: o.funny_name.last_name)
    password = factory.PostGenerationMethodCall('set_password', DEFAULT_PASSWORD)

    is_superuser = False
    is_staff = False
    is_active = True


class RideshareFactory(factory.django.DjangoModelFactory):
    """ Create Rideshare objects with the following params:
            location is random
            event is a random event from the loaded data
            direction is random (to or from event)
            round_trip is random (either one-way or round-trip)
            rtype is random (either request or offer)
            ride is random (either passengers or not)
            hauling is random (either hauling or not)
            riders is random (0-20)
            departure/return dates are within two days of event start/end
            full is random (~33% rideshares are marked as already filled)
            flight is False
    """
    class Meta:
        model = Rideshare

    user = factory.SubFactory(UserFactory)
    rtype = factory.fuzzy.FuzzyChoice(RideType.objects.all())
    #rtype = factory.SubFactory(RideTypeFactory)

    # for now, only get locations that are cities -- airports cause errors in
    # tests.RideshareTestCase.setUp().self.rideshare when it is accessed
    # from test_search_rideshares() search by city
    location = factory.fuzzy.FuzzyChoice(Location.objects.filter(city__isnull=False))
    #for loc in Location.objects.all():
        #print loc.city, loc.airport, loc.name
    event = factory.fuzzy.FuzzyChoice(Event.objects.all())
    #event = factory.fuzzy.FuzzyChoice(Event.objects.all())
    direction = factory.fuzzy.FuzzyChoice(Direction.objects.all())
    round_trip = factory.fuzzy.FuzzyChoice(RoundTrip.objects.all())
    # TODO: ride and hauling cannot both be false!
    # Leave it invalid to test detection?
    ride = factory.fuzzy.FuzzyChoice(Ride.objects.all())
    hauling = factory.fuzzy.FuzzyChoice(Hauling.objects.all())

    password_hint = 'Password hint: %s' % DEFAULT_PASSWORD
    #name = factory.Sequence(lambda n: 'Rideshare Test Case #%s' % n)
    name = factory.fuzzy.FuzzyChoice([
        'Twas brillig, and the slithy toves',
        'Did gyre and gimble in the wabe',
        'All mimsy were the borogoves',
        'And the mome raths outgrabe',
        ])

    riders = factory.fuzzy.FuzzyInteger(0, 20)

    # Note: Converting to UTC timezone-aware types 
    earliest_departure = factory.LazyAttribute(random_start_date)
    latest_departure = earliest_departure
    earliest_return = factory.LazyAttribute(random_end_date)
    latest_return = latest_departure

    # Make about a third of them full
    full = factory.fuzzy.FuzzyChoice([True, False, False])
    #flight = factory.Iterator([True, False])
    flight = False
    description = factory.fuzzy.FuzzyChoice([
"""
Beware the Jabberwock, my son!
The jaws that bite, the claws that catch!
Beware the Jubjub bird, and shun
The frumious Bandersnatch!"
""",
"""
He took his vorpal sword in hand:
Long time the manxome foe he sought --
So rested he by the Tumtum tree,
And stood awhile in thought.
""",
"""
And, as in uffish thought he stood,
The Jabberwock, with eyes of flame,
Came whiffling through the tulgey wood,
And burbled as it came!
""",
"""
One, two! One, two! And through and through
The vorpal blade went snicker-snack!
He left it dead, and with its head
He went galumphing back.
""",
"""
And, has thou slain the Jabberwock?
Come to my arms, my beamish boy!
O frabjous day! Callooh! Callay!'
He chortled in his joy.
""",
    ])
    contact_name = factory.LazyAttribute(lambda o: '%s %s' %
            (o.user.first_name, o.user.last_name))
    contact_email = factory.LazyAttribute(lambda o: o.user.email)
    contact_info = 'Contact info'
    contact_phone = settings.SMS_TEST_NUMBER
    sms_notify = True

    # weight is only non-zero when this is a flight
    riders_baggage_weight = factory.LazyAttribute(lambda o: 40 if o.flight else 0)
    deleted = False
    active = True

    @factory.post_generation
    def preferences(self, create, extracted, **kwargs):
        # various preferences, in randomized combinations
        smoking = random.choice(['SMOKING', 'NON_SMOKING', None])
        disabled = random.choice(['DISABLED', None])
        open_minded = random.choice(['OPEN_MINDED', None])
        prefs = [_f for _f in [smoking, disabled, open_minded] if _f]
        for key in prefs:
            self.preferences.add(Preference.objects.get(key=key))


class ContactFactory(factory.django.DjangoModelFactory):
    """ Create Contact objects.
    """ 
    class Meta:
        model = Contact
        exclude = ('funny_name', 'funny_quote')

    @factory.sequence
    def funny_name(n):
        return FunnyName()

    @factory.sequence
    def funny_quote(n):
        return FunnyQuote()

    rideshare = factory.SubFactory(RideshareFactory)

    contact_phone = settings.SMS_TEST_NUMBER
    contact_name = factory.LazyAttribute(lambda o: o.funny_name)
    contact_email = factory.LazyAttribute(lambda o: "%s.%s@example.org" % (
        o.funny_name.first_name.lower(),
        o.funny_name.last_name.lower()))
    contact_info = factory.fuzzy.FuzzyText(
        prefix="Call me at +1",
        chars=string.digits,
        length=10,
        suffix=' any time!')
    message = factory.LazyAttribute(lambda o: str(o.funny_quote))


## TODO: Possibly create Locations and Events, etc.  This was getting too
## complicated and I decided to import a json file instead, but it would be nice
## to have this completed.
##
#class LocationFactory(factory.django.DjangoModelFactory):
#    """ Create Location objects.
#    """
#    class Meta:
#        model = Location
#
#    # foreign key relations
#    city = factory.SubFactory(CityFactory)
#    airport = None
#
#class SiteFactory(factory.django.DjangoModelFactory):
#    """ Create Site objects.
#    """
#    class Meta:
#        model = Site
#
#class CityFactory(factory.django.DjangoModelFactory):
#    """ Create City objects.
#    """
#    class Meta:
#        model = City
#
#class RegionalFactory(factory.django.DjangoModelFactory):
#    """ Create Regional objects.
#    """
#    class Meta:
#        model = Regional
#        exclude = ('funny_name',)
#
#    @factory.sequence
#    def funny_name(n):
#        return FunnyName()
#
#    # foreign key relations
#    city = factory.SubFactory(CityFactory)
#    airport = None
#
#    name = factory.LazyAttribute(lambda o: u'%s %d' % (o.funny_name,
#        o.start_date.year))
#    slug = factory.LazyAttribute(lambda o: u'%s %d'.lower() % (o.funny_name,
#        o.start_date.year))
#
#class EventFactory(factory.django.DjangoModelFactory):
#    """ Create Event objects.
#    """
#    class Meta:
#        model = Event
#        exclude = ('funny_name',)
#
#    @factory.sequence
#    def funny_name(n):
#        return FunnyFestival()
#
#    # foreign key relations
#    location = factory.SubFactory(LocationFactory)
#    location = factory.fuzzy.FuzzyChoice(Location.objects.filter(city__isnull=False))
#    regional = factory.fuzzy.FuzzyChoice(Regional.objects.all())
#    auth_type = 2
#
#    # slug is created when object is saved
#
#    # pick a random date in the next year, and make the event up to
#    # a week long.
#    start_date = factory.fuzzy.FuzzyDateTime(
#            pytz.utc.localize(datetime.combine(datetime.now(),
#                datetime.min.time())),
#            pytz.utc.localize(datetime.combine(datetime.now(),
#                datetime.min.time())+EVENT_TIMEDELTA))
#
#    end_date = start_date.evaluate(2, None, False) + timedelta(days=7)
#    #factory.fuzzy.FuzzyDateTime(start_date.evaluate(2, None, False),
#            #start_date.evaluate(2, None, False) + timedelta(days=7))
#
#    name = factory.LazyAttribute(lambda o: u'%s %d' % (o.funny_name,
#        o.start_date.year))
#
#    # active = default
#    # current = default
#    # airport = default
#    # official = default
#
