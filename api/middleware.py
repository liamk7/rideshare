"""
A JSON Vulnerability middleware that AngularJS knows how to process.
Borrowed from https://github.com/appliedsec/djangular
Fixed to work with Django 1.10+
"""

class AngularJsonVulnerabilityMiddleware(object):
    """
    A middleware that inserts the AngularJS JSON Vulnerability request
    on JSON responses.
    """
    # The AngularJS JSON Vulnerability content prefix. See http://docs.angularjs.org/api/ng.$http
    CONTENT_PREFIX = b")]}',\n"

    # Make this class easy to extend by allowing class level access.
    VALID_STATUS_CODES = [200, 201, 202]
    VALID_CONTENT_TYPES = ['application/json']

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        if response.status_code in self.VALID_STATUS_CODES and \
           response['Content-Type'] in self.VALID_CONTENT_TYPES:
            response.content = self.CONTENT_PREFIX + response.content

        return response
