import collections
import copy
import datetime
import hashlib
import json
import logging
import pprint
import smtplib
import time

import django_filters.rest_framework
from anymail.exceptions import AnymailRecipientsRefused
from airports.models import Airport
from cities.models import City
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.gis.measure import D
from django.contrib.gis.db.models.functions import Distance
from django.core.cache import cache
from django.core.mail import EmailMessage
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage, Page
from django.db.models import Count
from django.db.models.query import EmptyQuerySet
from django.shortcuts import render_to_response, get_object_or_404
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.encoding import force_text
from django.utils.http import urlencode, quote_plus
from django.views.decorators.csrf import ensure_csrf_cookie
from django.conf import settings
#from django.contrib.gis.geoip2 import GeoIP2

from elasticsearch.exceptions import NotFoundError, ConnectionError, ElasticsearchException
from elasticsearch_dsl import Q

from rest_framework.authentication import (TokenAuthentication, SessionAuthentication)
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view
from rest_framework.exceptions import APIException
from rest_framework.generics import *
from rest_framework import authentication
from rest_framework import filters
from rest_framework import generics
from rest_framework import permissions
from rest_framework import renderers
from rest_framework import status
from rest_framework import viewsets
from rest_framework import mixins
from rest_framework.decorators import detail_route
from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from rest_framework_extensions.cache.mixins import CacheResponseMixin
from rest_framework_extensions.cache.decorators import cache_response
from rest_framework_extensions.key_constructor.constructors import DefaultKeyConstructor
from rest_framework_extensions.key_constructor.bits import (
    KeyBitBase,
    ListSqlQueryKeyBit,
    PaginationKeyBit,
    QueryParamsKeyBit,
    RetrieveSqlQueryKeyBit,
    UserKeyBit,
)


from api.authentication import PlayaAuthentication
from api.permissions import IsOwner, IsOwnerOrReadOnly
from rideshare.geospatial import bbox_from_point
from rideshare.models import (
    Aggregate,
    Contact,
    Event,
    FAQ,
    Location,
    Preference,
    Regional,
    Rideshare,
    RideType,
)
from rideshare.documents import (
    AirportDocument,
    CityDocument,
    LocationDocument,
    RideshareDocument,
)

from api.serializers import (
    AirportSerializer,
    AutoAirportSerializer,
    AutoComboSerializer,
    AutoRideshareSerializer,
    CitySerializer,
    ContactSerializer,
    EventSerializer,
    FaqSerializer,
    HelpMessageSerializer,
    LocationSerializer,
    LoginSerializer,
    OffsetRideshareSerializer,
    PreferenceSerializer,
    RegionalSerializer,
    RideshareFullSerializer,
    RideshareSerializer,
    RideTypeSerializer,
    StatSerializer,
    UserSerializer,
)
from . import nesting

log = logging.getLogger(__name__)

# Note that the default permissions set in settings.py is
# IsAuthenticatedOrReadOnly.   This is used for all the API endpoints
# except RideshareViewSet and ContactViewSet.
# The latter two require explicit permission checks.

# This is the default, normally the radius is set in the search request
SEARCH_RADIUS = 100.0 # kilometers

# Fewer than MIN results triggers search region expansion.  Setting this
# to 0 disables search expansion.
MIN_SEARCH_RESULT = 0

# Utility functions
def _get_boolean(value_string):
    """ Convert a JSON boolean string ('true' or 'false')
        to a Python boolean value (True or False)
    """
    value = None
    if value_string == 'true': value = True
    elif value_string == 'false': value = False
    #assert(value in (True, False))
    return value

def _auth_error_response(request):
    log.debug('Token: %s', request.META.get('HTTP_AUTHORIZATION'))
    if (request.user):
        if not(request.user.is_authenticated):
            #raise APIException
            log.debug('User %s exists but is not authenticated', request.user)
            return Response('User %s exists but is not authenticated' % 
                    request.user, status=status.HTTP_401_UNAUTHORIZED)
    else:
        #raise RideshareAPIException('Unauthenticated.  No user.')
        log.debug('No user in request.')
        return Response('No user',
            status=status.HTTP_401_UNAUTHORIZED)

def _generate_username(name):
    m = hashlib.md5()
    seed = str(time.time())
    m.update(seed.encode('utf-8'))
    m.update(name.encode('utf-8'))
    username = m.hexdigest()[:30]
    return username

# Exceptions
class RideshareAPIException(APIException):
    status_code = 400
    default_detail = 'API error'

# Filters
class IsOwnerFilterBackend(filters.BaseFilterBackend):
    """
    Filter that only allows users to see their own objects.
    """
    def filter_queryset(self, request, queryset, view):
        log.debug('IsOwnerFilterBackend: %s', request.user)
        return queryset.filter(user=request.user)

class RideshareFilter(django_filters.rest_framework.FilterSet):
    """
    Filter on direction, slug, riders, location
    """
    direction = django_filters.rest_framework.NumberFilter(field_name='direction', lookup_expr='exact')
    location = django_filters.rest_framework.NumberFilter(field_name='location', lookup_expr='exact')
    riders = django_filters.rest_framework.NumberFilter(field_name='riders', lookup_expr='exact') 
    slug = django_filters.rest_framework.CharFilter(field_name='event__slug', lookup_expr='exact')
    class Meta:
        model = Rideshare
        fields = ['direction', 'slug', 'riders', 'location' ]

class RegionalFilter(django_filters.rest_framework.FilterSet):
    slug = django_filters.rest_framework.CharFilter(field_name='slug', lookup_expr='exact')
    class Meta:
        model = Regional
        fields = ['slug']

class EventFilter(django_filters.rest_framework.FilterSet):
    active = django_filters.rest_framework.BooleanFilter(field_name='active')
    airport = django_filters.rest_framework.BooleanFilter(field_name='airport')
    end_date = django_filters.rest_framework.DateFilter(field_name='end_date', lookup_expr='lte')
    name = django_filters.rest_framework.CharFilter(field_name='name', lookup_expr='startswith')
    official = django_filters.rest_framework.BooleanFilter(field_name='official')
    slug = django_filters.rest_framework.CharFilter(field_name='slug', lookup_expr='exact')
    start_date = django_filters.rest_framework.DateFilter(field_name='start_date', lookup_expr='gte')
    class Meta:
        model = Event
        fields = ['active', 'airport', 'end_date', 'name', 'official', 'slug', 'start_date',]

class CityFilter(django_filters.rest_framework.FilterSet):
    country = django_filters.rest_framework.CharFilter(field_name="country__name", lookup_expr='iexact')
    name = django_filters.rest_framework.CharFilter(field_name="name", lookup_expr='istartswith')
    region = django_filters.rest_framework.CharFilter(field_name="region__name", lookup_expr='iexact')
    class Meta:
        model = City
        fields = ['country', 'name', 'region',]

class AirportFilter(django_filters.rest_framework.FilterSet):
    country = django_filters.rest_framework.CharFilter(field_name="iso_country__name", lookup_expr='iexact')
    ident = django_filters.rest_framework.CharFilter(field_name="ident", lookup_expr='iexact')
    name = django_filters.rest_framework.CharFilter(field_name="name", lookup_expr='istartswith')
    region = django_filters.rest_framework.CharFilter(field_name="iso_region__name", lookup_expr='iexact')
    #type = django_filters.rest_framework.CharFilter(field_name="type", lookup_expr='iexact')
    class Meta:
        model = Airport
        #fields = ['country', 'ident', 'name', 'region', 'type',]
        fields = ['country', 'ident', 'name', 'region',]

class FaqFilter(django_filters.rest_framework.FilterSet):
    language = django_filters.rest_framework.CharFilter(field_name="language", lookup_expr='iexact')
    class Meta:
        model = FAQ
        fields = ['language',]

# Views
@api_view(('GET',))
class APIRoot(GenericAPIView):
    """
    View to handle '/' url.
    """
    model = None

class HelpMessageView(APIView):
    """ Endpoint for help message
    """
    #renderer_classes = (JSONPRenderer, JSONRenderer)
    permission_classes = (permissions.AllowAny,)


    #@method_decorator(ensure_csrf_cookie)
    def post(self, request, format=None):
        serializer = HelpMessageSerializer(data=request.data)

        if serializer.is_valid():
            sd = serializer.validated_data
            user_email = sd.get('email')
            subject = sd.get('subject')
            message = sd.get('message')

            # honeypot!  zipcode should be false
            zipcode = sd.get('zipcode')
            if zipcode:
                # Lie about it!
                log.warn('Mail message abuse: %s', serializer.validated_data)
                return Response({'success': True}, status=status.HTTP_200_OK)


            # Some values we'll use below
            help_admins = [email for name, email in settings.HELP_ADMINS] 
            from_email = settings.DEFAULT_FROM_EMAIL
            reply_to = [user_email]
            mail_subject = '[{}] {}'.format(settings.HELP_SUBJECT, subject)
            receipt_subject = 'Re: {}'.format(subject)
            receipt_message = '{}\n\n-----\n\n{}'.format(settings.HELP_RECEIPT, message)

            remote_addr = request.META.get('REMOTE_ADDR')
            user_agent_str = request.META.get('HTTP_USER_AGENT')

            # Send mail to HELP_ADMINS
            help_msg = EmailMessage()
            help_msg.subject = mail_subject
            help_msg.from_email = from_email
            help_msg.to = help_admins
            help_msg.reply_to = reply_to
            help_msg.body = message

            # Send receipt mail to user
            rcpt_msg = EmailMessage()
            rcpt_msg.subject = receipt_subject
            rcpt_msg.from_email = from_email
            rcpt_msg.to = [user_email]
            rcpt_msg.body = receipt_message
            rcpt_msg.send()

            for msg in (help_msg, rcpt_msg):
                try:
                    msg.send()
                    log.info('Help Message: %s, %s at %s (%s)',
                        subject, user_email, request.META['REMOTE_ADDR'], user_agent_str)
                except AnymailRecipientsRefused as e:
                    log.error('Failed to send Rideshare Message: %s', e.describe_response)
                    return Response({'error': e}, status=status.HTTP_502_BAD_GATEWAY)
                except smtplib.SMTPException as e:
                    log.error('Failed to send Rideshare Message: %s', e)
                    return Response({'error': e}, status=status.HTTP_502_BAD_GATEWAY)
                except Exception as e:
                    log.error('Failed to send Rideshare Message: %s', e)
                    return Response({'error': e}, status=status.HTTP_502_BAD_GATEWAY)

            response = Response({'success': True}, status=status.HTTP_200_OK)

        else:
            log.error('serializer errors: %s', serializer.errors)
            response = Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return response

class LogoutView(APIView):
    """ Endpoint for user logout.
    """
    #renderer_classes = (JSONPRenderer, JSONRenderer)
    #permission_classes = (permissions.IsAuthenticated,)

    #@method_decorator(ensure_csrf_cookie)
    def post(self, request, format=None):
        log.debug('Logging out...')
        logout(request)
        # set the Token header to None
        data = {'isLogged': False}
        response = Response(data, status=status.HTTP_200_OK)
        response['Token'] = None
        return response

class LoginView(APIView):
    """ Endpoint for user login. Supports multiple authentication modes:
        This is only for POST from the JS frontend.
        1. Standard authentication against Django auth.User with
           username/password
        2. PlayaAuthentication based on a password and an object.  The
           password must authenticate the username of the owner of the
           object.
        3. TODO Add OAuth here?
    """
    renderer_classes = (JSONRenderer,)
    permission_classes = (permissions.AllowAny,)

    #@method_decorator(ensure_csrf_cookie)
    def post(self, request, format=None):
        # Basically, this just checks the required fields and field length
        # since this is not a model, just a dictionary.
        serializer = LoginSerializer(data=request.data)
        #log.debug('%s', serializer)

        if serializer.is_valid():
            sd = serializer.validated_data
            username = sd.get('username')
            password = sd.get('password')
            obj_cls = sd.get('obj_cls')
            obj_id = sd.get('obj_id')

            if username and password:
                # Standard Django username/password authentication
                try:
                    auth_user = authenticate(username=sd['username'], password=sd['password'])
                except:
                    log.info('Unauthorized username/password: %s/%s from %s',
                            username, password, request.META['REMOTE_ADDR'])
                    raise RideshareAPIException('Unauthorized')

            elif password and obj_cls and obj_id:
                # Special PlayaAuthentication using password and looking up user
                # associated with Rideshare object.

                # Check class string against list of potential classes
                # Currently only Rideshare is allowed, so this is essentially a
                # constant.
                if obj_cls in ('Rideshare'):
                    cls = eval(obj_cls)
                else:
                    log.error('Illegal object class')
                    raise RideshareAPIException('Illegal object class')

                obj = cls.objects.get(pk=obj_id)

                if not obj:
                    log.error('No object %s %s found' % (obj_cls, obj_id))
                    raise RideshareAPIException('No object %s %s found' % (obj_cls, obj_id))

                try:
                    try:
                        event = obj.event
                    except Exception as e:
                        log.error('Exception: %s', e)
                        raise
                    assert event.auth_type.name == 'playa'
                    # authenticate user based on owner of obj and supplied password
                    auth_user = authenticate(username=obj.user.username, password=password)
                except AssertionError:
                    log.error('Wrong auth type for this event: %s != %s', event.auth_type)
                    raise RideshareAPIException('Wrong auth type for this event: %s', event.auth_type)
            else:
                log.error('Illegal authentication parameters: %s', sd)
                raise RideshareAPIException('Illegal authentication parameters')


            if auth_user:
                if auth_user.is_active:
                    login(request, auth_user)
                    try:
                        # might be able to just use UserSerializer here
                        try:
                            #user_profile = Profile.objects.get(user=auth_user)
                            #serializer = UserProfileSerializer(user_profile)
                            serializer = UserSerializer(auth_user)
                        except Exception as e:
                            serializer = UserSerializer(auth_user)

                        user = [serializer.data, {'isLogged': True}]

                        # Set the Token in the response header.
                        # The Access-Control line is critical for CORS functionality!
                        response = Response(user, status=status.HTTP_200_OK)
                        response['Token'] = str(auth_user.auth_token)
                        response['Access-Control-Expose-Headers'] = 'Token'
                    except Exception as e:
                        serializer = UserSerializer(auth_user)
                        user = [serializer.validated_data, {'isLogged': True}]
            else:
                log.info('Unauthorized password: %s/%s from %s',
                        auth_user, password, request.META['REMOTE_ADDR'])
                user = {'isLogged': False}
                response = Response(user, status=status.HTTP_401_UNAUTHORIZED)

            return response

        log.debug('serializer errors: %s', serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UsageView(APIView):
    """
    View to handle '/usage' endpoint.
    This reports raw usage statistics, simply the total number of rideshares by country/region/city.
    It does not distinguish between flights/rides or between offers/requests or filled.
    Usage endpoint takes these arguments:
        country         filter by country (two character abbreviation, e.g. "US"
        event           filter by event (use the event slug, e.g. "&event=burning-man-2014")
        region          filter by region (exact match: e.g. state name -- "&region=California")
        name            filter by name (exact match: e.g. city name -- "&name=San Francisco")
        values          CSV fields on which to aggregate Rideshare count.
                        Can be any combination of country,region,name but you probably
                        want the more general ones if you are including the specific ones:
                        "&values=country", "&values=country,region", "&values=country,region,name".
                        If you filter by name, you probably want to include name in values.

    """
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    def get(self, request, *args, **kwargs):
        # Set of allowed vars
        valid_http_vars = {
            'country',
            'event',
            'format', # ignore; this is set by DRF interface
            'name',
            'region',
            'values',
        }
        # Catch any vars that we don't recognize
        try:
            query_param_set = set(request.query_params.keys())
            assert(query_param_set <= valid_http_vars)
        except AssertionError:
            msg = ', '.join(query_param_set - valid_http_vars)
            raise AssertionError('Invalid parameter(s): %s' % msg)

        event_slug = request.query_params.get('event', None)
        country = request.query_params.get('country', None)
        name = request.query_params.get('name', None)
        region = request.query_params.get('region', None)
        values = request.query_params.get('values', None)
        if values:
            values = [v.lower().strip() for v in values.split(',')]
        else:
            values = ['country', 'region', 'name']

        # Filter by these, if present
        extra_kwargs = {}
        if event_slug:
            extra_kwargs['rideshare__event__slug'] = event_slug
        if country:
            extra_kwargs['country'] = country
        if region:
            extra_kwargs['region'] = region
        if name:
            extra_kwargs['name'] = name

        # Start with location objects associated with rideshares for events that are active.
        # Then filter based on event, country, region and name.
        # The values are a selection of country, region and name to aggregate on, and return in json.
        locations = Location.objects.filter(rideshare__deleted=False, **extra_kwargs)
        locations = locations.values(*values)
        locations = locations.annotate(num_rideshares=Count('rideshare')).order_by('-num_rideshares')
        locations = list(locations)

        # Nest converts the flat list of country, region, city and num_rideshares
        # into a nested structure.
        nest = nesting.Nest().key('country').key('region')

        # Convert the nested structure into something D3 understands
        root = {'name': 'rideshare', 'children': []}
        for loc in nest.entries(locations):
            country, region_values = loc
            countries = {'name': country, 'children':[]}
            for region, usage_values in region_values:
                regions = { 'name': region, 'children': [] }
                for usage in usage_values:
                    regions['children'].append({'name':usage['name'], 'size':usage['num_rideshares']})
                countries['children'].append(regions)
            root['children'].append(countries)
        return Response(root, status=status.HTTP_200_OK)

class AggregateKeyConstructor(DefaultKeyConstructor):
    query_params = QueryParamsKeyBit('*')

class AggregateView(APIView):
    """
    View to handle '/aggregate' url.
    """
    model = Aggregate
    #authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    @cache_response(3600, key_func=AggregateKeyConstructor(),
            cache_errors=False)
    def get(self, request, *args, **kwargs):
        airport = _get_boolean(request.query_params.get('airport', None))
        event = request.query_params.get('event', None)
        official = _get_boolean(request.query_params.get('official', None))
        stats = Aggregate(airport, event, official).stats()
        serializer = StatSerializer(stats, many=True)
        response = Response(serializer.data, status=status.HTTP_200_OK)
        return response

class ContactViewSet(viewsets.ViewSet):
    """ Current behavior is to request authentication, but not require it.
        This is consistent with behavior in existing Rideshare application.

        In the list (GET) method, we explicitly check that the user making
        the request is authenticated *and* is the owner of the Rideshare
        object.

        In the create (POST) method, no authentication is required.  Any
        user may create a Contact.  This is governed by overriding
        permission_classes, with AllowAny permission.
    """
    model = Contact
    paginate_by = 10
    authentication_classes = (TokenAuthentication, PlayaAuthentication)
    #permission_classes = (permissions.AllowAny,)
    permission_classes = (IsOwnerOrReadOnly,)
    #filter_class = IsOwnerFilterBackend
    #filter_backends = (IsOwnerFilterBackend,)

    def list(self, request):
        rsid = request.query_params.get('rsid', None)
        if not rsid:
            return Response('Missing rsid parameter',
                status=status.HTTP_400_BAD_REQUEST)

        # NB Only list Contacts related to the request.user's Rideshare object.
        # This is an additional "permission" requirement.
        rs = Rideshare.objects.get(id=rsid)
        self.check_object_permissions(request, rs)

        queryset = Contact.objects.filter(
                rideshare=rs,
                rideshare__user=request.user).order_by('-created')
        serializer = ContactSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        data = request.data
        serializer = ContactSerializer(data=data, many=False)
        if serializer.is_valid():
            serializer.save()
            # Log the contact IP address in case there is any abuse
            log.info('Rideshare %d contacted from %s',
                    serializer.data['rideshare'], request.META['REMOTE_ADDR'])
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            log.error('ContactSerializer invalid: %s', serializer.errors)
            return Response(serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST)

class UserView(RetrieveAPIView):
    """ Allows the retrieval of a single user record, if authenticated.
        Authentication can be TokenAuthentication or PlayaAuthentication.
    """
    model = User
    serializer_class = UserSerializer
    authentication_classes = (TokenAuthentication, PlayaAuthentication)

    def retrieve(self, request, pk=None):
        """
        If provided 'pk' is "me" then return the current user.
        """
        if request.user and pk == 'me':
            return Response(UserSerializer(request.user).data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        #return super(UserView, self).retrieve(request, pk)

class EventViewSet(CacheResponseMixin, viewsets.ReadOnlyModelViewSet):
    """ Returns a list of active Events or an individual event.
        The primary key for an individual event is actually the
        event.slug, not the primary key.
    """
    # Make this active when putting into production!
    # For development, don't filter by active, so we have some data.
    extension = datetime.timedelta(settings.EVENT_VIABILITY_EXTENSION)
    queryset = Event.objects.filter(active=True,
            end_date__gt=timezone.now() - extension)
    serializer_class = EventSerializer
    filter_class = EventFilter
    #pagination_class = None
    #pagination_by_param = None

    def retrieve(self, request, pk=None):
        ev = get_object_or_404(Event, slug=pk)
        serializer = EventSerializer(ev)
        return Response(serializer.data)


class RideTypeViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = RideType.objects.all().order_by('id')
    serializer_class = RideTypeSerializer
    pagination_class = None
    pagination_by_param = None

class PreferenceViewSet(viewsets.ReadOnlyModelViewSet):
    # just return non-smoking, smoking, accommodate disabled passngers and open
    # minded, the other ones are now deprecated, but we can leave the data
    # there for past events.
    #queryset = Preference.objects.filter(id__in=(19,20,23,24)).order_by('order')
    queryset = Preference.objects.all().order_by('order')
    serializer_class = PreferenceSerializer
    pagination_class = None
    pagination_by_param = None

class FaqViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = FAQ.objects.all().order_by('order')
    filter_class = FaqFilter
    serializer_class = FaqSerializer
    pagination_class = None
    pagination_by_param = None

class UpdatedAtKeyBit(KeyBitBase):
    def get_data(self, **kwargs):
        key = 'api_updated_at_timestamp'
        value = cache.get(key, None)
        #log.debug('GETTING key: %s == %s', key, value)
        if not value:
            value = timezone.now()
            #log.debug('SETTING key: %s == %s', key, value)
            cache.set(key, value=value)
        ret = force_text(value)
        #log.debug('FORCING key: %s == %s', key, ret)
        return ret

class RideshareKeyConstructor(DefaultKeyConstructor):
    retrieve_sql = RetrieveSqlQueryKeyBit()
    user = UserKeyBit()
    updated_at = UpdatedAtKeyBit()

class RideshareListKeyConstructor(DefaultKeyConstructor):
    retrieve_sql = ListSqlQueryKeyBit()
    user = UserKeyBit()
    query_params = QueryParamsKeyBit('*')
    updated_at = UpdatedAtKeyBit()

class RidesharePaginator(PageNumberPagination):
    page_query_param = 'pageNumber'
    page_size_query_param = 'pageSize'

class RideshareViewSet(viewsets.GenericViewSet):
    """
    Rideshares!

    Filters:
    direction lookup_expr='exact'
    location lookup_expr='exact'
    riders lookup_expr='exact'
    slug lookup_expr='exact'

    """
    # paginate False is for infinite scrolling in the client 
    # paginate True uses PageNumberPagination
    paginate = True

    model = Rideshare
    queryset = Rideshare.objects.filter(
                active=True, deleted=False).all().select_related()
    serializer_class = RideshareSerializer
    pagination_class = RidesharePaginator

    # first check for token, then check to see if the Rideshare object's
    # creator's (i.e. the user's) password matches a supplied password.
    authentication_classes = (TokenAuthentication, PlayaAuthentication)
    permission_classes = (IsOwnerOrReadOnly,)

    # TODO: redundant?  probably can remove
    filter_class = RideshareFilter
    filter_backends = (filters.SearchFilter, django_filters.rest_framework.DjangoFilterBackend)
    search_fields = ('contact_name', 'contact_info')

    # this is a set!
    valid_http_vars = {
        'airport',
        'city',
        'direction',
        'flight',
        'hauling',
        'name',
        'pageIndex',
        'pageNumber',
        'pageSize',
        'radius',
        'region',
        'ride',
        'riders',
        'round_trip',
        'rtype',
        'slug',
        'sortInfo',
        'format',
        }

    #def get_object(self):
        #queryset = self.get_queryset()
        #log.debug('queryset', queryset)
        #obj = get_object_or_404(queryset)
        #log.debug('obj', obj)
        #return obj

    @cache_response(key_func=RideshareListKeyConstructor(), cache_errors=False)
    def list(self, request):
        # using get_queryset() prevents multiple requests to db, because it caches
        queryset = self.get_queryset().filter(full=False)
        order_by = request.query_params.get('sortInfo', [])

        # Note that pageNumber and pageIndex are mutually exclusive, and are
        # used for paginating and scrolling, respectively.
        # pageNumber and pageSize are now not used, due to new DRF pagination,
        # see RidesharePaginator, above.  However, we can leave pageSize here,
        # in case we go back to infinite scrolling.
        page = request.query_params.get('pageNumber', 1)
        page_size = int(request.query_params.get('pageSize', 10))
        page_index = int(request.query_params.get('pageIndex', 0))

        city = request.query_params.get('city', None)
        airport = request.query_params.get('airport', None)

        direction = request.query_params.get('direction', None)
        hauling = request.query_params.get('hauling', None)
        ride = request.query_params.get('ride', None)
        round_trip = request.query_params.get('round_trip', None)
        rtype = request.query_params.get('rtype', None)

        flight = request.query_params.get('flight', None)
        name = request.query_params.get('name', None)
        radius = float(request.query_params.get('radius', SEARCH_RADIUS))
        region = request.query_params.get('region', None)
        riders = request.query_params.get('riders', None)
        slug = request.query_params.get('slug', None)

        # Order by most recently modified, then id (for test cases where
        # modified time might be identical).
        # If there is a location, then order by distance from that first.
        order_by.extend(['-modified', 'id'])
        if city or airport:
            order_by.insert(0, 'distance')

        try:
            # catch any vars that we don't recognize
            try:
                query_param_set = set(request.query_params.keys())
                assert(query_param_set <= self.valid_http_vars)
            except AssertionError:
                msg = ', '.join(query_param_set - self.valid_http_vars)
                raise AssertionError('Invalid parameter(s): %s' % msg)

            if rtype not in (None, ''):
                assert int(rtype) in (0, 1)
                queryset = queryset.filter(rtype=rtype)
            if direction not in (None, ''):
                assert int(direction) in (0, 1)
                queryset = queryset.filter(direction=direction)
            if round_trip not in (None, ''):
                assert int(round_trip) in (0, 1)
                queryset = queryset.filter(round_trip=round_trip)
            if ride not in (None, ''):
                assert int(ride) in (0, 1)
                queryset = queryset.filter(ride=ride)
            if hauling not in (None, ''):
                assert int(hauling) in (0, 1)
                queryset = queryset.filter(hauling=hauling)
            if riders not in (None, ''):
                # If we know which kind of ride type it is, we can make it
                # a bit more intelligent.  If searching for requests, then
                # include results with rs.riders <= riders, if searching for
                # offers, include results for rs.riders => riders.
                if rtype not in (None, ''):
                    if rtype.lower() == '0': # it's a request
                        queryset = queryset.filter(riders__gte=riders)
                    elif rtype.lower() == '1': # it's an offer
                        queryset = queryset.filter(riders__lte=riders)
                else:
                    queryset = queryset.filter(riders=riders)
            if name: queryset = queryset.filter(name__icontains=name)
            if slug: queryset = queryset.filter(event__slug=slug)
            if region: queryset = queryset.filter(region__slug=region)
            if flight not in (None, ''):
                # convert the JSON boolean string to Python boolean value
                queryset = queryset.filter(flight=_get_boolean(flight))

            # here are some conditions that should not happen
            # TODO: write db integrity check for this sort of condition
            if ride == '0' and riders not in (None, '') and riders <= 0:
                raise ValueError('Passenger ride without any riders!')
            if ride == '0' and hauling == '0':
                raise ValueError('Neither passenger nor hauling!')

        except Exception as e:
            error_msg = 'Parsing query args: %s' % str(e)
            log.error(error_msg)
            raise RideshareAPIException(error_msg)

        if airport:
            airport = Airport.objects.get(id=airport)
            qscopy = copy.deepcopy(queryset)

            bbox = bbox_from_point(airport.location, radius)
            log.debug('Searching within %sK of %s', radius, airport)
            queryset = queryset.filter(location__location__contained=bbox)
            qs_count = queryset.count()
            log.debug('Found %s Flightshares within %sK', qs_count, radius)
            if qs_count < MIN_SEARCH_RESULT:
                log.debug('Expanding search in region %s', airport.iso_region)
                queryset = qscopy.filter(
                    location__city__region__name=airport.iso_region.name,
                    location__city__country__code=airport.iso_country.code)
                qs_count = queryset.count()
                log.debug('Found %s Flightshares', qs_count)
                if qs_count < MIN_SEARCH_RESULT:
                    log.debug('Expanding search in country %s', airport.iso_country)
                    queryset = qscopy.filter(location__city__country__code=airport.iso_country.code)
                    log.debug('Found %s Flightshares', queryset.count())

            queryset = queryset.distance(airport.location,
                    field_name = 'location__location')
        elif city:
            city = City.objects.get(id=city)

            qscopy = copy.deepcopy(queryset)

            # NOTE: Try using dwithin for this, instead of bbox.
            bbox = bbox_from_point(city.location, radius)
            log.debug('Bounding Box: %s', bbox)
            log.debug('Searching within %sK of %s', radius, city)
            queryset = queryset.filter(location__location__contained=bbox)
            qs_count = queryset.count()
            log.debug('Found %s Rideshares within %sK', qs_count, radius)
            if qs_count < MIN_SEARCH_RESULT:
                log.debug('Expanding search in region %s', city.region)
                queryset = qscopy.filter(location__city__region=city.region)
                qs_count = queryset.count()
                log.debug('Found %s Rideshares', qs_count)
                if qs_count < MIN_SEARCH_RESULT:
                    log.debug('Expanding search in country %s', city.country)
                    queryset = qscopy.filter(location__city__country=city.country)
                    log.debug('Found %s Rideshares', queryset.count())

            queryset = queryset.annotate(distance=Distance('location__location', city.location))

        queryset = queryset.order_by(*order_by)

        if self.paginate:
            rideshares = self.paginator.paginate_queryset(queryset, request)
            serializer_context = {'request': request}
            serializer = RideshareSerializer(rideshares, context=serializer_context, many=True)
            return self.paginator.get_paginated_response(serializer.data)
        else:
            # This was used for infinite scrolling -- if we go back to that
            # it will need to be reworked. 
            # Strangely, the ui-scroll control index is 1-relative,
            # adjust for that here.
            page_index -= 1;

            # Slice based on index and size.
            # Apparently, it's important to handle the negative indexes by
            # pretending that the negative index exists.
            if page_index >= 0:
                queryset = queryset[page_index:page_index+page_size]
            elif page_index + page_size > 0:
                queryset = queryset[0:page_index+page_size]
            else:
                queryset = Rideshare.objects.none()

            serializer_context = {'request': request}
            serializer = OffsetRideshareSerializer(dict(count=queryset.count(),
                    results=queryset), context=serializer_context)
            serializer_data = serializer.data

#       log.warning('Actual Count: %s (%s)', len(serializer.data['results']),
#           [x.get('id') for x in serializer.data['results']])


    @detail_route(methods=['GET'])
    def force_auth(self, request, pk=None):
        """ In this method we want to force the user to authenticate prior
            to sending the Rideshare object.  The default permissions won't
            do that, so we force the HTTP_401_UNAUTHORIZED response here.

            This is used when the user has initiated an edit operation -- we
            need to make sure they will be able to save the object before
            starting editing.
        """
        if ((request.user and not request.user.is_authenticated) or 
             not request.user):
            return Response('Forced authentication',
                status=status.HTTP_401_UNAUTHORIZED)

        # if they have authenticated, then return object, as normal
        return self.retrieve(request, pk)

    @cache_response(key_func=RideshareKeyConstructor(), cache_errors=False)
    def retrieve(self, request, pk=None):
        # use select_related to reduce number of db requests
        queryset = Rideshare.objects.all().select_related('user', 'location',
        'event', 'event__location', 'event__auth_type', 'event__regional',
        'direction', 'round_trip', 'ride', 'hauling', 'rtype')
        rs = get_object_or_404(queryset, pk=pk, deleted=False, active=True)
        # Check for Token here.  If token is valid for this object then
        # use serializer that will include email address, else, the default 
        # will use the md5 digest of the email.
        try:
            self.check_object_permissions(request, rs)
            serializer = RideshareFullSerializer(rs, cleartext_private_fields=True)
        except APIException:
            serializer = RideshareFullSerializer(rs)
        return Response(serializer.data)

    def pre_save(self, validated_data, password):
        """ For PlayaAuthentication a new user is created for each new 
            Rideshare.  
            Probably need to double-check the event here for which type of 
            authentication is being used.
            Let's try to check the authentication earlier (in the serializer?)
        """
        if not self.request.user or self.request.user.is_anonymous:
            username = _generate_username(validated_data.get('name', None))
            user = User.objects.create_user(username)
            user.set_password(password)
            if validated_data.get('contact_email', None):
                user.email = validated_data.get('contact_email')
            user.save()
        else:
            user = self.request.user
        return user

    def create(self, request):
        data = request.data
        #log.debug('Token: %s', request.META.get('HTTP_AUTHORIZATION'))
        #print pprint.pprint(data, indent=4)
        serializer = RideshareFullSerializer(data=data, many=False)
        if serializer.is_valid(raise_exception=False):
            password = data.get('password', None)
            if password != data.get('password2', None):
                error_msg = 'Password mismatch!'
                log.error(error_msg)
                raise RideshareAPIException(error_msg)
            user = self.pre_save(serializer.validated_data, password)
            rideshare = serializer.save(user=user)
            log.info('Rideshare %d created from %s', rideshare.id, request.META['REMOTE_ADDR'])
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            log.error('RideshareFullSerializer errors: %s', serializer.errors)
            log.error('RideshareFullSerializer non_field_errors: %s',
                    hasattr(serializer, 'non_field_errors') and
                    serializer.non_field_errors or None)
            return Response(serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST)

    # update() and partial_update() correspond to PUT and PATCH
    def update(self, request, pk=None):
        return self._update(request, pk=pk, partial=False)
    def partial_update(self, request, pk=None):
        return self._update(request, pk=pk, partial=True)

    def _update(self, request, pk=None, partial=False):
        data = request.data
        self.object = Rideshare.objects.get(pk=pk)
        serializer = RideshareFullSerializer(self.object,
                data=data, partial=partial, many=False,
                cleartext_private_fields=True)

        if serializer.is_valid(raise_exception=False):
            # check that the user has authenticated and is the
            # object's owner
            self.check_object_permissions(request, self.object)

            # If a pasword/password2 pair is supplied, then (re)set password
            password = data.get('password', None)
            if password:
                password2 = data.get('password2', None)
                if password != password2:
                    error_msg = 'Password mismatch!'
                    log.error(error_msg)
                    raise RideshareAPIException(error_msg)
                self.object.user.set_password(password)
                self.object.user.save()

            serializer.save(force_update=True)

            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            log.error('RideshareFullSerializer update errors: %s', serializer.errors)
            log.error('RideshareFullSerializer update non_field_errors: %s',
                    hasattr(serializer, 'non_field_errors') and serializer.non_field_errors or None)
            return Response(serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        if pk:
            rs = Rideshare.objects.get(id=pk)
            self.check_object_permissions(request, rs)
            rs.deleted = True
            rs.save()
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response('No primary key',
                    status=status.HTTP_400_BAD_REQUEST)


class RegionalKeyConstructor(DefaultKeyConstructor):
    query_params = QueryParamsKeyBit('*')

class RegionalViewSet(viewsets.ReadOnlyModelViewSet):
    """ Regionals correspond to the Regional entities which
        are creating regional events.
    """
    queryset = Regional.objects.all()
    serializer_class = RegionalSerializer
    filter_class = RegionalFilter

    @cache_response(600, key_func=RegionalKeyConstructor(),
            cache_errors=False)
    def retrieve(self, request, pk=None):
        rn = get_object_or_404(Regional, slug=pk)
        serializer = RegionalSerializer(rn)
        return Response(serializer.data)

class LocationViewSet(CacheResponseMixin, viewsets.ReadOnlyModelViewSet):
    """ Locations present an abstract notion of a place that may be
        1) the city associated with a Rideshare OR
        2.1) the city associated with an Event OR
        2.2) an airport associated with an Event
    """
    queryset = Location.objects.all()
    serializer_class = LocationSerializer

class AutoCityViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """ Lookup city by name using Elasticsearch.  This is designed for
        use with typeahead auto-complete functionality.

        There are actually two queries performed:
        1. A completion suggester query of existing Locations.
        2. A completion suggester against Cities.

        In both cases (Locations and Cities) the result set is limited
        to the countries in Event.country_search_contexts or if that is
        empty, to the continent on which the event is being held.

        The return is serialized by the AutoComboSerializer, which combines
        the lists and returns JSON objects containing the
        city ids.
    """
    serializer_class = AutoComboSerializer

    def get_queryset(self, *args, **kwargs):
        req = self.request
        suggestion_list = city_list = []
        q = req.GET.get('name')

        if q is not None:
            country_codes, continent_code = get_event_search_contexts(req)
            try:
                suggestions = LocationDocument.suggestions(q,
                    country_ctx=country_codes, continent_ctx=continent_code)
                log.debug('Suggest: %s', suggestions)
                suggestion_list = list(suggestions)
                log.debug('Suggest: %s', suggestion_list)
            except NotFoundError as e:
                log.error('Location index on ElasticSearch was not found: %s', e)
                return []
            except ConnectionError as e:
                log.error('Elasticseach ConnectionError... Is Elasticsearch running?')
                return []
            except ElasticsearchException as e:
                log.error('%s', e)
                return []

            suggestion_count = len(suggestion_list)

            try:
                city_suggestion_ids = [c._source.loc_obj_id for c in suggestion_list]
                log.debug('city_suggestion_ids: {}'.format(city_suggestion_ids))
            except Exception as e:
                city_suggestion_ids = []
                log.error('%s', e)

            # If we have > 10 Location suggestions then this doesn't get executed
            if suggestion_count <= 10:
                # Get cities but filter out the ones we got from Location search
                city_list = CityDocument.suggestions(q, country_ctx=country_codes, continent_ctx=continent_code)
                city_list = [c for c in city_list if int(c._id) not in city_suggestion_ids]
                city_list = list(city_list[:(10 - suggestion_count)])

            log.debug('Retrieved {} Locations and {} Cities'.format(
                    suggestion_count, len(city_list)))

        return suggestion_list + city_list

class AutoAirportViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """ Lookup airport by name/ident using Elasticsearch.  This is designed for
        use with typeahead auto-complete functionality.
    """
    serializer_class = AutoAirportSerializer

    def get_queryset(self, *args, **kwargs):
        req = self.request
        airport_list = []
        q = req.GET.get('name')

        if q is not None:
            country_codes, continent_code = get_event_search_contexts(req)
            try:
                airport_list = AirportDocument.suggestions(q, fuzzy={'fuzziness':0},
                        continent_ctx=continent_code, country_ctx=country_codes)
            except NotFoundError as e:
                log.error('Airport index on ElasticSearch was not found: %s', e)
                return []
            except ConnectionError as e:
                log.error('Elasticseach ConnectionError... Is Elasticsearch running?')
                return []
            except ElasticsearchException as e:
                log.error('%s', e)
                return []

            airport_list = airport_list[:10]

        return airport_list

class AutoRideshareViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """ Lookup Rideshare by name using Elasticsearch.  This is designed for
        use with typeahead auto-complete functionality.
        Name and slug are required params, flight is optional.
    """
    serializer_class = AutoRideshareSerializer

    def get_queryset(self, *args, **kwargs):
        req = self.request

        rideshare_list = []

        if req.GET.get('name') is not None:
            q = req.GET.get('name')
            slug = req.GET.get('slug')
            flight = req.GET.get('flight', None)

            # Defaults to returning max of 10 results
            try:
                rideshare_list = RideshareDocument.match(q, event_slug=slug, flight=flight)
            except NotFoundError as e:
                log.error('Rideshare index on ElasticSearch was not found: %s', e)
                return []
            except ConnectionError as e:
                log.error('Elasticseach ConnectionError... Is Elasticsearch running?')
                return []
            except ElasticsearchException as e:
                log.error('%s', e)
                return []

        return rideshare_list

class CityViewSet(CacheResponseMixin, viewsets.ReadOnlyModelViewSet):
    """ Cities endpoint.  But see AutoCityViewSet if using typeahead.
            Filters:
                country lookup_expr='iexact'
                name lookup_expr='istartswith'
                region lookup_expr='iexact'
    """
    queryset = City.objects.all().order_by('name')
    serializer_class = CitySerializer
    filter_class = CityFilter
    filter_backends = (filters.SearchFilter, django_filters.rest_framework.DjangoFilterBackend)
    search_fields = ['name']
    paginate_by = 30

class AirportViewSet(CacheResponseMixin, viewsets.ReadOnlyModelViewSet):
    """ Airports endpoint.  But see AutoAirportViewSet if using typeahead.
            Filters:
                country lookup_expr='iexact'
                ident lookup_expr='iexact'
                name lookup_expr='istartswith'
                region lookup_expr='iexact'
    """
    #queryset = Airport.objects.exclude(type='closed').order_by('name')
    queryset = Airport.objects.all().order_by('name')
    serializer_class = AirportSerializer
    filter_class = AirportFilter
    filter_backends = (filters.SearchFilter, django_filters.rest_framework.DjangoFilterBackend)
    search_fields = ['name', 'ident']
    paginate_by = 30

def get_event_search_contexts(request):
    """ Get the event id from the request and extract the country_search_contexts
        if there are any.  As a fallback, use the Event's continent to limit
        the search.
    """
    event_id = request.GET.get('e_id')
    try:
        event = Event.objects.select_related('location__city__country__continent')
        event = event.prefetch_related('country_search_contexts').get(id=event_id)
    except Event.DoesNotExist:
        # This should not happen, but let's go ahead and default to North America
        log.error("Event with event id '{}' not found".format(event_id))
        return [], 'NA'
    except Exception as e:
        log.error(e)
        raise

    # Try to use list of countries to limit typeahead
    country_codes = [c.code for c in event.country_search_contexts.all()]
    # Fall back to continent code if not set
    continent_code = event.location.city.country.continent.code if not country_codes else None

    return country_codes, continent_code

