import datetime
import json
import logging
import random
import unittest
import xml.etree.ElementTree as ET
from itertools import filterfalse

from airports.models import Airport
from cities.models import City
from django.conf import settings
from django.core.cache import cache
from django.core.cache.backends import locmem
from django.core.management import call_command
from django.db import transaction
from django.urls import NoReverseMatch
from rest_framework import status
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.test import APIRequestFactory
from rest_framework.test import APITestCase

from api.factories import RideshareFactory, UserFactory, ContactFactory, DEFAULT_PASSWORD
from api.serializers import RideshareFullSerializer, ContactSerializer
from rideshare.models import (
        Event,
        Hauling,
        Location,
        Regional,
        Ride,
        Rideshare,
        RideType,
)

# logging.disable gives us cleaner output
# When running a specific test, commenting out the .disable() can give
# usefule debugging information.
log = logging.getLogger(__name__)
#logging.disable(logging.ERROR)

# NOTE: testing automatically disables the settings.DEBUG flag, setting it to
# False.  Therefore, if you want to test caching with Locmem just set it by
# hand in settings.CACHES['default'] when testing.

def setUpModule():
    """ This loads initial data for testing from a fixture.  There is also
        some basic data loaded as a result of migrations, which are also run
        prior to testing.  There are no Rideshare objects created.
        Cities
        Airports
        Site
        Regional
        Event
    """
    call_command('loaddata', 'rideshare/fixtures/api_test_data.json', verbosity=0)
    # Add Location objects for all the cities and airports
    [Location.from_obj(city) for city in City.objects.filter(r_cities__isnull=True)]
    [Location.from_obj(airport) for airport in Airport.objects.filter(r_airports__isnull=True)]

def create_initial_data():
    pass
    #call_command('loaddata', 'merged_test_data.json', verbosity=0)


class HelpMessageTestCases(APITestCase):
    def setUp(self):
        self.data = {
            'subject':'Test message',
            'email': settings.EMAIL_TEST_ADDR,
            'message': 'This is a test message'
        }

    def test_help(self):
        url = reverse('help')
        response = self.client.post(url, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class InitialDataTestCase(APITestCase):
    """ Load initial data from json file, for tests that inherit from this.
    """
    def setUp(self):
        create_initial_data()

    # tracking down some kind of integrity problem in api test data json?
    def integrity(self):
        locations = Location.objects.filter(city__isnull=False)
        for loc in locations:
            print(loc.name, loc.id)
            print(loc.city, loc.city.id)


# Miscellaneous
class MiscellaneousTests(InitialDataTestCase):
    def test_rss(self):
        url = '/rss/burning-man-2019'
        response = self.client.get(url)
        root = ET.fromstring(response.content)
        channel_title = root.find('channel').find('title').text
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(channel_title, 'Burning Rideshares', 'RSS okay?')

    def test_root(self):
        url = '/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class RideshareUsageTests(InitialDataTestCase):
    # TODO: Could add more tests here for various combinations of http vars
    def setUp(self):
        super(RideshareUsageTests, self).setUp()
        self.batch_size = 20
        self.event = Event.objects.get(slug='burning-man-2019')
        RideshareFactory.create_batch(self.batch_size,
                event=self.event, flight=False)

    def test_list_usage(self):
        """
        Ensure we can list Usage
        """
        url = reverse('usage')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_usage_event(self):
        """
        Ensure we can get Usage data by event
        """
        url = reverse('usage')
        response = self.client.get(url, {'event':self.event.slug})
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class AggregateTests(InitialDataTestCase):
    def setUp(self):
        super(AggregateTests, self).setUp()
        self.batch_size = 20
        #RideshareFactory.create_batch(self.batch_size, type=(1,3), flight=False)
        #events = Event.objects.all()
        #self.event = random.choice(events)
        self.event = Event.objects.get(slug='burning-man-2019')
        RideshareFactory.create_batch(self.batch_size,
                event=self.event, flight=False)

    def test_list_aggregates(self):
        """
        Ensure we can list Aggregates
        """
        url = reverse('aggregate')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_aggregate(self):
        """
        Ensure we can get Aggregates.  The number of offers, requests and
        filled rides should add up to the number of rideshares we created.
        """
        url = reverse('aggregate')
        response = self.client.get(url, {'event':self.event.slug})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response, Response,
            'Should be Response class.  Running memcached? Disable it.')
        self.assertEqual(self.batch_size,
            response.data[1]['direction']['to']['offers'] +
            response.data[1]['direction']['to']['requests'] +
            response.data[1]['direction']['from']['offers'] +
            response.data[1]['direction']['from']['requests'] +
            response.data[1]['filled'])


class AutoAirportTests(InitialDataTestCase):
    def test_list_autoairports(self):
        """
        Ensure we can find an Airport
        """
        url = reverse('autoairports-list')
        response = self.client.get(url, {'name':'San Rafael Airport'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response, Response,
            'Should be Response class.  Running memcached? Disable it.')
        airport_names = [a['text'] for a in response.data['results']]
        self.assertIn('San Rafael Airport, California, US (CA35)', airport_names)


class AirportTests(InitialDataTestCase):
    def setUp(self):
        super(AirportTests, self).setUp()

    def test_list_airports(self):
        """
        Ensure we can list Airports
        """
        url = reverse('airports-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_airport(self):
        """
        Ensure we can get an Airport
        16566 is the San Rafael airport
        """
        url = reverse('airports-detail', args=('16566',))
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response, Response,
            'Should be Response class.  Running memcached? Disable it.')
        self.assertEqual(response.data.get('id'), 16566)


class AutoCityTests(InitialDataTestCase):
    def test_list_autocities(self):
        """
        Ensure we can list Cities
        """
        url = reverse('autocities-list')
        response = self.client.get(url, {'name':'San Francisco, CA'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response, Response,
            'Should be Response class.  Running memcached? Disable it.')
        self.assertEqual(response.data['results'][0]['text'],
            'San Francisco, California, US')


class CityTests(InitialDataTestCase):
    def test_list_cities(self):
        """
        Ensure we can list Cities
        """
        url = reverse('city-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_city(self):
        """
        Ensure we can get a City
        """
        url = reverse('city-detail', args=('5558953',))
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response, Response,
            'Should be Response class.  Running memcached? Disable it.')
        self.assertEqual(response.data.get('id'), 5558953)


class LocationTests(InitialDataTestCase):
    def test_list_locations(self):
        """
        Ensure we can list Locations
        """
        url = reverse('location-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_location(self):
        """
        Ensure we can get Locations
        """
        location = random.choice(Location.objects.all())
        url = reverse('location-detail', args=(location.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response, Response,
            'Should be Response class.  Running memcached? Disable it.')
        self.assertEqual(location.name, response.data.get('name'))


class EventTests(InitialDataTestCase):
    def test_list_events(self):
        """
        Ensure we can list Events
        """
        url = reverse('event-list')
        response = self.client.get(url)
        self.assertIsInstance(response, Response,
            'Should be Response class.  Running memcached? Disable it.')
        # there are 4 in api test data, however this depends on 
        # today's date, so this is not the best test!.
        self.assertEqual(len(response.data), 4)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_event(self):
        """
        Ensure we can get Events
        """
        url = reverse('event-detail', args=('burning-man-2019',))
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertEqual(response.data,{'id': 8, 'name': u'Burning Man 2014', 'short_name': u'BM14', 'slug': u'burning-man-2014', 'start_date': datetime.datetime(2014, 8, 25, 0, 0), 'end_date': datetime.datetime(2014, 9, 1, 0, 0), 'location': {'id': 147, 'airport': None, 'city': {'id': 1, 'name': u'Black Rock City', 'region': {'name': u'Nevada'}, 'country': {'name': u'United States', 'code': u'US'}, 'location': {u'type': u'Point', u'coordinates': [-119.222367, 40.781581]}, 'std_name': u'Black Rock City, Nevada, US'}, 'location': {u'type': u'Point', u'coordinates': [-119.222367, 40.781581]}, 'location_type': u'City', 'name': u'Black Rock City'}, 'official': True, 'airport': True, 'auth_type': {'name': u'playa'}, 'regional': {'id': 1, 'name': u'Burning Man HQ', 'slug': u'burning-man-hq'}})


class RegionalTests(InitialDataTestCase):
    def test_list_regionals(self):
        """
        Ensure we can list Regionals
        """
        url = reverse('regional-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_regional_node(self):
        """
        Ensure we can get Regionals
        """
        regional_node = random.choice(Regional.objects.all())
        url = reverse('regional-detail', args=(regional_node.slug,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response, Response,
            'Should be Response class.  Running memcached? Disable it.')
        self.assertEqual(response.data['slug'], regional_node.slug)


class LoginTestCase(InitialDataTestCase):
    def setUp(self):
        super(LoginTestCase, self).setUp()
        self.user = UserFactory()
        self.rideshare = RideshareFactory()

    def api_login(self):
        url = reverse('login')
        data = { 'username':self.user.username, 'password':DEFAULT_PASSWORD}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.get('token'), self.user.auth_token.key)
        return response


class LoginTests(LoginTestCase):
    def test_django_login(self):
        """
        Ensure we can login
        """
        response = self.api_login()

    def test_hids_login(self):
        """
        Ensure we can login using HIDS (while High In a Drunk Storm)
        """
        url = reverse('login')
        data = { 'obj_cls':'Rideshare', 'obj_id':self.rideshare.id,
                'password':DEFAULT_PASSWORD }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response, Response,
            'Should be Response class.  Running memcached? Disable it.')
        #self.assertFalse(response.data.get('isLogged', True))
        self.assertEqual(response.get('token'), self.rideshare.user.auth_token.key)

    def test_hids_login_blank_username(self):
        """
        Ensure we can login using HIDS (while High In a Drunk Storm)
        No username.
        """
        url = reverse('login')
        data = {'username':'',
                'obj_cls':'Rideshare',
                'obj_id':self.rideshare.id,
                'password':DEFAULT_PASSWORD }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response, Response,
            'Should be Response class.  Running memcached? Disable it.')
        #self.assertFalse(response.data.get('isLogged', True))
        self.assertEqual(response.get('token'), self.rideshare.user.auth_token.key)

    @unittest.skip("OAuth not implemented.")
    def test_oauth_login(self):
        """
        Ensure we can login using OAuth
        """
        pass

    def test_logout(self):
        """
        Ensure we can logout
        """
        # first login...
        response = self.api_login()
        token = response.get('token', None)
        self.client.credentials(HTTP_AUTHORIZATION='Token '+token)

        # ...then logout
        url = reverse('logout')
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response, Response,
            'Should be Response class.  Running memcached? Disable it.')
        self.assertFalse(response.data.get('isLogged', False))
        self.assertFalse(response.get('token') == None)


class UserTests(InitialDataTestCase):
    def setUp(self):
        super(UserTests, self).setUp()
        self.user = UserFactory()

    def api_login(self):
        url = reverse('login')
        data = { 'username':self.user.username, 'password':DEFAULT_PASSWORD}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.token = response.get('token')
        self.assertEqual(self.token, self.user.auth_token.key)
        return response

    def test_list_users(self):
        """
        Ensure we /cannot/ list Users
        """
        self.assertRaises(NoReverseMatch, reverse, 'users')
        # try it anyway, just to make sure
        url = '/api/users/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_users_me(self):
        """
        Ensure we can get User "me"
        """
        response = self.api_login()

        self.assertEqual(self.token, self.user.auth_token.key)
        self.client.credentials(HTTP_AUTHORIZATION='Token '+self.token)

        url = reverse('users', args=('me',))
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_users_id(self):
        """
        Ensure we can't get Users by id
        """
        response = self.api_login()

        self.assertEqual(self.token, self.user.auth_token.key)
        self.client.credentials(HTTP_AUTHORIZATION='Token '+self.token)

        url = reverse('users', args=(self.user.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class RideshareTestCase(InitialDataTestCase):
    def setUp(self):
        super(RideshareTestCase, self).setUp()
        # Note that you have to create() (i.e. save to db) here, you can't use
        # build() because of m2m dependencies.
        self.count = 10
        self.rideshares = RideshareFactory.create_batch(self.count,
                full=False,
                ride=Ride.objects.get(key='YES'),
                hauling=Hauling.objects.get(key='NO')
                )
        #self.rideshare = self.rideshares[random.randint(0,self.count-1)]
        self.rideshare = self.rideshares[0]
        self.random_string = 'random_' + str(random.getrandbits(32))


class AutoRideshareTests(RideshareTestCase):
    @unittest.skip("Needs Elasticsearch database to exist.")
    def test_list_autorideshares(self):
        """
        Ensure we can list Rideshares
        """
        url = reverse('autorideshares-list')
        response = self.client.get(url, {'name':'Burn',
            'slug':self.rideshare.event.slug, 'flight':False})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Currently, 30 is the number of responses returned
        self.assertIsInstance(response, Response,
            'Should be Response class.  Running memcached? Disable it.')
        self.assertEqual(response.data['count'], 30, 'Is Elasticsearch running?')
        self.assertIn('Burn', response.data['results'][0]['content_auto'])


class RideshareTests(RideshareTestCase):
    def test_list_rideshares(self):
        """
        Ensure we can list Rideshares
        """
        url = reverse('rideshares-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_search_rideshares(self):
        """
        Ensure we can search Rideshares
        """
        base_url = reverse('rideshares-list') + '?'

        def _get_qstrings():
            # create all possible combinations of these http query vars
            qvars = ['rtype', 'direction', 'round_trip', 'ride', 'hauling']
            qvlen = len(qvars)
            # qvals looks like [['0', '0', '0', '0', '0'], ['0', etc.]]
            qvals = [list(format(x, '0%db' % qvlen)) for x in range(2**qvlen)]

            # pick out invalid cases where ride and hauling are both false
            valid_qvals = filterfalse(lambda x: x[3]=='0' and x[4]=='0', qvals)
            invalid_qvals = filter(lambda x: x[3]=='0' and x[4]=='0', qvals)

            # create lists of url query strings
            def _mkstr(v):
                return ''.join(['&%s=%s' % (x,y) for x,y in zip(qvars,v)])
            valid = list(map(_mkstr, valid_qvals))
            invalid = list(map(_mkstr, invalid_qvals))

            return valid, invalid

        valid_qstrings, invalid_qstrings = _get_qstrings()
        valid_qstrings.insert(0, 'pageNumber=1&pageSize=100')

        # test valid combinations of ride & hauling
        for s in valid_qstrings:
            url = base_url + s
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

        # test invalid combinations of ride & hauling
        for s in invalid_qstrings:
            url = base_url + s
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # test that unrecognized vars are caught
        url = base_url + 'invalid_v1=abc&invalid_v2=xyz'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # test location search (city)
        url = base_url + '&city=%s' % self.rideshare.location.city.id
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK, "Bad location in api test data: %s (%d)" % (self.rideshare.location, self.rideshare.location.id))

    def test_get_rideshare(self):
        """
        Ensure we can get Rideshares
        """
        #rsid = str(self.rs.id)
        url = reverse('rideshares-detail', args=(self.rideshare.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response, Response,
            'Should be Response class.  Running memcached? Disable it.')
        self.assertEqual(response.data['id'], self.rideshare.id)
        # These test that the serializer is obfuscating the private data
        self.assertEqual(response.data['contact_phone'], '*****')
        self.assertRegex(
                response.data['contact_email'], '^[0-9a-fA-F]{32}$')

    def test_get_rideshare_cached(self):
        """
        Ensure Rideshares are being cached
        """
        log.debug('LOCMEM CACHE: %s', locmem._caches)
        url = reverse('rideshares-detail', args=(self.rideshare.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response, Response,
            'Should be Response class.  Running memcached? Disable it.')
        self.assertEqual(response.data['id'], self.rideshare.id)

        log.debug('LOCMEM CACHE: %s', locmem._caches)


        # for testing eTags
        #response = self.client.get(url)
        #print response, dir(response)
        #self.assertEqual(response.status_code, status.HTTP_304_NOT_MODIFIED)

    def test_rideshare_cache_invalidation(self):
        """
        Ensure cache is invalidated for Rideshares that are deleted or changed.
        """
        def _log_cache():
            unique_snowflake = locmem._caches.get(settings.CACHES['default']['LOCATION'], {})
            #timestamp = unique_snowflake.pop('rs:1:api_updated_at_timestamp')
            #timestamp = pickle.loads(timestamp, encoding='bytes')
            timestamp = cache.get('api_updated_at_timestamp')
            try:
                obj_key = list(unique_snowflake.keys())
            except IndexError:
                obj_key = "<none>"
            log.debug('LOCMEM CACHE: %s %s', timestamp, obj_key)

        _log_cache()

        # the first request should cache the object
        url = reverse('rideshares-detail', args=(self.rideshare.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response, Response,
            'Should be Response class.  Running memcached? Disable it.')
        self.assertEqual(response.data['id'], self.rideshare.id)

        _log_cache()

        # Now delete it
        #self.client.force_authenticate(user=self.rideshare.user)
        #response = self._destroy_rideshare()
        #self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # Now update it -- directly, not using the API
        # The Rideshare.post_save method is called, and invalidates the cache.
        # That is, it updates the api_updated_at_timestamp cache key
        db_rideshare = Rideshare.objects.get(id=self.rideshare.id)
        db_rideshare.name = self.random_string
        db_rideshare.save()

        #self.client.force_authenticate(user=self.rideshare.user)
        #response = self._update_rideshare()
        #self.assertEqual(response.status_code, status.HTTP_200_OK)

        _log_cache()

        # Make sure the change was written to the object in db, bypassing
        # the API & caching)
        #db_rideshare = Rideshare.objects.get(id=self.rideshare.id)
        #self.assertTrue(db_rideshare.deleted)

        # Try to get it again via API.  The "name" value should be the
        # random string we updated it with above, NOT whatever the
        # RideshareFactory had already put in that field.  That indicates
        # that we've retrieved the newly updated object, not the cached version.
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response, Response,
            'Should be Response class.  Running memcached? Disable it.')
        self.assertEqual(response.data['name'], self.random_string)

        _log_cache()


    def test_get_rideshare_force_auth(self):
        """
        Ensure we get 401 at this endpoint
        """
        #rsid = str(self.rs.id)
        url = reverse('rideshares-force-auth', args=(self.rideshare.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


    def _create_with_rideshare(self, rideshare, expected_status):
        """
        Ensure we can create Rideshares
        """
        # Simulating creation by owner since we want valid values for
        # contact_email and contact_phone, which would otherwise be 
        # obfuscated.
        serializer = RideshareFullSerializer(rideshare,
                cleartext_private_fields=True)
        data = serializer.data

        # Okay, this is a hack -- using the build() method from factory in
        # setUp() to create the rideshare object without saving it won't work
        # because of m2m dependencies.  Instead, we create one and save it
        # there, and then just get rid of the pk here, so it looks like a new
        # one.
        data['id'] = None
        data['password'] = data['password2'] = DEFAULT_PASSWORD

        #xdata = data
        #xdata['earliest_departure'] = str(xdata['earliest_departure'])
        #xdata['earliest_return'] = str(xdata['earliest_return'])
        #xdata['latest_departure'] = str(xdata['latest_departure'])
        #xdata['latest_return'] = str(xdata['latest_return'])
        #xdata['event']['start_date'] = str(xdata['event']['start_date'])
        #xdata['event']['end_date'] = str(xdata['event']['end_date'])
        #print json.dumps(xdata, indent=4)
        url = reverse('rideshares-list')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, expected_status)

        # bail out here if we got an error status that was expected
        if expected_status >= 400:
            return

        self.assertIsInstance(response, Response,
            'Should be Response class.  Running memcached? Disable it.')
        self.assertEqual(response.data['name'], rideshare.name)

        response_set = set([p.get('id') for p in response.data.get('preferences')])
        rideshare_set = set([p.id for p in rideshare.preferences.all()])
        self.assertEqual(response_set, rideshare_set, msg='Preference sets should be identical')


        # Test that the serializer is obfuscating the private data
        self.assertIsInstance(response, Response,
            'Should be Response class.  Running memcached? Disable it.')
        self.assertEqual(response.data['contact_phone'], '*****')
        self.assertRegex(
                response.data['contact_email'], '^[0-9a-fA-F]{32}$')

    def test_create_rideshare(self):
        self._create_with_rideshare(
                RideshareFactory(
                    full=False,
                    riders=1,
                    ride=Ride.objects.get(key='YES'),
                    hauling=Hauling.objects.get(key='NO')
                ),
                status.HTTP_201_CREATED
            )

    def test_create_bad_1(self):
        """ Disallow rideshares that are rides without passengers """
        rs = RideshareFactory(
                full=False,
                riders=None,
                ride=Ride.objects.get(key='YES'),
                hauling=Hauling.objects.get(key='NO')
        )
        self._create_with_rideshare(rs, status.HTTP_400_BAD_REQUEST)

    def test_create_bad_2(self):
        """ Disallow rideshares that are rides without passengers """
        rs = RideshareFactory(
                full=False,
                riders=0,
                ride=Ride.objects.get(key='YES'),
                hauling=Hauling.objects.get(key='NO')
        )
        self._create_with_rideshare(rs, status.HTTP_400_BAD_REQUEST)

    def test_create_bad_3(self):
        """ Disallow rideshares that are neither rides nor hauling"""
        rs = RideshareFactory(
                full=False,
                ride=Ride.objects.get(key='NO'),
                hauling=Hauling.objects.get(key='NO')
        )
        self._create_with_rideshare(rs, status.HTTP_400_BAD_REQUEST)

    def test_create_bad_4(self):
        """ Disallow rideshares that are rides without passengers """
        rs = RideshareFactory(
                full=False,
                ride=Ride.objects.get(key='YES'),
                hauling=Hauling.objects.get(key='YES'),
                riders=0,
        )
        self._create_with_rideshare(rs, status.HTTP_400_BAD_REQUEST)

    # Apparently this is being handled correctly now, None should be valid
#    def test_create_bad_5(self):
#        """ Disallow rideshares with contact_phone=None; s/b valid or '' """
#        rs = RideshareFactory(
#                full=False,
#                ride=Ride.objects.get(key='YES'),
#                hauling=Hauling.objects.get(key='NO'),
#                contact_phone=None,
#        )
#        self._create_with_rideshare(rs, status.HTTP_400_BAD_REQUEST)
#
#    def test_create_bad_6(self):
#        """ Disallow rideshares with baggage_weight=None; s/b valid or 0 """
#        rs = RideshareFactory(
#                full=False,
#                ride=Ride.objects.get(key='YES'),
#                hauling=Hauling.objects.get(key='NO'),
#                riders_baggage_weight=None,
#        )
#        self._create_with_rideshare(rs, status.HTTP_400_BAD_REQUEST)

    def test_create_flightshare_offer(self):
        """
        Ensure we can create Flightshare
        """
        # pick a location that is an airport 
        # note that hauling, ride and rtype have to be correct, otherwise
        # it will fail validation
        location = Location.objects.filter(city__isnull=True).first()
        self.rideshare = RideshareFactory(full=False, flight=True,
                hauling=Hauling.objects.get(key='NO'),
                ride=Ride.objects.get(key='YES'),
                rtype=RideType.objects.get(key='OFFER'),
                location=location)
        # Simulating creation by owner since we want valid values for
        # contact_email and contact_phone, which would otherwise be 
        # obfuscated.
        serializer = RideshareFullSerializer(self.rideshare,
                cleartext_private_fields=True)
        data = serializer.data

        # Okay, this is a hack -- using the build() method from factory in
        # setUp() to create the rideshare object without saving it won't work
        # because of m2m dependencies.  Instead, we create one and save it
        # there, and then just get rid of the pk here, so it looks like a new
        # one.
        data['id'] = None
        data['password'] = data['password2'] = DEFAULT_PASSWORD

        url = reverse('rideshares-list')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsInstance(response, Response,
            'Should be Response class.  Running memcached? Disable it.')
        self.assertEqual(response.data['name'], self.rideshare.name)

        response_set = set([p.get('id') for p in response.data.get('preferences')])
        rideshare_set = set([p.id for p in self.rideshare.preferences.all()])
        self.assertEqual(response_set, rideshare_set, msg='Preference sets should be identical')


        # Test that the serializer is obfuscating the private data
        self.assertEqual(response.data['contact_phone'], '*****')
        self.assertRegex(
                response.data['contact_email'], '^[0-9a-fA-F]{32}$')

    def _update_rideshare(self):
        """ Change some stuff, and then check it.
            The password needs to be changed in the data, since it is not
            serialized.
        """
        self.rideshare.name = self.random_string
        self.rideshare.password_hint = 'Password: %s' % self.random_string
        serializer = RideshareFullSerializer(self.rideshare,
                cleartext_private_fields=True, partial=True)
        # don't want to reset the created field, of course.
        del serializer.data['created']
        data = serializer.data

        # add in a password reset
        data['password'] = self.random_string
        data['password2'] = self.random_string

        url = reverse('rideshares-detail', args=(self.rideshare.id,))
        response = self.client.put(url, data, format='json')
        return response

    def _patch_rideshare(self):
        data = {
            'name':self.random_string,
            'password':self.random_string,
            'password2':self.random_string,
            'password_hint':'Password: %s' % self.random_string,
        }
        url = reverse('rideshares-detail', args=(self.rideshare.id,))
        response = self.client.patch(url, data, format='json')
        return response

    def _rideshare_filled(self, filled):
        data = {
            'full':filled
        }
        url = reverse('rideshares-detail', args=(self.rideshare.id,))
        response = self.client.patch(url, data, format='json')
        return response

    def test_patch_rideshare_filled(self):
        """
        Ensure we can mark Rideshare as filled, method = PATCH
        """
        #log.debug('user = %s', self.rideshare.user)
        self.client.force_authenticate(user=self.rideshare.user)

        response = self._rideshare_filled(True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self._rideshare_filled(False)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_patch_rideshare_authorized(self):
        """
        Ensure we can partial update Rideshares, method = PATCH
        """
        #log.debug('user = %s', self.rideshare.user)
        self.client.force_authenticate(user=self.rideshare.user)

        response = self._patch_rideshare()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Make sure the change was written to the object in db
        rs = Rideshare.objects.get(id=self.rideshare.id)
        self.assertEqual(rs.name, self.random_string)
        self.assertEqual(rs.password_hint, 'Password: %s' % self.random_string)
        self.assertTrue(rs.user.check_password(self.random_string))

        # Make sure these fields haven't been unset
        self.assertEqual(rs.location.id, self.rideshare.location.id)
        self.assertEqual(rs.location.name, self.rideshare.location.name)
        self.assertEqual(rs.event.name, self.rideshare.event.name)
        self.assertEqual(rs.direction.key, self.rideshare.direction.key)
        self.assertEqual(rs.round_trip.key, self.rideshare.round_trip.key)
        self.assertEqual(rs.rtype.key, self.rideshare.rtype.key)
        self.assertEqual(rs.contact_info, self.rideshare.contact_info)
        self.assertEqual(rs.contact_email, self.rideshare.contact_email)
        self.assertEqual(rs.contact_phone, self.rideshare.contact_phone)

    def _patch_multi(self):
        """
        Ensure we can partial update Rideshares, method = PATCH
        """
        #log.debug('user = %s', self.rideshare.user)
        self.client.force_authenticate(user=self.rideshare.user)

        response = self._patch_rideshare()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Make sure the change was written to the object in db
        rs = Rideshare.objects.get(id=self.rideshare.id)
        self.assertEqual(rs.name, self.random_string)
        self.assertEqual(rs.password_hint, 'Password: %s' % self.random_string)
        self.assertTrue(rs.user.check_password(self.random_string))

        # Make sure these fields haven't been unset
        self.assertEqual(rs.location.name, self.rideshare.location.name)
        self.assertEqual(rs.event.name, self.rideshare.event.name)
        self.assertEqual(rs.direction.key, self.rideshare.direction.key)
        self.assertEqual(rs.round_trip.key, self.rideshare.round_trip.key)
        self.assertEqual(rs.rtype.key, self.rideshare.rtype.key)
        self.assertEqual(rs.contact_info, self.rideshare.contact_info)
        self.assertEqual(rs.contact_email, self.rideshare.contact_email)
        self.assertEqual(rs.contact_phone, self.rideshare.contact_phone)

#    def test_patch_rideshare_authorized_loop(self):
#        for i in range(0, 100):
#            self._patch_multi()

    def test_update_rideshare_authorized(self):
        """
        Ensure we can update Rideshares, method = PUT
        """
        log.debug('user = %s', self.rideshare.user)
        self.client.force_authenticate(user=self.rideshare.user)

        response = self._update_rideshare()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response, Response,
            'Should be Response class.  Running memcached? Disable it.')

        # Make sure we get the same set of preferences
        response_set = set(
                [p.get('id') for p in response.data.get('preferences')])
        rideshare_set = set(
                [p.id for p in self.rideshare.preferences.all()])
        self.assertEqual(response_set, rideshare_set,
                msg='Preference sets should be identical')

        # Make sure the change was written to the object in db
        rs = Rideshare.objects.get(id=self.rideshare.id)
        self.assertEqual(rs.name, self.random_string)
        self.assertEqual(rs.password_hint, 'Password: %s' % self.random_string)
        self.assertTrue(rs.user.check_password(self.random_string))

        # Make sure these fields haven't been unset
        self.assertEqual(rs.location.name, self.rideshare.location.name)
        self.assertEqual(rs.event.name, self.rideshare.event.name)
        self.assertEqual(rs.direction.key, self.rideshare.direction.key)
        self.assertEqual(rs.round_trip.key, self.rideshare.round_trip.key)
        self.assertEqual(rs.rtype.key, self.rideshare.rtype.key)

    def test_update_rideshare_unauthorized(self):
        """
        Ensure we /cannot/ update Rideshares
        """
        self.client.force_authenticate(user=None)
        response = self._update_rideshare()
        self.assertNotEqual(response.status_code, status.HTTP_200_OK)

        # Make sure the change was NOT written to the object in db
        rs = Rideshare.objects.get(id=self.rideshare.id)
        self.assertNotEqual(rs.name, self.random_string)

    def _destroy_rideshare(self):
        url = reverse('rideshares-detail', args=(self.rideshare.id,))
        response = self.client.delete(url, format='json')
        return response

    def test_destroy_rideshare_authorized(self):
        """
        Ensure we can delete Rideshares
        """
        self.client.force_authenticate(user=self.rideshare.user)
        response = self._destroy_rideshare()
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # Make sure the change was written to the object in db
        db_rideshare = Rideshare.objects.get(id=self.rideshare.id)
        self.assertTrue(db_rideshare.deleted)

    def test_destroy_rideshare_unauthorized(self):
        """
        Ensure we /cannot/ delete Rideshares
        """
        self.client.force_authenticate(user=None)
        response = self._destroy_rideshare()
        self.assertNotEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # Make sure the change was NOT written to the object in db
        db_rideshare = Rideshare.objects.get(id=self.rideshare.id)
        self.assertFalse(db_rideshare.deleted)


class ContactTests(InitialDataTestCase):
    def setUp(self):
        super(ContactTests, self).setUp()
        self.rideshare = RideshareFactory()
        self.contacts = ContactFactory.create_batch(1, rideshare=self.rideshare)

    def test_list_contacts_unauthorized(self):
        url = reverse('contacts-list')
        response = self.client.get(url, {'rsid':self.rideshare.id})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_list_contacts_authorized(self):
        self.client.force_authenticate(user=self.rideshare.user)
        url = reverse('contacts-list')
        response = self.client.get(url, {'rsid':self.rideshare.id})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_contact_without_created(self):
        """ Make sure creating a Contact without the 'created' field works."""
        # Unauthenticate for this test!
        self.client.force_authenticate(user=None)

        # ... and then create a contact and attach to a rideshare
        url = reverse('contacts-list')

        # Dummy data omitting ContactFactory... this doesn't have the
        # 'created' field which is the timezone-aware datetime that the
        # Contact is created.  It gets set in Contact.save().
        data = {
                'contact_email': 'testuser@example.com',
                'contact_info': 'test info',
                'contact_name': 'Test Case',
                'contact_phone': '+14151234567',
                'message': 'test msg',
                'rideshare': self.rideshare.id,
                }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_contact_missing_phone(self):
        """ Missing contact_phone while sms_notify=True is an error, but
            should not prevent creation of the Contact, since the email
            may go through.
        """
        # Unauthenticate for this test!
        self.client.force_authenticate(user=None)

        # Override with a Rideshare missing phone
        self.rideshare = RideshareFactory(sms_notify=True, contact_phone=None)

        # ... and then create a contact and attach to a rideshare
        url = reverse('contacts-list')

        # Dummy data omitting ContactFactory... this doesn't have the
        # 'created' field which is the timezone-aware datetime that the
        # Contact is created.  It gets set in Contact.save().
        data = {
                'contact_email': 'testuser@example.com',
                'contact_info': 'test info',
                'contact_name': 'Test Case',
                'contact_phone': '+14151234567',
                'message': 'test msg',
                'rideshare': self.rideshare.id,
                }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_contact_unauthorized(self):
        """ Authorization is NOT required to post a Contact,
            so this should work regardless of authenticated status.
        """
        # Unauthenticate for this test!
        self.client.force_authenticate(user=None)

        # ... and then create a contact and attach to a rideshare
        url = reverse('contacts-list')
        contact = ContactFactory()
        serializer = ContactSerializer(contact)
        data = serializer.data
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_contact_authorized(self):
        self.client.force_authenticate(user=self.rideshare.user)

        # ... and then create a contact and attach to a rideshare
        url = reverse('contacts-list')
        contact = ContactFactory()
        serializer = ContactSerializer(contact)
        data = serializer.data
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


def clip_prefix(content):
    """ Remove the AngularJS Vulnerability prefix.
        CONTENT_PREFIX = b")]}',\n"
        See djangular/middleware.py
    """
    return content.split('\n', 1)[1]

