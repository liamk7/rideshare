from django.conf.urls import url, include
from rest_framework import viewsets, routers
from rest_framework.generics import ListCreateAPIView, GenericAPIView
from rideshare.models import Rideshare

from api.views import *

# Import the authentication module so we're sure to have the create_auth_token
# signal processing for Token generation
import api.authentication

# The routing only works with ViewSets
router = routers.DefaultRouter()
router.register(r'autoairports', AutoAirportViewSet, base_name='autoairports')
router.register(r'autocities', AutoCityViewSet, base_name='autocities')
router.register(r'autorideshares', AutoRideshareViewSet, base_name='autorideshares')
router.register(r'airports', AirportViewSet, base_name='airports')
router.register(r'cities', CityViewSet)
router.register(r'contacts', ContactViewSet, base_name='contacts')
router.register(r'events', EventViewSet)
router.register(r'locations', LocationViewSet)
router.register(r'regionals', RegionalViewSet)
router.register(r'preferences', PreferenceViewSet)
router.register(r'faqs', FaqViewSet)
router.register(r'rideshares', RideshareViewSet, base_name='rideshares')
router.register(r'types', RideTypeViewSet)

urlpatterns = [
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^aggregate/?$', AggregateView.as_view(), name='aggregate'),
    url(r'^users/(?P<pk>\w+)', UserView.as_view(), name='users'),
    url(r'^login/?$', LoginView.as_view(), name='login'),
    url(r'^logout/?$', LogoutView.as_view(), name='logout'),
    url(r'^help/?$', HelpMessageView.as_view(), name='help'),
    url(r'^usage/?$', UsageView.as_view(), name='usage'),
]
urlpatterns += [
    url(r'', include(router.urls)),
    #url(r'^rideshares', RideshareList.as_view()),
]

