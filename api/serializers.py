import logging
import hashlib
import datetime

from cities.models import City, Region, Subregion, Country, Continent
from airports.models import Airport
from rest_framework.serializers import (
    CharField,
    DateTimeField,
    DictField,
    EmailField,
    FloatField,
    ImageField,
    Field,
    ReadOnlyField,
    IntegerField,
    ModelSerializer,
    PrimaryKeyRelatedField,
    RelatedField,
    Serializer,
    SerializerMethodField,
    ValidationError,
)
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone

from rideshare.models import (
    AuthType,
    Contact,
    Direction,
    Event,
    FAQ,
    Hauling,
    Location,
    Preference,
    Regional,
    Ride,
    Rideshare,
    RideType,
    RoundTrip,
)

log = logging.getLogger(__name__)

class AuthTypeSerializer(ModelSerializer):
    class Meta:
        model = AuthType
        fields = ['name']


class RoundTripSerializer(ModelSerializer):
    class Meta:
        model = RoundTrip
        fields = [ 'id', 'name', 'key']


class RideSerializer(ModelSerializer):
    class Meta:
        model = Ride
        fields = [ 'id', 'name', 'key']


class RideTypeSerializer(ModelSerializer):
    class Meta:
        model = RideType
        fields = [ 'id', 'name', 'key']


class HaulingSerializer(ModelSerializer):
    class Meta:
        model = Hauling
        fields = [ 'id', 'name', 'key']


class DirectionSerializer(ModelSerializer):
    class Meta:
        model = Direction
        fields = [ 'id', 'name', 'key', 'reverse_key']


class PreferenceSerializer(ModelSerializer):
    class Meta:
        model = Preference
        fields = [ 'id', 'name', 'key']


class FaqSerializer(ModelSerializer):
    class Meta:
        model = FAQ
        fields = [ 'id', 'question', 'answer', 'language', 'order']


class RegionSerializer(ModelSerializer):
    class Meta:
        model = Region
        fields = ['name', 'code']


class SubRegionSerializer(ModelSerializer):
    class Meta:
        model = Subregion
        fields = ['name']


class ContinentSerializer(ModelSerializer):
    class Meta:
        model = Continent
        fields = ['name', 'code']


class CountrySerializer(ModelSerializer):
    continent = ContinentSerializer()
    class Meta:
        model = Country
        fields = ['name', 'code', 'continent']


class AutoAirportSerializer(Serializer):
    id = IntegerField(required=False, source='pk')
    text = CharField()
    content_auto = CharField()
    location = CharField()

    def to_representation(self, obj):
        try:
            id = obj['_id']
            loc_type = 'airport'
            text = obj['_source']['name_composite']
        except (KeyError, TypeError):
            raise
        return { 'id': id, 'loc_type': loc_type, 'text': text }


class AutoComboSerializer(Serializer):
    """ This serializer accepts a list containing Locations and/or Cities.
    """
    def to_representation(self, obj):
        try:
            # In this case, it's a LocationDocument object
            id = obj['_source']['loc_obj_id']
            loc_type = obj['_source']['loc_type']
            text = obj['_source']['name_composite']
        except (KeyError, TypeError):
            # In this case, it's a CityDocument object
            id = obj['_id']
            loc_type = None
            text = obj['_source']['name_composite']
        return { 'id': id, 'loc_type': loc_type, 'text': text }

        # Could return whether this is a suggestion or a regular
        # query result, but currently not used.
        #return { 'id': id, 'text': text, 's':suggestion }
        #return { 'id': loc_type_id, 'type': loc_type, 'text': text }


class AutoCitySerializer(Serializer):
    id = IntegerField(required=False, source='pk')
    text = CharField()
    content_auto = CharField()
    location = CharField()

    def to_representation(self, obj):
        try:
            id = obj['_id']
            loc_type = 'city'
            text = obj['_source']['name_composite']
        except (KeyError, TypeError):
            raise
        return { 'id': id, 'loc_type': loc_type, 'text': text }


class AutoRideshareSerializer(Serializer):
    pk = IntegerField(required=False)
    text = CharField()
    content_auto = CharField()

    def to_representation(self, obj):
        pk = obj._id
        text = obj.name_description
        content_auto = obj.name_composite
        return { 'pk': pk, 'content_auto': content_auto }


class CitySerializer(ModelSerializer):
    region = RegionSerializer()
    subregion = SubRegionSerializer()
    country = CountrySerializer()
    std_name = SerializerMethodField('make_std_name')
    depth = 6

    class Meta:
        geo_field = 'location'
        model = City
        fields = (
            'id',
            'name',
            'region',
            'subregion',
            'country',
            'location',
            'std_name',
            )

    def make_std_name(self, obj):
        if not obj: return
        return ', '.join(reversed(self.names(obj)))

    def names(self, obj):
        names = []
        names.append(obj.country.code)
        try: names.append(obj.region.name)
        except: pass
        names.append(obj.name_std)
        return names


class AirportSerializer(ModelSerializer):
    region = RegionSerializer()
    country = CountrySerializer()
    city = CitySerializer()
    std_name = SerializerMethodField('make_std_name')
    depth = 3

    class Meta:
        geo_field = 'location'
        model = Airport
        fields = (
            'id',
            'name',
            'city',
            'city_name',
            'type',
            'ident',
            'region',
            'country',
            'location',
            'std_name',
            )
    def make_std_name(self, obj):
        """Get full name including hierarchy"""
        return ', '.join(reversed(self.names(obj)))

    def names(self, obj):
        """Get a hierarchy of non-null names, root first"""
        return [e for e in [
            obj.country.code,
            obj.city.region.name,
            obj.name,
        ] if e]


class LocationSerializer(ModelSerializer):
    city = CitySerializer(required=False, allow_null=True)
    airport = AirportSerializer(required=False, allow_null=True)
    # TODO Not needed? Not sure if this field is being used.
    #location_type = SerializerMethodField('get_location_type')

    def get_location_type(self, obj):
        return obj.location_type

    class Meta:
        geo_field = 'location'
        depth = 7
        model = Location
        fields = ['id', 'city', 'airport', 'location',  'name', 'region', 'country']

    def validate(self, attrs):
        """ Either city OR airport is required
        """
        if attrs.get('city', None) or attrs.get('airport', None):
            return attrs
        log.error('attrs: %s', attrs)


class SimpleRegionalSerializer(ModelSerializer):
    class Meta:
        model = Regional
        fields = ['id', 'name', 'slug']


class EventSerializer(ModelSerializer):
    location = LocationSerializer()
    auth_type = AuthTypeSerializer()
    regional = SimpleRegionalSerializer()
    image = ImageField(use_url=True, required=False, allow_null=True)
    header_image = ImageField(use_url=True, required=False, allow_null=True)

    class Meta:
        model = Event
        #geo_field = 'location'
        fields=(
            'id',
            'name',
            'slug',
            'start_date',
            'end_date',
            'location',
            'official',
            'airport',
            'auth_type',
            'regional',
            'current',
            'featured',
            'image',
            'header_image',
            'description',
            'url',
        )


class SimpleEventSerializer(ModelSerializer):
    class Meta:
        model = Event
        fields=(
            'id',
            'name',
            'slug',
        )


class RegionalSerializer(ModelSerializer):
    event_set = SerializerMethodField('get_events')

    class Meta:
        model = Regional
        fields = ['id', 'name', 'slug', 'event_set']
        depth = 3

    def get_events(self, regional):
        extension = datetime.timedelta(settings.EVENT_VIABILITY_EXTENSION)
        event_set = Event.objects.filter(
            regional=regional,
            active=True,
            end_date__gt=timezone.now() - extension
        )
        serializer = EventSerializer(instance=event_set, many=True)
        return serializer.data


class ContactSerializer(ModelSerializer):
    contact_email = EmailField()
    created = DateTimeField(read_only=True, required=False)
    class Meta:
        model = Contact
        fields = (
            'rideshare',
            'created',
            'contact_name',
            'contact_email',
            'contact_info',
            'contact_phone',
            'message',
        )


class UserSerializer(ModelSerializer):
    #profile = ProfileSerializer()
    #email = EmailField()
    class Meta:
        model = User
        fields = (
                'id',
                'username',
                'first_name',
                'last_name',
                #'email',
                'is_staff',
                #'profile',
                )


class LoginSerializer(Serializer):
    username = CharField(max_length=128, required=False, allow_blank=True)
    password = CharField(max_length=128)
    obj_cls = CharField(max_length=32, required=False)
    obj_id = IntegerField(required=False)


class HelpMessageSerializer(Serializer):
    email = EmailField(max_length=128, required=True)
    subject = CharField(max_length=128, required=True)
    message = CharField(max_length=2048, required=True)
    zipcode = CharField(max_length=1, required=False)


# Deprecated? Now bypassing this serializer to return nested 
class UsageSerializer(Serializer):
    country = CharField(max_length=2, required=False)
    region = CharField(max_length=128, required=False)
    name = CharField(max_length=128, required=False)
    num_rideshares = IntegerField(required=True)


# These two Rideshare fields, contact_email and contact_phone are normally
# transmitted in obfuscated form -- md5 digest and '****', unless the user
# has object permissions that allow them, in which case we swap out the fields
# for regular Field and EmailField.
class PrivatePhoneField(Field):
    """ Obfuscate phone field -- only when retrieving """
    def to_representation(self, obj):
        return '*****'
    def to_internal_value(self, data):
        return data


class EmailDigestField(EmailField):
    """ Create the md5 digest on the fly -- only when retrieving """
    def to_representation(self, value):
        m = hashlib.md5()
        m.update(value.encode('utf-8'))
        return m.hexdigest()
    def to_internal_value(self, data):
        return data


class EventField(RelatedField):
    def to_representation(self, value):
        serializer = EventSerializer(value)
        return serializer.data
    def to_internal_value(self, data):
        return data.get('id')


class LocationField(RelatedField):
    def to_representation(self, value):
        serializer = LocationSerializer(value)
        return serializer.data
    def to_internal_value(self, data):
        """ Get a Location object by looking up the city or airport.
            If Location object is not found, then create one.
            Non-RESTful side-effect, but should be fine.
        """
        try:
            city = City.objects.get(id=data.get('city').get('id'))
        except (AttributeError, City.DoesNotExist):
            city=None
        try:
            airport = Airport.objects.get(id=data.get('airport').get('id'))
        except (AttributeError, Airport.DoesNotExist):
            airport=None

        try:
            loc, created = Location.objects.get_or_create(city=city, airport=airport)
            return loc.id
        except (Location.IntegrityError, Location.DuplicateError) as e:
            msg = 'Location must have one and only one city or airport: {}'.format(e)
            log.error(msg)
            raise ValidationError(msg)

#
#        if city and not airport:
#            try:
#                loc = Location.objects.get(city=city.get('id'))
#                #log.debug('Using Existing Location %s', loc)
#            except Exception as e:
#                #log.debug('Creating Location for city %s', city.get('content_auto'))
#                loc = Location.from_obj(City.objects.get(id=city.get('id')))
#            return loc.id
#        elif airport and not city:
#            try:
#                loc = Location.objects.obj(airport=airport.get('id'))
#                #log.debug('Using Existing Location %s', loc)
#            except Exception as e:
#                #log.debug('Creating Location for airport %s', airport.get('content_auto'))
#                loc = Location.from_obj(Airport.objects.get(id=airport.get('id')))
#            return loc.id
#        else:
#            log.error('Location must have one and only one city or airport')
#            raise ValidationError('Location must have one and only one city or airport')


class PreferenceField(RelatedField):
    def to_representation(self, value):
        serializer = PreferenceSerializer(value)
        return serializer.data
    def to_internal_value(self, data):
        return data.get('id')


class RoundTripField(RelatedField):
    def to_representation(self, value):
        serializer = RoundTripSerializer(value)
        return serializer.data
    def to_internal_value(self, data):
        return data.get('id')


class DirectionField(RelatedField):
    def to_representation(self, value):
        serializer = DirectionSerializer(value)
        return serializer.data
    def to_internal_value(self, data):
        return data.get('id')


class RideTypeField(RelatedField):
    def to_representation(self, value):
        serializer = RideTypeSerializer(value)
        return serializer.data
    def to_internal_value(self, data):
        return data.get('id')


class HaulingField(RelatedField):
    def to_representation(self, value):
        serializer = HaulingSerializer(value)
        return serializer.data
    def to_internal_value(self, data):
        return data.get('id')


class RideField(RelatedField):
    def to_representation(self, value):
        serializer = RideSerializer(value)
        return serializer.data
    def to_internal_value(self, data):
        return data.get('id')


class RideshareAbstractSerializer(ModelSerializer):
    event = EventField(queryset=Event.objects.all())
    location = LocationField(queryset=Location.objects.all())
    preferences = PreferenceField(queryset=Preference.objects.all(), many=True)
    round_trip = RoundTripField(queryset=RoundTrip.objects.all())
    direction = DirectionField(queryset=Direction.objects.all())
    hauling = HaulingField(queryset=Hauling.objects.all())
    rtype = RideTypeField(queryset=RideType.objects.all())
    ride = RideField(queryset=Ride.objects.all())

    created = DateTimeField(read_only=True, required=False)
    contact_email = EmailDigestField()
    contact_phone = PrivatePhoneField(required=False, allow_null=True)
    riders_baggage_weight = FloatField(required=False, allow_null=True)
    #riders = IntegerField(required=False)

    def create(self, validated_data):
        event = validated_data.pop('event')
        location = validated_data.pop('location')
        round_trip = validated_data.pop('round_trip')
        direction = validated_data.pop('direction')
        hauling = validated_data.pop('hauling')
        rtype = validated_data.pop('rtype')
        ride = validated_data.pop('ride')
        preferences = validated_data.pop('preferences')

        rideshare = Rideshare.objects.create(
                event_id=event,
                location_id=location,
                round_trip_id=round_trip,
                direction_id=direction,
                hauling_id=hauling,
                rtype_id=rtype,
                ride_id=ride,
                **validated_data)
        rideshare.preferences.add(*preferences)

        return rideshare

    def update(self, instance, validated_data):
        log.debug('RideshareAbstractSerializer.update validated_data: %s', validated_data)

        # first get the one-to-many foreign keys
        instance.event_id = validated_data.pop('event', instance.event.id)
        instance.location_id = validated_data.pop('location',
                instance.location.id)
        instance.round_trip_id = validated_data.pop('round_trip',
                instance.round_trip.id)
        instance.direction_id = validated_data.pop('direction',
                instance.direction.id)
        instance.hauling_id = validated_data.pop('hauling', instance.hauling.id)
        instance.rtype_id = validated_data.pop('rtype', instance.rtype.id)
        instance.ride_id = validated_data.pop('ride', instance.ride.id)

        #TODO pop the 'created' datetime field, if present, for updates?

        # next, get the many-to-many foreign keys
        preferences = validated_data.pop('preferences',
                instance.preferences.all())

        # finally, the standard fields; leave force_update in validated_data.
        # are there other possible fields to ignore?
        fields = [f for f in list(validated_data.keys()) if f != 'force_update']
        for f in fields:
            setattr(instance, f, validated_data.pop(f, getattr(instance, f)))

        instance.save(**validated_data)
        instance.preferences.add(*preferences)

        return instance

    def validate(self, attrs):
        log.debug('RideshareAbstractSerializer.validate attrs: %s', attrs)

        # A valid patch may be a single element setting the "filled"
        # status to True or False.
        # If there is just the filled key, and it is a boolean, then
        # this is valid. Otherwise, continue with validation of other 
        # attributes.
        # Possibly, split validate() into separate functions depending 
        # on whether it's a POST or PATCH method?
        full = attrs.get('full')
        if full in (True, False):
            if len(list(attrs.keys())) == 1:
                return attrs

        flight = attrs.get('flight')
        riders = attrs.get('riders')

        # The two try/except clauses below test that the attributes are
        # correct if they are set, if not just fall through.
        try:
            rtype = RideType.objects.get(id=attrs.get('rtype'))
            loc = Location.objects.get(id=attrs.get('location'))
            # If this rideshare represents a pilot offering a flight, then
            # the location type should be an airport.
            # In all other cases, it should be a city.
            log.debug('LOC: %s, %s %s, %s %s', flight, loc, loc.airport, rtype, rtype.key)
            if flight and rtype.key == 'OFFER':
                assert loc.airport
            else:
                assert loc.city
        except AssertionError:
            raise ValidationError('Flight/Airport/City mismatch')
        except ObjectDoesNotExist as e:
            log.debug(e)

        try:
            # If this is a passenger ride, then number of passengers must be >= 1
            # If this is not a passenger ride, then number of passengers must be None
            # It cannot be a non-passenger AND non-hauling ride.
            hauling = Hauling.objects.get(id=attrs.get('hauling'))
            ride = Ride.objects.get(id=attrs.get('ride'))
            if ride.key == 'YES':
                assert riders is not None, 'passenger ride, but riders is None'
                assert riders >= 1, 'passenger ride, but riders is not >= 1'
            elif ride.key == 'NO':
                assert riders == None, 'not-passenger ride, but riders is not None'
                assert hauling.key != 'NO', 'not-passenger ride, and also not-hauling'
        except AssertionError as e:
            log.error(e)
            raise ValidationError('Ride/Hauling/Riders mismatch: %s' % e)
        except ObjectDoesNotExist as e:
            log.debug(e)

        return attrs


class RideshareFullSerializer(RideshareAbstractSerializer):
    def __init__(self, *args, **kwargs):
        cleartext_private_fields = kwargs.pop('cleartext_private_fields', None)
        super(RideshareFullSerializer, self).__init__(*args, **kwargs)

        # If this request was made by the authenticated owner of the object,
        # then send the actual field values.  Otherwise, the default behavior
        # is to obfuscate the email and phone fields.
        if cleartext_private_fields:
            self.fields['contact_email'] = EmailField()
            self.fields['contact_phone'] = CharField(required=False, allow_null=True)

    class Meta:
        model = Rideshare
        depth = 3
        lookup_field = 'id'
        # Don't include contacts field here! That needs to be a separate
        # authenticated query.
        fields = (
            'id',
            #'user', *use UserSerializer?
            'password_hint',
            'name',
            'location',
            'riders',
            'event',
            'direction',
            'round_trip',
            'ride',
            'hauling',
            'rtype',
            'earliest_departure',
            'latest_departure',
            'earliest_return',
            'latest_return',
            'preferences',
            'full',
            'flight',
            'description',
            'contact_name',
            'contact_email',
            'contact_info',
            'contact_phone',
            'sms_notify',
            'riders_baggage_weight',
            'created',
            )


class RideshareArchiveSerializer(RideshareAbstractSerializer):
    """ Archiving format may include real contact_phone and contact_email 
        fields.  Does NOT include the Event, since this is known when
        archiving, and would be a lot of repeated data.  We are archiving by
        event.
    """
    contact_set = ContactSerializer(many=True, read_only=True)

    def __init__(self, *args, **kwargs):
        cleartext_private_fields = kwargs.pop('cleartext_private_fields', None)
        super(RideshareArchiveSerializer, self).__init__(*args, **kwargs)

        # If this request was made by the authenticated owner of the object,
        # then send the actual field values.  Otherwise, the default behavior
        # is to obfuscate the email and phone fields.
        if cleartext_private_fields:
            self.fields['contact_email'] = EmailField()
            self.fields['contact_phone'] = CharField()

    class Meta:
        model = Rideshare
        depth = 3
        lookup_field = 'id'
        # DO include contact fields here!
        fields = (
            'id',
            'user',
            'contact_set',
            'password_hint',
            'name',
            'location',
            'riders',
            #'event',
            'direction',
            'round_trip',
            'ride',
            'hauling',
            'rtype',
            'earliest_departure',
            'latest_departure',
            'earliest_return',
            'latest_return',
            'preferences',
            'full',
            'flight',
            'description',
            'contact_name',
            'contact_email',
            'contact_info',
            'contact_phone',
            'sms_notify',
            'riders_baggage_weight',
            'created',
            )


class RideshareSerializer(RideshareAbstractSerializer):
    class Meta:
        model = Rideshare
        depth = 3
        fields = (
            'id',
            'name',
            'location',
            'riders',
            'rtype',
            'direction',
            'round_trip',
            'hauling',
            'ride',
            'earliest_departure',
            'latest_departure',
            'earliest_return',
            'latest_return',
            'contact_email',
            'contact_name',
            'full',
            'flight',
            'created',
            )
    def restore_object(self, attrs, instance=None):
        log.debug('RideshareSerializer.restore_obj attrs: %s', attrs)


class OffsetRideshareSerializer(Serializer):
    """ Make output look like Paginated output """
    count = IntegerField()
    results = RideshareSerializer(many=True)


class StatSerializer(Serializer):
    """ Usage statistics: number of offers and requests (by direction) and
        number of offers/requests that have been marked as filled.
    """
    direction = DictField(read_only=True, child=DictField())
    filled = IntegerField(read_only=True)
    event = SimpleEventSerializer(read_only=True)
    airport = ReadOnlyField(read_only=True)


class AggregateSerializer(Serializer):
    """ Combines both flight and non-flight Stats.  """
    stats = StatSerializer(many=True)
