import logging
import inspect
from rest_framework import permissions

log = logging.getLogger(__name__)

ALLOWED_METHODS = permissions.SAFE_METHODS + ('POST',)

class IsOwner(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to
    view or edit it.
    """
    def has_object_permission(self, request, view, obj):
        #log.debug('%s.%s request method == %s',
            #self, inspect.currentframe().f_code.co_name, request.method)
        #log.debug('%s.%s %s == %s ? %s',
            #self, inspect.currentframe().f_code.co_name,
            #obj.user, request.user, obj.user == request.user)

        return obj.user == request.user

class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Special permissions for Rideshare.
    """
    def has_permission(self, request, view):
        """ 1) Allow method GET, OPTIONS, HEAD and *POST* for all users.
            2) Allow PUT and DELETE for authenticated users -- however, those
               operations are subject to subsequent has_object_permissions()
               checks (see below).
        """
        #log.debug('%s.%s request method == %s',
            #self, inspect.currentframe().f_code.co_name, request.method)

        if request.method in ALLOWED_METHODS:
            return True
        elif (request.user and request.user.is_authenticated):
            #log.debug('User %s exists and is authenticated', request.user)
            return True
        return False

    def has_object_permission(self, request, view, obj):
        """ Allow GET, PUT and DELETE for authenticated users who own the
            object.
        """
        #log.debug('%s.%s request method == %s',
            #self, inspect.currentframe().f_code.co_name, request.method)

        # This check is not that important, but really this method is only for 
        # PUT and DELETE.
        if request.method not in ['GET', 'PUT', 'PATCH', 'DELETE']:
            log.error('Bad request method')
            return False

        # Request must have an attribute named `user`.
        # Check that the user has authenticated.
        # Check that user owns the object.
        if (request.user and request.user.is_authenticated):
            #log.debug('User %s exists and is authenticated', request.user)
            #log.debug('User %s %s own object %s', request.user, obj.user == request.user and "does" or "doesn't", obj)
            return obj.user == request.user

        return False

