import logging
from django.conf import settings
from django import http
from django.shortcuts import render
from django.views.generic.base import TemplateView

from rideshare.models import Event

log = logging.getLogger(__name__)

def server_error(request, template_name='500.html'):
    """
    500 error handler.
    """
    return render(request, template_name, {})

def page_not_found(request, template_name='404.html'):
    """
    404 error handler.
    """
    return render(request, template_name, {})

# Experimental view to relieve us of hard-coding the name of the event for
# the rideshares reverse url.  Works, but has problem with /logout...
class HomepageView(TemplateView):
    template_name = 'homepage.html'
    def get_context_data(self, **kwargs):
        """This simply selects the official, featured event for the
            Burning Man "regional" which should always be TTITD during the time
            that the Kiosk is active.
        """
        try:
            # only one featured event per region is allowed, so get() s/b okay
            log.warn('Getting Context, getting Event')
            event = Event.objects.get(
                    active=True, official=True, featured=True,
                    regional__slug='burning-man')
            return {'event_slug':event.slug}
        except Event.DoesNotExist:
            log.error('Unable to get Kiosk event slug')
            pass
        return {'event_slug':None}

