#!/env/python
import pprint

def run():
    from rideshare.serializers import *
    e = Event.objects.all()[0]
    l = e.location
    es = EventSerializer(e)
    ls = LocationSerializer(l)
    r = Rideshare.objects.all()[0]
    rs = RideshareSerializer(r)

    print(ls.data)
    print(es.data)
    pprint.pprint(rs.data, indent=4)
