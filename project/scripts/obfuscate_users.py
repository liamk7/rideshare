import re
import logging
import hashlib
from django.contrib.auth.models import User
from django.utils.text import slugify

# Obfuscate user emails and passwords


def run():
    users = User.objects.exclude(username__in=('liam', 'kenyoni', 'brianberlin'))
    for u in users:
        if u.email:
            print(u.email, "===>", end='')
            addr, domain = u.email.split('@')
            addr = hashlib.md5(addr).hexdigest()
            u.email = '@'.join([hashlib.md5(addr).hexdigest(), domain])
            print(u.email)
            #u.save()


