import logging
from cities.models import *
from django.utils.text import slugify
from rideshare.models import Region as RSRegion, Rideshare, Location
from .lookup import valid_places

log = logging.getLogger(__name__)

def run():
    place_array = valid_places
    place_hash = dict([(p[0], p[1][0]) for p in place_array])

    rideshares = Rideshare.objects.all()

    for rs in rideshares:
        try:
            # This works if the region name is like "Wyoming"
            # or a city name; it can be either a Region or City
            real_place = place_hash[rs.region.name]
        except:
            continue

        city_slug = slugify(rs.nearest_city)

        if isinstance(real_place, City):
            loc, created = Location.objects.get_or_create(
                    city=real_place)
            log.debug("%s %s", ('Found', 'Created')[int(created)], loc)
            rs.location = loc
            rs.save()
        elif city_slug:
            try:
                qs = City.objects.filter(slug=city_slug)
                if isinstance(real_place, Region):
                    qs.filter(region=real_place)
                elif isinstance(real_place, Country):
                    qs.filter(country=real_place)

                if qs.count() == 1:
                    city = qs[0]
                    loc, created = Location.objects.get_or_create(city=city)
                    log.debug("%s %s", ('Found', 'Created')[int(created)], loc)
                    #print "%s %s" % (('created','found')[int(created)], loc)
                    rs.location = loc
                    #print rs.nearest_city, rs.region.name, '==>', str(rs.location)
                    log.debug("Mapping %s %s to %s", rs.nearest_city,
                            rs.region.name, rs.location)

                    log.debug("Saving Rideshare: %s", rs)
                    rs.save()

            except Exception as e:
                log.error("%s", e)
                raise
