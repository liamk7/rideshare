import logging
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

log = logging.getLogger(__name__)

def run():
    users = User.objects.all()
    log.info('Generating tokens for %s users', users.count()) 
    for user in users:
        Token.objects.get_or_create(user=user)
