import re
import logging
from cities.models import *
from django.utils.text import slugify
from rideshare.models import Region as RSRegion
from .lookup import valid_places, invalid_places

# This just runs through all the old regions and tries to match them to the
# Regions, SubRegions and Cities imported from cities (which in turn come 
# from geonames.
# It just prints them to the screen.  First the successful matches, then the
# ones that need some love.

def run():
    #for p in (get_valid_places() + get_invalid_places()):
    for p in (valid_places + invalid_places):
        print("%s ==> %s" % (p[0].encode('utf8'), p[1]))


