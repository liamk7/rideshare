import re
from cities.models import *
from django.utils.text import slugify
from rideshare.models import Region as RSRegion

def lookup_country(country):
    country = slugify(country)
    return Country.objects.filter(slug=country)

def lookup_city_region(city, region):
    region = slugify(region)
    city = slugify(city)
    ret = City.objects.filter(country__slug__in=('united-states','canada'),
            region__slug=region, slug=city)
    #if not ret:
        #ret = City.objects.filter(country__slug__in=('united-states','canada'),
            #slug=city)
    return ret

def lookup_city_country(city, country):
    country = slugify(country)
    city = slugify(city)
    ret = City.objects.filter(country__slug=country, slug=city)
    return ret

def lookup_region(region):
    region = slugify(region)
    return Region.objects.filter(
            country__slug__in=('united-states','canada'), slug=region)

def lookup_subregion(subregion):
    subregion = slugify(subregion)
    return Subregion.objects.filter(
            region__country__slug__in=('united-states','canada'), slug__startswith=subregion)

def lookup_region_subregion(region, subregion):
    subregion = slugify(subregion)
    region = slugify(region)
    return Subregion.objects.filter(
            region__country__slug__in=('united-states','canada'), region__slug=region, slug__startswith=subregion)

def lookup_region_country(region, country):
    country = slugify(country)
    region = slugify(region)
    return Region.objects.filter(
            country__slug__in=('united-states','canada'), slug=region)

def lookup_city(city):
    city = slugify(city)
    return City.objects.filter(slug=city)

def lookup_place(placename):
    for f in (lookup_country, lookup_region, lookup_subregion, lookup_city):
        place = None
        try:
            place = f(placename)
            if place: return place
        except:
            raise
    return place
class Lookup():
    place_array = []


    def get_valid_places(self):
        if not self.place_array:
            self.place_array = self.get_place_array()
        return [p for p in self.place_array if len(p[1]) == 1]

    def get_invalid_places(self):
        if not self.place_array:
            self.place_array = self.get_place_array()
        return [p for p in self.place_array if len(p[1]) != 1]

    def get_place_array(self):
        regions = RSRegion.objects.all()
        p = re.compile('([^(]*)\(([^)]+)\)')

        for r in regions:
            m = p.match(r.name)
            if m:
                m1 = m.group(1).strip()
                m2 = m.group(2).strip()
                if m1 in ('Europe', 'Asia'):
                    #print ("%d\t%s" % (r.id, m2)),
                    result = lookup_country(m2)
                else:
                    #print ("%d\t%s, %s" % (r.id, m2, m1)),
                    result = lookup_city_region(m2, m1)
                    if not result:
                        result = lookup_city_country(m2, m1)
                    if not result:
                        result = lookup_region_country(m2, m1)
                    if not result:
                        result = lookup_region_subregion(m1, m2)
            else:
                #print ("%d\t%s" % (r.id, r.name)),
                result = lookup_place(r.name)

            self.place_array.append((r.name, result))

        return self.place_array


lookup = Lookup()
valid_places = lookup.get_valid_places()
invalid_places = lookup.get_invalid_places()
get_place_array = lookup.get_place_array()
