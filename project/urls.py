import logging
from django.conf import settings
from django.conf.urls import include, url
from django.contrib.gis import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.static import serve

from rideshare.feeds import RideshareFeed

log = logging.getLogger(__name__)

admin.autodiscover()

urlpatterns = [
    # Admin and API
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r"^grappelli/", include('grappelli.urls')),
    url(r"^admin/",     admin.site.urls),
    url(r'^api/',       include('api.urls')),
    url(r'^rss/(?P<event_slug>.*)/?$', RideshareFeed(), name="rss"),
]

urlpatterns += [
    # Note that r"^$" doesn't work right
    url(r"", include("rideshare.urls")),
    url(r"", include("rideshare_frontend.urls")),
]

# Static URLs
if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()

# Upload URLS
if settings.DEBUG:
    urlpatterns.insert(-2,
        url(r'^%s(?P<path>.*)' % settings.MEDIA_URL[1:],
            serve, {'document_root': settings.MEDIA_ROOT})
    )
    #urlpatterns += rtt_test_urlpatterns

