#!/usr/bin/env python
import json
import os
import sys

# Create a smallish dataset for testing
# First do something like this in the shell.
# This gets the basic objects that are needed to simluate the full
# environment.  It doesn't get any Rideshare or Contact objects.
"""
./manage.py dump_object rideshares.event '*' > events.json
./manage.py dump_object rideshares.location '*' > locations.json
./manage.py merge_fixtures events.json locations.json > rideshares_initial.json
./manage.py reorder_fixtures rideshares_initial.json \
        airports.country \
        airports.region \
        airports.airport \
        cities.country \
        cities.region \
        cities.subregion \
        cities.city \
        rideshares.authtype \
        rideshares.location \
        rideshares.regional \
        rideshares.event > new_rideshares_initial.json
"""
# Then run this script to remove extraneous unused data
# (cities.city.alt_names and cities.city.neighbours)
# ...

# Then copy the output to the initial data file
#cp new_rideshares_initial.json apps/rideshares/fixtures/initial_data.json

def main():
    try:
        filename = sys.argv[1]
        assert os.path.isfile(filename)
    except IndexError:
        print("Usage: %s <filename>" % sys.argv[0])
        sys.exit(-1)

    f = open(filename)

    def fix(val):
        try:
            del val['alt_names']
        except:
            pass
        try:
            del val['neighbours']
        except:
            pass
        return val

    data = json.load(f, object_hook=fix)
    json.dump(data, sys.stdout, indent=4)

if __name__ == '__main__':
    main()
