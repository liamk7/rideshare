"""
This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::
    GRAPPELLI_INDEX_DASHBOARD = 'rideshare.dashboard.CustomIndexDashboard'
"""
from django.conf import settings
from django.contrib.sites.models import Site
from django.urls import reverse
from django.template import Template
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name

from rideshare.models import Event

import pprint, json


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for www.
    """

    def init_with_context(self, context):
        site_name = get_admin_site_name(context)
        site = Site.objects.get_current()

        if settings.DEBUG:
            # NOTE: hostname value should not be trusted since it comes from
            # the browser and can be spoofed.  However, this dashboard file
            # is only accessible to authenticated superusers, so it should be
            # fine.
            t = Template('{{request.META.HTTP_HOST}}')
            hostname = t.render(context)
        else:
            # this one is okay, since it comes from site object
            hostname = site.domain

        active_events = Event.objects.filter(active=True, official=True)
        #if not settings.DEBUG:
            #active_events = active_events.filter(end_date__gt=timezone.now())

        # append a group for "Administration" & "Applications"
        self.children.append(modules.Group(
            _('Administration & Applications'),
            column=1,
            collapsible=False,
            children = [
                modules.AppList(
                    _('Django Administration'),
                    column=1,
                    collapsible=True,
                    css_classes=('collapse grp-closed',),
                    models=('django.contrib.*',),
                ),
                modules.AppList(
                    'Rideshare Administration',
                    column=1,
                    collapsible=True,
                    css_classes=('collapse grp-closed',),
                    models=('rideshare.*',),
                ),
#                modules.AppList(
#                    'Kiosk Administration',
#                    column=1,
#                    collapsible=True,
#                    css_classes=('collapse grp-closed',),
#                    models=(
#                        'camps.*',
#                        'artwork.*',
#                        'playa.*',
#                        'events.*',
#                        'postings.*',
#                        'profiles.*',
#                        'signedauth.*',
#                        ),
#                ),
#                modules.AppList(
#                    'Kiosk Runtime Tags',
#                    column=1,
#                    collapsible=True,
#                    css_classes=('collapse grp-closed',),
#                    models=(
#                        'django_runtime_tags.*',
#                        ),
#                ),
                modules.AppList(
                    'City/Airport Administration',
                    column=1,
                    collapsible=True,
                    css_classes=('collapse grp-closed',),
                    models=(
                        'cities.*',
                        'airports.*',
                        ),
                ),
            ]
        ))

        # append another link list module for "support".
        self.children.append(modules.LinkList(
            _('Support'),
            column=2,
            children=[
                {
                    'title': _('Django Documentation'),
                    'url': 'http://docs.djangoproject.com/',
                    'external': True,
                },
                {
                    'title': _('Rideshare API'),
                    'url': '/api/',
                    'external': True,
                },
            ]
        ))

        usage_links = []
        for event in active_events:
            usage_links.append({
                'title': event.name,
                'url': '/usage/%s/' % (event.slug),
                'external': True,
            })
        self.children.append(modules.LinkList(
            title='Usage Statistics',
            column=2,
            children=usage_links,
            css_classes=('collapse grp-closed',),
         ))


        # Get RSS feed for active events.
        # Note that this gets the *local* list of active events but
        # queries the production server.  It should really query the
        # server on which it's running, but this is not critical.
        for event in active_events:
            title = 'RSS %s' % event.name
            self.children.append(modules.Feed(
                title,
                pre_content='From production server!',
                column=2,
                feed_url='https://rideshare.burningman.org/rss/%s' % (event.slug),
                limit=5,
                css_classes=('collapse grp-closed',),
            ))

        # append a recent actions module
        self.children.append(modules.RecentActions(
            _('Recent Actions'),
            limit=5,
            collapsible=False,
            column=3,
        ))

        # append a feed module
        self.children.append(modules.Feed(
            _('Latest Django News'),
            column=3,
            feed_url='http://www.djangoproject.com/rss/weblog/',
            limit=5
        ))


