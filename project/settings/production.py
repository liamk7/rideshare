# Production settings

try:
    from project.settings.base import *
except ImportError:
    raise ImportError('Unable to import project/settings/base.py.')

DEBUG = False

ALLOWED_HOSTS = [
    'localhost',
    '127.0.0.1',
    'rideshare-production-inst.burningman.org',
    'rideshare.burningman.org',
]

CSRF_COOKIE_DOMAIN = '.burningman.org'
CSRF_TRUSTED_ORIGINS = ['.burningman.org']

CORS_ORIGIN_ALLOW_ALL = False
CORS_ORIGIN_WHITELIST = (
        'rideshare.burningman.org',
        'directory.burningman.org',
        'localhost:8000',
        'dev.directory.burningman.org',
        )

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'OPTIONS': {
            'options': '-c search_path=rideshare,postgis,postgis_topology,topology,pg_catalog,public',
	},
        'NAME': os.getenv('DEFAULT_DATABASE_NAME'),
        'USER': os.getenv('DEFAULT_DATABASE_USER'),
        'PASSWORD': os.getenv('DEFAULT_DATABASE_PASSWORD'),
        'HOST': os.getenv('DEFAULT_DATABASE_HOST'),
        'PORT': '',
    }
}

# Elasticsearch-dsl params, set in .env file
ELASTICSEARCH_DSL = {
    'default': {
        'hosts': os.getenv('ELASTICSEARCH_URL'),
        # Required for SSL. Setting port:443 in URL is insufficient.
        'use_ssl': True,
        'verify_certs': True,
    },
}

MANAGERS = ADMINS = (
    ("Liam Kirsher", "liamk@numenet.com"),
)

######################## LOCAL SETTINGS INCLUDE ###############################
# local.py is used to override environment-specific settings
try:
    from project.settings.local_settings import *
except ImportError:
    print("""
********************************************************************************
Unable to import project/settings/local.py. Skipped.
To avoid this message cp local_example.py to local.py.
********************************************************************************
""")
