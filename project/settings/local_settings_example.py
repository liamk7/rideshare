# Local settings that override generic stuff set previously

GRAPPELLI_ADMIN_TITLE = 'My Rideshare Site'
RIDESHARE_REGION_SLUG = 'rideshare'
DEFAULT_FROM_EMAIL = 'donotreply@mysite.com'

# People to receive messages users address via the Help facility
HELP_ADMINS = (
    ('Rideshare Help', 'help@mysite.org'),
)
# People to receive management message
MANAGERS = ADMINS = (
    ('Admin User', 'admin@mysite.org'),
)

EMAIL_TEST_ADDR = 'valid@mysite.org'

TIME_ZONE = 'America/Los_Angeles'

# An SMS number for use during testing
SMS_TEST_NUMBER = '+14151111111'

