############################# PATH & ENV STUFF #####################################

import os
import sys

# Find and load .env file.  It should be in the same file as the settings file.
from dotenv import load_dotenv
load_dotenv(verbose=True)

# Django screws up AngularJS POSTS because it redirects, losing the data.
# See router_patch.js for complementary fix for this problem.
# Maybe can be removed if fix is incorporated in AngularJS itself.
APPEND_SLASHES = False
APPEND_SLASH = False

PROJECT_ROOT = os.path.abspath(
        os.path.join(
            os.path.dirname(__file__),
            '../'
        )
    )
ROOT = os.path.abspath(os.path.join(PROJECT_ROOT, "../"))
LOG_ROOT = '/var/log/django/'

# Create the secret key and save in secret.txt
from project.secret_key_gen import *


############################# DJANGO EXTENSIONS #############################
# Add some convenient imports when using shell_plus
SHELL_PLUS_PRE_IMPORTS = [
    ('django.contrib.gis.geos', 'Point'),
    ('rideshare.documents', 'LocationDocument'),
    ('rideshare.documents', 'RideshareDocument'),
    ('rideshare.documents', 'CityDocument'),
    ('rideshare.documents', 'AirportDocument'),
]
SHELL_PLUS_MODEL_IMPORTS_RESOLVER = 'django_extensions.collision_resolvers.FullPathCR'


############################# RIDESHARE_FRONTEND #############################
# These are required by rideshare_frontend. Override in local_settings.py
RIDESHARE_REGION_SLUG = 'my-region-name'

# KioskMode is set to True for Directory project in its settings.py
# This is passed down to the front-end app, so it knows whether it's running
# locally (on a LAN at the event) or in the cloud.  Mainly controls handling
# of gravatar images.
KIOSK_MODE = False


################################### DEPLOY ##############################
# Debug settings. You will have slow-downs etc if you use this for more
# than dev.  Never deploy a site into production with DEBUG turned on!
# Override in settings_production.py
DEBUG = False

# Number of days after event.end_date to continue showing event.
# TODO make this consistent with the days_after_event variable in controllers.js
EVENT_VIABILITY_EXTENSION = 5


################################### EMAIL & SMS ###############################
# Address to use for various automated correspondence from the site manager(s).
DEFAULT_FROM_EMAIL = 'donotreply@example.org'
HELP_SUBJECT = 'Rideshare Help'
HELP_RECEIPT = """Thank you for your message. If it requires a response we will
get back to you promptly."""

HELP_ADMINS = [
    ('Rideshare Help', 'email@example.org'),
]

# Set the subject prefix for email messages sent to admins and managers
EMAIL_SUBJECT_PREFIX = '[Rideshare] '

# Maximum size (in bytes) before an upload gets streamed to the file system.
FILE_UPLOAD_MAX_MEMORY_SIZE = 5242880

EMAIL_BACKEND = 'anymail.backends.mandrill.EmailBackend'
SERVER_EMAIL = 'burningman.org'
ANYMAIL = {
    "MANDRILL_API_KEY" : os.getenv('MANDRILL_API_KEY'),
    "MANDRILL_SEND_DEFAULTS": {
        "esp_extra": {
            "message": {
                "subaccount": os.getenv('MANDRILL_SUBACCOUNT')
            }
        }
    }
}

# Set this in local_settings.py for testing.
EMAIL_TEST_ADDR = ''

# Twilio SMS account params
SMS_TEST_NUMBER = '+14151111111' # Just for testing!
TWILIO_FROM_VALID = '+15005550006' # Passes validation, no SMS sent, for tests

TWILIO_ACCOUNT_SID      = os.getenv('TWILIO_ACCOUNT_SID')
TWILIO_AUTH_TOKEN       = os.getenv('TWILIO_AUTH_TOKEN')
TWILIO_FROM_NUMBER      = os.getenv('TWILIO_FROM_NUMBER')
TWILIO_TEST_ACCOUNT_SID = os.getenv('TWILIO_TEST_ACCOUNT_SID') 
TWILIO_TEST_AUTH_TOKEN  = os.getenv('TWILIO_TEST_AUTH_TOKEN') 


############################### STORAGE #######################################

# S3 Storage for images
AWS_ACCESS_KEY_ID = os.getenv('AWS_ACCESS_KEY_ID') 
AWS_SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY') 
AWS_STORAGE_BUCKET_NAME = os.getenv('AWS_STORAGE_BUCKET_NAME')
AWS_LOCATION = 'media'
AWS_S3_FILE_OVERWRITE = True
#AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
# NOTE: must use AWS path name syntax, because otherwise the cert doesn't match
# and the browser raises an error when accessing over https.  For some types of
# buckets, you must put the specific region where the bucket resides:
#AWS_S3_CUSTOM_DOMAIN = 's3-us-west-2.amazonaws.com/%s' % AWS_STORAGE_BUCKET_NAME
AWS_S3_HOST = 's3-us-west-2.amazonaws.com'
AWS_S3_CALLING_FORMAT = 'boto.s3.connection.OrdinaryCallingFormat'
AWS_QUERYSTRING_AUTH = False
AWS_HEADERS = { 'Cache-Control': 'max-age=86400' }
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'


#################### LOCATION, LOCATION, LOCATION, ETC. #######################
# List of locations of the fixture data files, in search order
FIXTURE_DIRS = []

# A tuple of strings designating all the enabled applications
INSTALLED_APPS = [
    # Django Contrib Apps
    #"django.contrib.admin.apps.SimpleAdminConfig",
    'grappelli.dashboard',       # must go before django.contrib.admin
    'grappelli',                 # must go before django.contrib.admin
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.gis',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sites',
    'django.contrib.staticfiles',
    'django_filters',

    # External Apps
    'anymail',                  # Django package email service providers
    'airports',                 # Django-airports (manage.py airports)
    'cities',                   # Django-cities (manage.py cities --import country,region,city)
    'corsheaders',              # Sets CORS headers in middleware
    #'debug_toolbar',            # Debug Toolbar
    'django_extensions',        # Admin Extensions
    'fixture_magic',            # Ability to dump objects as fixtures
    'gunicorn',                 # WSGI HTTP Server
    'rest_framework.authtoken', # DRF TokenAuthentication
    'rest_framework',           # Django REST Framework (DRF)
    'rest_framework_gis',       # Django REST Framework (GIS)
    'storages',                 # storage framework required for S3
    'django_elasticsearch_dsl', # You know, for Search

    # RideShare Apps
    'api',                      # Implements the API with DRF
    'rideshare',                # Main application
    'rideshare_frontend',       # Delivery app for the AngularJS app
]

# Default site should be set appropriately in admin
SITE_ID = 1

# The language code for this installation
LANGUAGE_CODE = 'en-us'

# Who should get broken-link notifications when SEND_BROKEN_LINK_EMAILS=True
MANAGERS = ADMINS = []

# Absolute path to the directory that holds stored files.
MEDIA_ROOT = os.path.join(PROJECT_ROOT, '..', 'uploads')

# URL that handles the media served from MEDIA_ROOT (must end in a slash)
MEDIA_URL = '/uploads/'

# A tuple of middleware classes to use
MIDDLEWARE = [
    "django.middleware.gzip.GZipMiddleware",
    "django.middleware.cache.UpdateCacheMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.contrib.admindocs.middleware.XViewMiddleware",
    "django.contrib.sites.middleware.CurrentSiteMiddleware",
    "django.middleware.cache.FetchFromCacheMiddleware",

    #--- Added to project
    "api.middleware.AngularJsonVulnerabilityMiddleware",
]

# The full Python import path to the root URLconf
ROOT_URLCONF = 'project.urls'

# Absolute path to the directory where collectstatic will collect static files
STATIC_ROOT = os.path.realpath(os.path.join(ROOT, 'static'))

# Additional locations the staticfiles app will traverse
STATICFILES_DIRS = [
    os.path.join(PROJECT_ROOT, 'static'),
]

# URL to use when referring to static files located in STATIC_ROOT
STATIC_URL = '/static/'

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [ os.path.join(PROJECT_ROOT, 'templates') ],
        'OPTIONS': {
            'debug': True,
            'libraries': { },
            'loaders': [
                ('django.template.loaders.cached.Loader',
                    (
                    'django.template.loaders.filesystem.Loader',
                    'django.template.loaders.app_directories.Loader',
                )),
            ],
            'context_processors': [
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.request",
            ],

        },
    },
]

# The time zone for this installation
TIME_ZONE = 'America/Los_Angeles'

# Enable support for time zones when storing datetime objects
USE_TZ = True

# Output the "Etag" header. This saves bandwidth but slows down performance
USE_ETAGS = False

# Enable Django's internationalization system
USE_I18N = True

# Display numbers and dates using the format of the current locale
USE_L10N = True


########################## ELASTICSEARCH ######################################

ELASTICSEARCH_DSL_SIGNAL_PROCESSOR = 'rideshare.ded_signals.RideshareRealTimeSignalProcessor'
ELASTICSEARCH_DSL_AUTO_REFRESH = True
ELASTICSEARCH_DSL_PARALLEL = True

# Elasticsearch-dsl params, set in .env file
ELASTICSEARCH_DSL = {
    'default': {
        'hosts': os.getenv('ELASTICSEARCH_URL'),
        # Required for SSL. Setting port:443 in URL is insufficient.
        'use_ssl': True,
        'verify_certs': True,
    },
}


########################## DEBUG TOOLBAR ######################################

DEBUG_TOOLBAR_CONFIG = {
    "DISABLE_PANELS": set(['debug_toolbar.panels.redirects.RedirectsPanel']),
    "SHOW_TEMPLATE_CONTEXT": True,
}


######################### CACHE SETTINGS ####################################
#CACHES = {
#    'default': {
#        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
#        'LOCATION': 'unique-snowflake',
#        'KEY_PREFIX': 'rs',
#        'MAX_ENTRIES': 1024,
#    }
#}
TEST_MEMCACHE = False
if not DEBUG or TEST_MEMCACHE:
    CACHES = {
        'default': {
            'BACKEND':'django.core.cache.backends.memcached.MemcachedCache',
            'LOCATION': '127.0.0.1:11211',
            'KEY_PREFIX': 'rs',
        }
    }
else:
    CACHES = {
        'default': {
            'BACKEND':'django.core.cache.backends.dummy.DummyCache',
        }
    }


######################### LOGGING SETTINGS ####################################

LOGFILE = os.path.join(LOG_ROOT, 'rideshare-django.log')
DEBUG_LOGFILE = os.path.join(LOG_ROOT, 'rideshare-debug.log')
GUNICORN_LOGFILE = os.path.join(LOG_ROOT, 'rideshare-gunicorn.log')
ELASTICSEARCH_LOGFILE = os.path.join(LOG_ROOT, 'rideshare-elasticsearch.log')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(name) %(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
        'default': {
            'format': '%(asctime)s %(levelname)s [%(filename)s:%(lineno)s] %(message)s'
        },
        # Colors for the colore formatter are:
        # black, red, green, yellow, blue, purple, cyan, white
        # Can be prefixed with 'bold_'
        'color': {
            '()': 'colorlog.ColoredFormatter',
            'format': '%(log_color)s%(levelname)-6s [%(filename)s:%(lineno)d] %(message)s',
            'datefmt': '[%d/%b/%Y %H:%M:%S]',
            'log_colors': {
                'DEBUG':    'cyan',
                'INFO':     'white',
                'WARNING':  'yellow',
                'ERROR':    'red',
                'CRITICAL': 'bold_red',
            },
        },
     },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
         },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
         }
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console':{
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'color',
            'filters': ['require_debug_true'],
        },
        'management':{
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'color',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'filters': ['require_debug_false'],
            'include_html': True,
        },
        'default': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'default',
            'filename': LOGFILE,
            'maxBytes': 1024 * 1024 * 10, # 10MB
            'backupCount': 5,
        },
        'debug': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'default',
            'filename': DEBUG_LOGFILE,
            'maxBytes': 1024 * 1024 * 10, # 10MB
            'backupCount': 5,
        },
        'gunicorn': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'simple',
            'filename': GUNICORN_LOGFILE,
            'maxBytes': 1024 * 1024 * 10, # 10MB
            'backupCount': 5,
        },
        'elasticsearch': {
            'level': 'WARNING',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'default',
            'filename': ELASTICSEARCH_LOGFILE,
            'maxBytes': 1024 * 1024 * 10, # 10MB
            'backupCount': 5,
        },
    },
    'loggers': {
        'api': {
            'handlers': ['console', 'default', 'debug'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'django': {
            'handlers': ['default'],
            'propagate': False,
            'level': 'INFO',
        },
        'django.request': {
            'handlers': ['console', 'default'],
            'level': 'ERROR',
            'propagate': False,
        },
        'elasticsearch': {
            'handlers': ['elasticsearch'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'elasticsearch.trace': {
            'handlers': ['elasticsearch'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'factory': {
            'handlers': ['debug'],
            'level': 'INFO',
            'propagate': False,
        },
        'gunicorn': {
            'handlers': ['console', 'gunicorn'],
            'level': 'INFO',
            'propagate': False,
        },
        'mandrill': {
            'handlers': ['console', 'debug'],
            'level': 'INFO',
            'propagate': False,
        },
        'urllib3': {
            'handlers': ['null'],
            'level': 'INFO',
            'propagate': False,
        },
        'management': {
            'handlers': ['management'],
            'level': 'DEBUG',
            'propagate': False,
        },
        '': {
            'handlers': ['console', 'default'],
            'level': 'INFO',
            'propagate': True,
        },
    }
}


######################### DRF SETTINGS ####################################

REST_FRAMEWORK = {
    # Use hyperlinked styles by default.
    # Only used if the `serializer_class` attribute is not set on a view.
    'DEFAULT_MODEL_SERIALIZER_CLASS':
        'rest_framework.serializers.HyperlinkedModelSerializer',

    'DEFAULT_AUTHENTICATION_CLASSES': [
        'api.authentication.SuperUserSessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
        #'rest_framework.authentication.SessionAuthentication',
    ],
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        #'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
        #'rest_framework.permissions.AllowAny',
        #'rest_framework.permissions.IsAuthenticated',
        'rest_framework.permissions.IsAuthenticatedOrReadOnly',
    ],
    'PAGE_SIZE': 10,
    # NOTE: Test api pagination behavior
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'DEFAULT_FILTER_BACKENDS': ['django_filters.rest_framework.DjangoFilterBackend'],
    'DEFAULT_RENDERER_CLASSES': [
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ],
}

# The default timeout is set to 24 hours -- some objects like cities and
# airports and locations don't change... really at all.  For shorter timeouts,
# set them individually.
REST_FRAMEWORK_EXTENSIONS = {
    'DEFAULT_CACHE_RESPONSE_TIMEOUT': 60 * 60 * 24,
    'DEFAULT_OBJECT_CACHE_KEY_FUNC':
      'rest_framework_extensions.utils.default_object_cache_key_func',
    'DEFAULT_LIST_CACHE_KEY_FUNC':
      'rest_framework_extensions.utils.default_list_cache_key_func',
}


##################### TESTING VARIABLES ################################

TEST_REQUEST_DEFAULT_FORMAT = 'json'
TEST_RUNNER = 'project.test_runner.FastTestRunner'


##################### CITIES & AIRPORTS ################################
# For info see: https://github.com/coderholic/django-cities

# Override the default source files and URLs
CITIES_FILES = {
    'city': {
       'filename': 'cities1000.zip',
       'urls':     ['http://download.geonames.org/export/dump/'+'{filename}']
    },
}

# Localized names will be imported for all ISO 639-1 locale codes below.
# 'und' is undetermined language data (most alternate names are missing a lang tag).
# See download.geonames.org/export/dump/iso-languagecodes.txt
# 'LANGUAGES' will match your language settings
CITIES_LOCALES = ['en', 'und', 'LANGUAGES']

# Postal codes will be imported for all ISO 3166-1 alpha-2 country codes below.
# See cities.conf for a full list of country codes.
# See download.geonames.org/export/dump/countryInfo.txt
CITIES_POSTAL_CODES = ['US', 'CA']

# List of plugins to process data during import
CITIES_PLUGINS = [
    # Canadian postal codes need region codes remapped to match geonames
    'cities.plugin.postal_code_ca.Plugin',
    # Reduce memory usage when importing large datasets (e.g. "allCountries.zip")
    'cities.plugin.reset_queries.Plugin',
]

CITIES_DATA_DIR = '/tmp'


######################## CORS SETTINGS ########################################

CORS_ORIGIN_ALLOW_ALL = False
CORS_URLS_REGEX = r'^/api/.*$'
CORS_ALLOW_CREDENTIALS = True
CORS_ALLOW_HEADERS = [
    'x-requested-with',
    'content-type',
    'accept',
    'origin',
    'authorization',
    'X-CSRFToken',
    'Token',
]

# Override in development and production settings files.
CORS_ORIGIN_WHITELIST = ['set-in-development-or-production']
CSRF_COOKIE_DOMAIN = '.example.org'
#CORS_ORIGIN_REGEX_WHITELIST = ['localhost:8000', 'localhost:8001']


######################## GRAPPELLI SETTINGS ###################################

GRAPPELLI_ADMIN_TITLE = 'MySite Rideshare'
GRAPPELLI_SWITCH_USER = True
GRAPPELLI_INDEX_DASHBOARD = 'project.dashboard.CustomIndexDashboard'
GRAPPELLI_AUTOCOMPLETE_SEARCH_FIELDS = {
    'airports': {
        'airport': ('id__iexact', 'name__icontains')
    },
    'cities': {
        'city': ('id__iexact', 'name__icontains')
    }
}



