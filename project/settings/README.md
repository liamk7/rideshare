# Project Settings

The settings file is broken up into the self-explanatory files:
*  [`base.py`](./base.py)
*  [`development.py`](./development.py)
*  [`production.py`](./production.py)
*  [`local_settings.py`](./local_settings.py)

### Configuration

The `DJANGO_SETTINGS_MODULE` environment variable must be set to either
the development or production settings file.  The default is production, so
if you are running in development you must set it explicitly on the command
line.
```shell
DJANGO_SETTINGS_MODULE=project.settings.production
```

We are using the `python-dotenv` package to read private values from a `.env` file
at startup.  Note that you can also add the values to your environment when 
testing, via `source .env`.

```shell
cd project/settings
cp env_example.sh .env
cp local_settings_example.py local_settings.py
```

... and then customize the files as needed.


The `.env` and `local_settings.py` files are ignored by git.

You can probably get by with only changing those two files, but if necessary
you can edit `development.py` and/or `production.py`.  At a minimum, you
need to set the database development parameters in `.env`.

```shell
export DEFAULT_DATABASE_NAME='rideshare'
export DEFAULT_DATABASE_USER='rideshare_user'
export DEFAULT_DATABASE_PASSWORD='**********'
export DEFAULT_DATABASE_HOST='127.0.0.1'
```
