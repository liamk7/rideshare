# Development settings

try:
    from project.settings.base import *
except ImportError:
    raise ImportError('Unable to import project/settings/base.py.')

DEBUG = True
ALLOWED_HOSTS = ['*']
CORS_ORIGIN_ALLOW_ALL = False
CORS_ORIGIN_WHITELIST = ['localhost:8000']
CSRF_COOKIE_DOMAIN = None
INTERNAL_IPS = ['127.0.0.1']
EMAIL_SUBJECT_PREFIX = '[Rideshare %s] ' % 'DEBUG' if DEBUG else 'Production'

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'OPTIONS': {
	},
        'NAME': os.getenv('DEFAULT_DATABASE_NAME'),
        'USER': os.getenv('DEFAULT_DATABASE_USER'),
        'PASSWORD': os.getenv('DEFAULT_DATABASE_PASSWORD'),
        'HOST': os.getenv('DEFAULT_DATABASE_HOST'),
        'PORT': '',
    }
}

# Elasticsearch-dsl params, set in .env file
ELASTICSEARCH_DSL = {
    'default': {
        'hosts': os.getenv('ELASTICSEARCH_URL'),
        # Do not use SSL in development
        #'use_ssl': True,
        #'verify_certs': True,
    },
}


######################## LOCAL SETTINGS INCLUDE ###############################
# local.py is used to override environment-specific settings
try:
    from project.settings.local_settings import *
except ImportError:
    print("""
********************************************************************************
Unable to import project/settings/local.py. Skipped.
To avoid this message cp local_example.py to local.py.
********************************************************************************
""")

