## Fixture Data

* `rideshare.faq.json` contains data that can be inserted in the FAQ table.  The FAQ entries can be in different languages.


#### Dumping Data

You can dump data from an existing database using [fixture-magic](https://github.com/davedash/django-fixture-magic).

```
./manage.py dump_object rideshare.FAQ '*' --no-follow > rideshare.faq.json
```

#### Importing Data

```
./manage.py load data/rideshare.faq.json
```

