import logging
from django.shortcuts import render, get_object_or_404
from rideshare.models import Event
from rideshare_frontend.views import rideshare_root

log = logging.getLogger(__name__)

def rideshare_usage(request, **kwargs):
    """ View for Usage Statistics using D3 modeling.
    """
    event_slug = kwargs.get('slug', None)
    template = kwargs.get('template', None)
    event_list = []

    if event_slug:
        event = Event.objects.get(slug=event_slug)
        region = event.regional
    else:
        event = None
        event_list = Event.objects.all()
        region = request.site.regional_set.all()[0]

    d3_names = (
            ('Bubble', 'd3_bubble'),
            ('Circle Packing', 'd3_circle_packing'),
            ('Hierarchicial Bars', 'd3_hierarchical_bars'),
            ('Hierarchicial Pack', 'd3_hierarchical_pack'),
            ('Treemap', 'd3_treemap'),
            ('Zoomable Treemap', 'd3_zoomable_treemap'),
            )

    if template:
        template_name = 'rideshare/%s.html' % template
    else:
        template_name = 'rideshare/usage.html'


    context = {
        'd3_names': d3_names,
        'event': event,
        'event_list': event_list,
        'region': region,
    }

    return render(
        request,
        template_name,
        context,
    )

