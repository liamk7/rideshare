from django.db.models.signals import post_save, post_delete
from django.core.cache import cache
from django.utils import timezone

from .models import Rideshare

# Signal handler for API cache invalidation
def change_api_updated_at(sender=None, instance=None, *args, **kwargs):
    cache.set('api_updated_at_timestamp', timezone.now())

# Remove a rideshare's auto-generated user when using playa auth type 
def delete_rideshare_user(sender=None, instance=None, *args, **kwargs):
    if instance.event.auth_type.name == 'playa':
        instance.user.delete()

post_save.connect(receiver=change_api_updated_at, sender=Rideshare)
post_delete.connect(receiver=change_api_updated_at, sender=Rideshare)
post_delete.connect(receiver=delete_rideshare_user, sender=Rideshare)
