"""
This file contains the Rideshare API model registration
and method definitions. This is accessed via __init__ for
the rideshare app. All of these use the default filter
defined by the cereal-box django-app.  If other filters
need to be defined they can be built and registered here.
Please read the cereal-box documentation for more information.
"""

import cereal

from rideshare.models import *

cereal.register(Region, [cereal.functions.filter()])
cereal.register(Event, [cereal.functions.filter()])
cereal.register(Preference, [cereal.functions.filter()])

#BJK: Disabled per Rob's direction, because this exposes phone numbers and emails.
#cereal.register(Rideshare, [cereal.functions.filter()])
