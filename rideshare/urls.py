from django.conf import settings
from django.conf.urls import url
from rideshare.feeds import RideshareFeed
from rideshare.views import rideshare_usage

urlpatterns = [
    url(r'^usage/(?P<slug>[\w\d-]+)/(?P<template>[\w\d_-]+)/?$', rideshare_usage, name="rideshare_usage"),
    url(r'^usage/(?P<slug>[\w\d-]+)/?$', rideshare_usage, name="rideshare_usage"),
    url(r'^usage/?$', rideshare_usage, name="rideshare_usage"),
    url(r'^rss/(?P<slug>[\w\d-]+)/?$', RideshareFeed(), name="rss"),
]
