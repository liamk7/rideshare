import logging
import shortuuid
import re
from django.conf import settings
from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.urls import reverse
from django.db.models import Count

from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
#from django_elasticsearch.models import EsIndexable

from airports.models import Airport
from cities.models import City, Region, Subregion
from rideshare import notify

log = logging.getLogger(__name__)

# make a short uuid of the specified length; probably not used,
# since the art, camps and events already have a uid.
# it will be used when creating new rideshares
def make_short_uuid():
    return shortuuid.ShortUUID().random(length=20)


# Note the nuance of the airport being None or False!
# For the Aggregate, airport == None means we don't care whether we are
# getting events which have airports or not.
# For Stat, where we are counting Rideshares, we consider that condition to
# mean that we are to count Rideshares that are not Flights.
# Note #2: Now adding the airport into the Stat object, since we are now
# returning *both* airport=True and airport=False results.
class Stat(object):
    def __init__(self, event, airport):
        self.event = event
        self.direction = {}
        if airport == None: airport = False

        # add airport to the returned values
        self.airport = airport;

        for direction_key in ('TO', 'FROM'):
            self.direction[direction_key.lower()] = {
                'offers': Rideshare.objects.filter(event=event,
                    flight=airport,
                    deleted=False,
                    active=True,
                    full=False,
                    rtype__key='OFFER',
                    direction__key=direction_key).count(),
                'requests': Rideshare.objects.filter(
                    event=event,
                    flight=airport,
                    deleted=False,
                    active=True,
                    full=False,
                    rtype__key='REQUEST',
                    direction__key=direction_key).count()
                }

        self.filled = Rideshare.objects.filter(event=event, flight=airport,
          deleted=False, active=True, full=True).count()


class Aggregate(object):
    def __init__(self, airport, event, official):
        self._stats = []
        events = Event.objects.all()
        if event:
            events = events.filter(slug=event)
        elif official is not None:
            events = events.filter(official=official)

        events = events.order_by('start_date')

        for e in events:
            if airport == None:
                for airport in (True, False):
                    self._stats.append(Stat(e, airport))
            else:
                for direction in ('TO', 'FROM'):
                    self._stats.append(Stat(e, airport))

    def stats(self):
        return self._stats


class Location(models.Model):
    """ A generic location which is either a city or an airport.
    """
    # The name, region and country are copies derived from the
    # airport or city, but you can customize the name, for example
    # if that makes sense (i.e. use whatever people would be looking
    # for).
    name = models.CharField(max_length=200, help_text='Name of location', blank=True)
    region = models.CharField(max_length=200, help_text='Region of location', blank=True)
    country = models.CharField(max_length=2, help_text='Country code of location', blank=True)
    city = models.ForeignKey('cities.City', null=True, blank=True,
            related_name='r_cities', on_delete=models.CASCADE)
    airport = models.ForeignKey('airports.Airport', null=True, blank=True,
            related_name='r_airports', on_delete=models.CASCADE)
    location = models.PointField(blank=True)

    class Meta:
        ordering = ['name', 'region', 'country']

    # Don't allow duplicated locations
    class DuplicateError(Exception): pass

    # Don't allow locations with none or both of airport and city
    class IntegrityError(Exception): pass


    # These propertiese are used by the Elasticsearch completion suggester.
    @property
    def obj_id(self):
        """ The id or the Location's City or Airport. """
        if self.city: return self.city.id
        elif self.airport: return self.airport.id
        else: return None

    @property
    def location_type(self):
        """ Type is either City or Airport. """
        if self.city: return self.city.__class__.__name__
        elif self.airport: return self.airport.__class__.__name__
        else: return None

    def __str__(self):
        return '{}, {} ({})'.format(self.name, self.region, self.country)

    @property
    def input(self):
        """ Input values used by elasticsearch indexing. """
        if self.airport:
            return [self.name, self.airport.city.name_std, self.region, self.country]
        else:
            return [self.name, self.region, self.country]

    def output(self):
        """ Formatted elasticsearch output that shows up in the search typeahead. """
        return '{}, {}, {}'.format(self.name, self.region, self.country)

    @property
    def weight(self):
        """ The weight is used for the elasticsearch score.
            It is used to rank results.  In this case, we are using the
            location popularity (as measured by number of Rideshares
            to/from it).
        """
        return self.rideshare_set.count() or 1

    def save(self, *args, **kwargs):
        if self.city and self.airport or not (self.city or self.airport):
            raise Location.IntegrityError('One and only one City or Airport!')

        if not self.id:
            if ((self.city and Location.objects.filter(city=self.city).exists()) or
                (self.airport and Location.objects.filter(airport=self.airport).exists())):
                raise Location.DuplicateError('City or airport Location already exists')
            try:
                # Whichever city we have, use it to derive country, region and name
                # if those values are not supplied, as might be the case when adding a
                # Location via the Django Admin.
                city = self.city or self.airport.city

                if not self.location:
                    self.location = city.location

                if not self.region:
                    try:
                        self.region = city.region.name
                    except AttributeError:
                        self.region = 'Unknown'

                if not self.country:
                    try:
                        self.country = city.country.code
                    except AttributeError:
                        if self.region:
                            self.country = self.region.country.code
                        else:
                            self.country = 'ZZ'

                if not self.name:
                    if self.airport:
                        self.name = self.airport.name or city.name or 'Unknown'
                    else:
                        self.name = city.name or 'Unknown'

            except Exception as e:
                log.error('This should not happen: %s', e)
                raise

        super(Location, self).save(*args, **kwargs)


    @classmethod
    def from_obj(cls, obj):
        if isinstance(obj, Airport):
            ret = cls(airport=obj)
            ret.name = obj.name
        elif isinstance(obj, City):
            ret = cls(city=obj)
            ret.name = obj.name_std
        else:
            raise Location.IntegrityError('Argument must be a City or Airport!')

        ret.save()
        return ret


class ActiveManager(models.Manager):
    def active(self, **kwargs):
        """A filter that automatically adds "active=True" to the returned query"""
        kwargs['active'] = True
        return self.filter(**kwargs)


class AuthType(models.Model):
    name = models.CharField(max_length=24, help_text='Authentication type')
    class Meta:
        ordering = ['-name']

    def __str__(self):
        return self.name


class Regional(models.Model):
    name = models.CharField(max_length=24, help_text='Regional Network')
    slug = models.SlugField(unique=True)
    # For sites served by Burning Man server (i.e. rideshare.burningman.org),
    # connect to site model, for other sites (using the API), this can and
    # should be null.
    site = models.ManyToManyField(Site, blank=True,
            help_text='Regional is served by burningman.org as /this/ site')

    class Meta:
        ordering = ['name']

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Regional, self).save(*args, **kwargs)

    def __str__(self):
        return self.name


class Event(models.Model):
    """ An event the user is travelling to or from. """
    name    = models.CharField(max_length=200, help_text='Event Name')
    slug    = models.SlugField(unique=True, blank=True)
    url     = models.URLField('URL', blank=True, null=True, help_text='Event URL')
    image   = models.ImageField(null=True, blank=True, help_text='JPEG 1920x499')
    header_image = models.ImageField(null=True, blank=True, help_text='JPEG 1920x499')

    start_date = models.DateTimeField(help_text='Set the start date of the event')
    end_date   = models.DateTimeField(help_text='Set the end date of the event')

    active      = models.BooleanField(default=True, help_text='Must be active to be displayed')
    featured    = models.BooleanField(default=False, help_text='Make this the featured event -- one per region')
    current     = models.BooleanField(default=False, help_text='Make this a current event -- multiple per region')
    official    = models.BooleanField(default=False, help_text='Official makes it visible in Rideshare site')

    airport     = models.BooleanField(default=False, help_text='Airport is available')
    location    = models.ForeignKey('Location', on_delete=models.CASCADE)
    regional    = models.ForeignKey('Regional', on_delete=models.CASCADE)

    auth_type   = models.ForeignKey('AuthType', help_text='Type of authentication for this event', on_delete=models.CASCADE)
    description = models.TextField('Description', blank=True, null=True, max_length=500, help_text='Event description. HTML allowed.')

    country_search_contexts = models.ManyToManyField('cities.Country', blank=True, related_name='r_countries',
            help_text='Confine typeahead search to these countries')

    objects = ActiveManager()

    class Meta:
        ordering = ['-featured', '-current', 'start_date']

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug,
        }
        return reverse('rideshare_event', kwargs=kwargs)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        if self.featured:
            # featured is different from current -- it's either/or
            self.current = False
            # allow one (1) featured event per regional
            for event in self.regional.event_set.filter(featured=True):
                event.featured = False
                event.save()
        super(Event, self).save(*args, **kwargs)

class Preference(models.Model):
    """ A preference for a rideshare such as 'non-smoking' """
    name = models.CharField(max_length=200, help_text='Preference')
    key = models.CharField(max_length=20, help_text='Translation key')
    order = models.PositiveIntegerField(default=0, blank=True, null=True, help_text='The order in which preferences appear')
    active = models.BooleanField(default=True)

    objects = ActiveManager()

    class Meta:
        ordering = ['order', 'name']

    def __str__(self):
        return self.name

class DeletionReason(models.Model):
    """ A reason a Rideshare was deleted """
    name = models.CharField(max_length=200, help_text='Reason a Rideshare was deleted')
    key = models.CharField(max_length=20, help_text='Translation key')
    order = models.PositiveIntegerField(default=0, blank=True, null=True, help_text='The order in which reasons appear')

    objects = ActiveManager()

    class Meta:
        ordering = ['order', 'name']

    def __str__(self):
        return self.name

class RideType(models.Model):
    """ A type of ride for a rideshare """
    name = models.CharField(max_length=200, help_text='Ride Type')
    key = models.CharField(max_length=30, help_text='Translation key')

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

class Direction(models.Model):
    name = models.CharField(max_length=200, help_text='Direction')
    key = models.CharField(max_length=30, help_text='Translation key')
    reverse_key = models.CharField(max_length=30, help_text='Translation key')

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

class RoundTrip(models.Model):
    name = models.CharField(max_length=200, help_text='Round Trip')
    key = models.CharField(max_length=30, help_text='Translation key')

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

class Hauling(models.Model):
    name = models.CharField(max_length=200, help_text='Hauling')
    key = models.CharField(max_length=30, help_text='Translation key')

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

class Ride(models.Model):
    name = models.CharField(max_length=200, help_text='Ride')
    key = models.CharField(max_length=30, help_text='Translation key')

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

class FAQ(models.Model):
    LANG_CHOICES = (
        ('en', 'English'),
        ('es', 'Spanish'),
        ('fr', 'French'),
    )
    question  = models.TextField('FAQ Question', blank=False, help_text='FAQ Question')
    answer    = models.TextField('FAQ Answer', blank=False, help_text='FAQ Answer')
    language  = models.CharField(choices=LANG_CHOICES, max_length=2, help_text='Language')
    order     = models.PositiveSmallIntegerField(default=0, help_text='Order on page')

    class Meta:
        ordering = ['order']

    def __str__(self):
        return self.question

class Rideshare(models.Model):
    """ Rideshare is a request or offer of a ride to a particular event. """

    # Rideshare internal and status fields
    uid         = models.CharField(max_length=24, default=make_short_uuid, unique=True, help_text='Unique digest id')
    created     = models.DateTimeField(_('created'), editable=False)
    modified    = models.DateTimeField(_('modified'))
    deleted     = models.BooleanField(_('deleted'), default=False, help_text='Soft-deleted')
    deletion    = models.ForeignKey(DeletionReason, blank=True, null=True, help_text='Choose deletion reason', on_delete=models.SET_NULL)
    full        = models.BooleanField(default=False, help_text='Indicates if rideshare is full')
    active      = models.BooleanField(default=True, help_text='Indicates this Rideshare is active')

    # About the user who created the Rideshare
    user          = models.ForeignKey(User, unique=False, verbose_name=_('user'), on_delete=models.CASCADE)
    password_hint = models.CharField(_('password hint'), max_length=120, blank=False)
    contact_name  = models.CharField(max_length=200, blank=True)
    contact_email = models.EmailField(blank=True, null=True)
    contact_info  = models.TextField(blank=True, null=True, help_text='Other ways to contact you')
    contact_phone = models.CharField(max_length=200, blank=True, null=True, help_text='Enter your number for text-message (SMS) notification.')
    sms_notify    = models.BooleanField(default=True, help_text='SMS (text message) notification')

    # Other Rideshare data
    name        = models.CharField(_('name'), max_length=128, help_text=_('Pick a meaningful name that you and others can find and recognize.'))
    description = models.TextField(_('description'), help_text=_('Important information that you want to share about this entry in the directory.'))
    event       = models.ForeignKey(Event, help_text='Event related to this rideshare', on_delete=models.CASCADE)
    location    = models.ForeignKey(Location, help_text='Rideshare endpoint', blank=True, null=True, on_delete=models.CASCADE)
    rtype       = models.ForeignKey(RideType, help_text='Request or Offer?', on_delete=models.CASCADE)
    direction   = models.ForeignKey(Direction, help_text='Which way are you headed?', on_delete=models.CASCADE)
    round_trip  = models.ForeignKey(RoundTrip, help_text='One way or Round Trip?', on_delete=models.CASCADE)
    ride        = models.ForeignKey(Ride, help_text='Taking passengers?', on_delete=models.CASCADE)
    hauling     = models.ForeignKey(Hauling, help_text='Hauling stuff?', on_delete=models.CASCADE)
    preferences = models.ManyToManyField(Preference, blank=True, help_text='Choose preference')
    riders      = models.PositiveSmallIntegerField(blank=True, null=True, help_text='Number of open rider spots or spots wanted')
    flight      = models.BooleanField(default=False, help_text='Indicates this is a FlightShare')
    riders_baggage_weight = models.DecimalField(blank=True, null=True, max_digits=12, decimal_places=8,
            help_text="FlightShare: Combined weight of riders' baggage (kilos)")

    # Trip departure/return.  We are currently only using earliest_departure/earliest_return.
    # Leaving the other values in case we want to re-implement time windows.
    earliest_departure = models.DateTimeField(blank=True, null=True)
    latest_departure   = models.DateTimeField(blank=True, null=True)
    earliest_return    = models.DateTimeField(blank=True, null=True)
    latest_return      = models.DateTimeField(blank=True, null=True)

    objects = ActiveManager()

    class Meta:
        ordering = ['id']

    def save(self, *args, **kwargs):
        if not self.id:
            self.created = timezone.now()
        self.modified = timezone.now()
        return super(Rideshare, self).save(*args, **kwargs)

    def __str__(self):
        if self.location:
            return '{}: {} {} {}'.format(self.event, self.direction.name.lower(), self.location, self.id)
        else:
            return '{}: {} {}'.format(self.event, self.direction.name.lower(), self.id)

    def get_absolute_url(self):
        kwargs = {
            'slug': self.event.slug,
            'object_id': self.id,
        }
        return reverse('rideshare_detail', kwargs=kwargs)

    def get_edit_url(self):
        kwargs = {
            'event_slug': self.event.slug,
            'object_id': self.id,
        }
        return reverse('rideshare_edit', kwargs=kwargs)


class Contact(models.Model):
    """ Data saved from an inquiry into a rideshare """
    rideshare      = models.ForeignKey(Rideshare, on_delete=models.CASCADE)
    contact_name   = models.CharField('Sender Name', max_length=200, blank=True, help_text="What is your (the sender's) name?")
    contact_email  = models.EmailField('Sender Email', blank=True, help_text="What is your (the sender's) email address?")
    contact_info   = models.TextField('Sender Contact Info', blank=True,
                     help_text='Are there other ways (phone, post, carrier pigeon, etc) that the recipient of this message should contact you?')
    contact_phone  = models.CharField('Sender Phone', max_length=200, blank=True, null=True, help_text='Enter your phone number')
    message = models.TextField(help_text='Sender message to be sent.')
    created = models.DateTimeField(_('created'), help_text='Set automatically in save()', editable=False)

    def __str__(self):
        try:
            rideshare_id = self.rideshare.id
        except:
            rideshare_id = None

        return self.contact_name

    def save(self, *args, **kwargs):
        if not self.id:
            self.created = timezone.now()

        # send internal message
        super(Contact, self).save(*args, **kwargs)

        # and now send out SMS and Email notifications
        if self.rideshare.sms_notify:
            try:
                assert self.rideshare.contact_phone
                notify.send_sms(self)
                log.info('SMS Message sent by Contact %s', self.id)
            except AssertionError:
                log.warn('Rideshare %s missing contact_phone. Message not sent',
                        self.rideshare.id)
            except Exception as e:
                log.error('SMS notification failed: %s', e)

        notify.send_email(self, *args, **kwargs)

