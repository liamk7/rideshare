# -*- coding: utf-8 -*-


from django.db import models, migrations
from django.utils import timezone


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0003_load_hauling_ride_data'),
    ]

    operations = [
        migrations.AddField(
            model_name='rideshare',
            name='hauling',
            field=models.ForeignKey(default=0, to='rideshare.Hauling', help_text=b'Hauling stuff?', on_delete=models.CASCADE),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rideshare',
            name='ride',
            field=models.ForeignKey(default=0, to='rideshare.Ride', help_text=b'Taking passengers?', on_delete=models.CASCADE),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='created',
            field=models.DateTimeField(default=timezone.now(), verbose_name='created', editable=False),
            preserve_default=True,
        ),
    ]
