# -*- coding: utf-8 -*-


from django.db import models, migrations
from django.utils import timezone


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0012_auto_20141204_1626'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='event',
            options={'ordering': ['-current', '-start_date']},
        ),
        migrations.AddField(
            model_name='rideshare',
            name='contact_phone',
            field=models.CharField(help_text=b'Enter your number for text-message (SMS) notification. ', max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rideshare',
            name='sms_notify',
            field=models.BooleanField(default=True, help_text=b'SMS (text message) notification'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='current',
            field=models.BooleanField(default=False, help_text=b'Make this the current (featured) event'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='created',
            field=models.DateTimeField(default=timezone.now(), verbose_name='created', editable=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='rtype',
            field=models.ForeignKey(help_text=b'Request or Offer?', to='rideshare.RideTypeChoice', on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
