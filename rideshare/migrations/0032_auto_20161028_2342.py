# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0031_auto_20160818_1524'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='description',
            field=models.TextField(help_text=b'Event description', null=True, verbose_name=b'Description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='url',
            field=models.URLField(help_text=b'Event URL', null=True, verbose_name=b'URL', blank=True),
            preserve_default=True,
        ),
    ]
