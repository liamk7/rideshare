# -*- coding: utf-8 -*-


from django.db import models, migrations
#from rideshare.models import Rideshare, RideTypeChoice

def load_data(apps, schema_editor):
    RideTypeChoice = apps.get_model("rideshare", "RideTypeChoice")
    RideTypeChoice(id=0, name='Offer').save()
    RideTypeChoice(id=1, name='Request').save()

class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0007_auto_20141106_1451'),
    ]

    operations = [
        migrations.RunPython(load_data)
    ]
