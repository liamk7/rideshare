# -*- coding: utf-8 -*-


from django.db import models, migrations
from django.conf import settings
from django.utils import timezone
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cities', '__first__'),
        ('airports', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AuthType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Authentication type', max_length=24)),
            ],
            options={
                'ordering': ['-name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('contact_name', models.CharField(help_text=b"What is your (the sender's) name?", max_length=200, verbose_name=b'Your Name', blank=True)),
                ('contact_email', models.EmailField(help_text=b"What is your (the sender's) email address?", max_length=75, verbose_name=b'Your Email', blank=True)),
                ('contact_info', models.TextField(help_text=b'Are there other ways (phone, post, carrier pigeon, etc) that the recipent of this message should contact you?', verbose_name=b'Your Contact Info', blank=True)),
                ('message', models.TextField(help_text=b'Your message to be sent.')),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Direction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Direction', max_length=200)),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Event Name', max_length=200)),
                ('short_name', models.CharField(help_text=b'Event short name', max_length=10)),
                ('slug', models.SlugField(unique=True)),
                ('start_date', models.DateTimeField(help_text=b'Set the start date of the event')),
                ('end_date', models.DateTimeField(help_text=b'Set the end date of the event')),
                ('active', models.BooleanField(default=True)),
                ('current', models.BooleanField(default=False, help_text=b'Set to make this the current event')),
                ('airport', models.BooleanField(default=False, help_text=b'Airport available')),
                ('official', models.BooleanField(default=False)),
                ('auth_type', models.ForeignKey(help_text=b'Type of authentication for this event', to='rideshare.AuthType', on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ['-start_date'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Name', max_length=200)),
                ('region', models.CharField(help_text=b'Region', max_length=200)),
                ('country', models.CharField(help_text=b'Country code', max_length=2)),
                ('slug', models.SlugField(help_text=b'Slug Name', max_length=200, blank=True)),
                ('location', django.contrib.gis.db.models.fields.PointField(srid=4326)),
                ('airport', models.ForeignKey(related_name='r_airports', blank=True, to='airports.Airport', null=True, on_delete=models.CASCADE)),
                ('city', models.ForeignKey(related_name='r_cities', blank=True, to='cities.City', null=True, on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ['name', 'region', 'country'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Preference',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Preference', max_length=200)),
                ('order', models.PositiveIntegerField(default=0, help_text=b'The order in which preferences appear', null=True, blank=True)),
                ('active', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ['order', 'name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Region', max_length=200)),
                ('slug', models.SlugField(help_text=b'Short Name')),
                ('active', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RegionalNode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Regional Network "Node"', max_length=24)),
                ('slug', models.SlugField(unique=True)),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Rideshare',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('earth_id', models.IntegerField(help_text=b'Original id of Object on PlayaEvents', null=True, verbose_name='earth id', blank=True)),
                ('name', models.CharField(help_text='Pick a meaningful name that you and others can find and recognize.', max_length=128, verbose_name='name')),
                ('description', models.TextField(help_text='Important information that you want to share about this entry in the directory.', verbose_name='description')),
                ('deleted', models.BooleanField(default=False, verbose_name='deleted')),
                ('playa_only', models.BooleanField(default=False, help_text='Indicate if you want to remove this record from the directory after the burn.', verbose_name='playa only')),
                ('password_hint', models.CharField(max_length=120, verbose_name='password hint')),
                ('created', models.DateTimeField(default=timezone.now(), verbose_name='created', editable=False)),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='modified')),
                ('nearest_city', models.CharField(help_text=b'Nearest city in your region', max_length=200, blank=True)),
                ('camp', models.CharField(max_length=200, blank=True)),
                ('contact_name', models.CharField(max_length=200, blank=True)),
                ('contact_email', models.EmailField(max_length=75, blank=True)),
                ('contact_info', models.TextField(help_text=b'Other ways to contact you', blank=True)),
                ('earliest_departure', models.DateTimeField(null=True, blank=True)),
                ('latest_departure', models.DateTimeField(null=True, blank=True)),
                ('earliest_return', models.DateTimeField(null=True, blank=True)),
                ('latest_return', models.DateTimeField(null=True, blank=True)),
                ('riders', models.PositiveIntegerField(default=0, help_text=b'Number of open rider spots or spots wanted')),
                ('stuff', models.TextField(help_text=b'What kind of room for stuff?', blank=True)),
                ('offering', models.TextField(help_text=b'What are you offering or willing to contribute for a ride and/or haul?', blank=True)),
                ('full', models.BooleanField(default=False, help_text=b'Indicates if rideshare is full')),
                ('active', models.BooleanField(default=True)),
                ('flight', models.BooleanField(default=False, help_text=b'Indicates this is a FlightShare')),
                ('riders_weight', models.PositiveIntegerField(default=0, help_text=b'FlightShare: Combined weight of riders', null=True, blank=True)),
                ('riders_baggage_weight', models.PositiveIntegerField(default=0, help_text=b"FlightShare: Combined weight of riders' baggage", null=True, blank=True)),
                ('direction', models.ForeignKey(help_text=b'Which way are you headed?', to='rideshare.Direction', on_delete=models.CASCADE)),
                ('event', models.ForeignKey(help_text=b'Event related to this rideshare', to='rideshare.Event', on_delete=models.CASCADE)),
                ('location', models.ForeignKey(blank=True, to='rideshare.Location', help_text=b'Rideshare endpoint', null=True, on_delete=models.CASCADE)),
                ('preferences', models.ManyToManyField(help_text=b'Choose preference', to='rideshare.Preference', null=True, blank=True)),
                ('region', models.ForeignKey(blank=True, to='rideshare.Region', help_text=b"Region you're coming from", null=True, on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ['id'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RideType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Ride Type', max_length=200)),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RoundTrip',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Round Trip', max_length=200)),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='rideshare',
            name='round_trip',
            field=models.ForeignKey(help_text=b'One way or Round Trip?', to='rideshare.RoundTrip', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rideshare',
            name='type',
            field=models.ManyToManyField(help_text=b'Are you offering or looking', to='rideshare.RideType'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rideshare',
            name='user',
            field=models.ForeignKey(verbose_name='user', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='location',
            field=models.ForeignKey(blank=True, to='rideshare.Location', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='regional',
            field=models.ForeignKey(blank=True, to='rideshare.RegionalNode', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='contact',
            name='rideshare',
            field=models.ForeignKey(to='rideshare.Rideshare', on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
