# -*- coding: utf-8 -*-


from django.db import models, migrations

def make_short_uuid():
    return shortuuid.ShortUUID().random(length=20)

class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0028_remove_location_slug'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='rideshare',
            name='earth_id',
        ),
        migrations.AddField(
            model_name='rideshare',
            name='uid',
            field=models.CharField(default=make_short_uuid,
                help_text=b'Original Object id on Content or PlayaEvents',
                null=True, max_length=24),
            preserve_default=True,
        ),
    ]
