# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0022_auto_20150305_1347'),
    ]

    operations = [
        migrations.CreateModel(
            name='FAQ',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.TextField(help_text=b'FAQ Question', verbose_name=b'FAQ Question', blank=True)),
                ('answer', models.TextField(help_text=b'FAQ Answer', verbose_name=b'FAQ Answer', blank=True)),
                ('language', models.CharField(help_text=b'Language code', max_length=2)),
                ('order', models.PositiveSmallIntegerField(default=0, help_text=b'Order on page')),
            ],
            options={
                'ordering': ['order'],
            },
            bases=(models.Model,),
        ),
    ]
