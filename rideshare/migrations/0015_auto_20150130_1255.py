# -*- coding: utf-8 -*-


from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0014_auto_20150130_1235'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='regionalnode',
            name='site',
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 30, 20, 55, 5, 690487, tzinfo=utc), verbose_name='created', editable=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 30, 20, 55, 5, 690525, tzinfo=utc), verbose_name='modified'),
            preserve_default=True,
        ),
    ]
