# -*- coding: utf-8 -*-


from django.db import models, migrations

from rideshare.models import AuthType, Event, Location
from cities.models import City, Region, Country, Subregion

def split_type(apps, schema_editor):
    try:
        AuthType = apps.get_model("rideshare", "AuthType")
        Event = apps.get_model("rideshare", "Event")
#        Location = apps.get_model("rideshare", "Location")
#        RegionalNode = apps.get_model("rideshare", "RegionalNode")
#        City = apps.get_model("cities", "City")
#        Region = apps.get_model("cities", "Region")
#        Country = apps.get_model("cities", "Country")
#        Subregion = apps.get_model("cities", "Subregion")

        AuthType(pk=1, name='django').save()
        AuthType(pk=2, name='playa').save()
        AuthType(pk=3, name='oauth').save()
        Event.objects.all().update(auth_type_id=2)

#        fields = {
#            "currency": None,
#            "currency_name": None,
#            "code": "US",
#            "name": "United States",
#            "area": None,
#            "capital": "",
#            "continent": "NA",
#            "languages": None,
#            "phone": "",
#            "code3": "",
#            "tld": "us",
#            "slug": "united-states",
#            "population": 310232863
#        }
#        country, created = Country.objects.get_or_create(pk=6252001, defaults=fields)
#        print "1", country, created
#
#        fields = {
#            "code": "NV",
#            "name": "Nevada",
#            "country": country,
#            "name_std": "Nevada",
#            "slug": "nevada"
#        }
#        region, created = Region.objects.get_or_create(pk=5509151, defaults=fields)
#        print "2", region, created
#
#        fields = {
#            "kind": "",
#            "elevation": None,
#            "name": "Black Rock City",
#            "country": country,
#            "region": region,
#            "name_std": "Black Rock City",
#            "subregion": None,
#            "location": "POINT (-119.2223670000000055 40.7815810000000027)",
#            "timezone": "",
#            "slug": "black-rock-city",
#            "population": 62000
#        }
#        brc, created = City.objects.get_or_create(pk=1, defaults=fields)
#        print "3", brc, created
#        print "3.1", brc.__class__, brc.__class__.__name__
#        print "3.2", dir(brc)
#
#        fields = {
#            "city": brc,
#            "name": "Black Rock City",
#            "country": "US",
#            "region": "Nevada",
#            "airport": None,
#            "location": "POINT (-119.2223670000000055 40.7815810000000027)",
#            "slug": "black-rock-city"
#        }
#        location, created = Location.objects.get_or_create(pk=147, defaults=fields)
#        print "4", location, created
#
#        fields = {
#            "name": "Burning Man HQ", 
#            "slug": "burning-man-hq"
#        }
#        regional, created = RegionalNode.objects.get_or_create(pk=1, defaults=fields)
#        print "5", regional, created
#
#        fields = {
#            "auth_type": AuthType.objects.get(pk=2),
#            "official": True,
#            "end_date": "2014-09-01",
#            "name": "Burning Man 2014",
#            "short_name": "BM14",
#            "regional": regional,
#            "slug": "burning-man-2014",
#            "current": True,
#            "airport": True,
#            "location": location,
#            "active": True,
#            "start_date": "2014-08-25"
#        }
#
#        Event.objects.get_or_create(id=8, defaults=fields)
#        print "6"

    except Exception as e:
        print('Error: %s' % e)
        pass


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0010_load_rtype_data'),
    ]

    operations = [
        migrations.RunPython(split_type)
    ]
