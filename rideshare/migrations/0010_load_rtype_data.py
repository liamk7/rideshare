# -*- coding: utf-8 -*-


from django.db import models, migrations
#from rideshare.models import Rideshare, Hauling, Ride, RideTypeChoice

def load_data(apps, schema_editor):
    RideTypeChoice = apps.get_model("rideshare", "RideTypeChoice")
    Rideshare = apps.get_model("rideshare", "Rideshare")
    offer = RideTypeChoice.objects.get(name='Offer')
    request = RideTypeChoice.objects.get(name='Request')
    try:
        qs = Rideshare.objects.filter(type__id__in=(2,4)).update(rtype=offer)
        qs = Rideshare.objects.filter(type__id__in=(1,3)).update(rtype=request)
    except:
        pass

class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0009_auto_20141106_1502'),
    ]

    operations = [
        migrations.RunPython(load_data)
    ]
