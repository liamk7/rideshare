# -*- coding: utf-8 -*-


from django.db import models, migrations
from django.utils import timezone


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0011_auth_type_data'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='rideshare',
            name='type',
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='created',
            field=models.DateTimeField(default=timezone.now(), verbose_name='created', editable=False),
            preserve_default=True,
        ),
    ]
