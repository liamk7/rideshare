# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0032_auto_20161028_2342'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='description',
            field=models.TextField(help_text=b'Event description. HTML allowed.', max_length=500, null=True, verbose_name=b'Description', blank=True),
            preserve_default=True,
        ),
    ]
