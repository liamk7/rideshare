# -*- coding: utf-8 -*-


from django.db import models, migrations
from django.utils import timezone


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0008_auto_20141106_1452'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rideshare',
            name='created',
            field=models.DateTimeField(default=timezone.now(), verbose_name='created', editable=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='rtype',
            field=models.ForeignKey(help_text=b'Are you offering or looking', to='rideshare.RideTypeChoice', on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
