# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0027_auto_20150528_1831'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='location',
            name='slug',
        ),
    ]
