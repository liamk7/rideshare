# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0026_auto_20150508_1719'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='background_image',
        ),
        migrations.RemoveField(
            model_name='event',
            name='short_name',
        ),
        migrations.RemoveField(
            model_name='rideshare',
            name='camp',
        ),
        migrations.RemoveField(
            model_name='rideshare',
            name='nearest_city',
        ),
        migrations.RemoveField(
            model_name='rideshare',
            name='region',
        ),
        migrations.RemoveField(
            model_name='rideshare',
            name='riders_weight',
        ),
    ]
