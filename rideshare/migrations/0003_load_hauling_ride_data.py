# -*- coding: utf-8 -*-


from django.db import models, migrations

#from rideshare.models import Hauling, Ride

def load_data(apps, schema_editor):
    Hauling = apps.get_model("rideshare", "Hauling")
    Ride = apps.get_model("rideshare", "Ride")
    Hauling(id=0, name='No').save()
    Hauling(id=1, name='Yes').save()
    Ride(id=0, name='No').save()
    Ride(id=1, name='Yes').save()


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0002_auto_20141106_1343'),
    ]

    operations = [
        migrations.RunPython(load_data)
    ]
