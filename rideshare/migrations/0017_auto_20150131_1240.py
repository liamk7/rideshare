# -*- coding: utf-8 -*-


from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0016_auto_20150130_1255'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='contact_phone',
            field=models.CharField(help_text=b'Enter your phone number', max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 31, 20, 40, 6, 708292, tzinfo=utc), verbose_name='created', editable=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 31, 20, 40, 6, 708333, tzinfo=utc), verbose_name='modified'),
            preserve_default=True,
        ),
    ]
