# -*- coding: utf-8 -*-


from django.db import models, migrations
from django.utils import timezone


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0006_auto_20141106_1430'),
    ]

    operations = [
        migrations.CreateModel(
            name='RideTypeChoice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Ride Type', max_length=200)),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='created',
            field=models.DateTimeField(default=timezone.now(), verbose_name='created', editable=False),
            preserve_default=True,
        ),
    ]
