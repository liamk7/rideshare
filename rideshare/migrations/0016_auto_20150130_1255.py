# -*- coding: utf-8 -*-


from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('rideshare', '0015_auto_20150130_1255'),
    ]

    operations = [
        migrations.AddField(
            model_name='regionalnode',
            name='site',
            field=models.ManyToManyField(help_text=b'Region is served by burningman.org as /this/ site', to='sites.Site', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 30, 20, 55, 23, 465944, tzinfo=utc), verbose_name='created', editable=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 30, 20, 55, 23, 465977, tzinfo=utc), verbose_name='modified'),
            preserve_default=True,
        ),
    ]
