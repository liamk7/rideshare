# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0020_direction_reverse_key'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='featured',
            field=models.BooleanField(default=False, help_text=b'Make this the featured event'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='current',
            field=models.BooleanField(default=False, help_text=b'Make this a current event'),
            preserve_default=True,
        ),
    ]
