# -*- coding: utf-8 -*-


from django.db import models, migrations
#from rideshare.models import Rideshare, Hauling, Ride

def split_type(apps, schema_editor):
    Hauling = apps.get_model("rideshare", "Hauling")
    Ride = apps.get_model("rideshare", "Ride")
    Rideshare = apps.get_model("rideshare", "Rideshare")
    hauling_yes = Hauling.objects.get(name='Yes')
    ride_yes = Ride.objects.get(name='Yes')
    try:
        qs = Rideshare.objects.filter(type__id__gte=3).update(hauling=hauling_yes)
        qs = Rideshare.objects.filter(type__id__lte=2).update(ride=ride_yes)
    except:
        pass

class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0004_auto_20141106_1349'),
    ]

    operations = [
        migrations.RunPython(split_type)
    ]
