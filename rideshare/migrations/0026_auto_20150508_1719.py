# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0025_auto_20150508_1710'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rideshare',
            name='riders_baggage_weight',
            field=models.DecimalField(help_text=b"FlightShare: Combined weight of riders' baggage", null=True, max_digits=12, decimal_places=8, blank=True),
            preserve_default=True,
        ),
    ]
