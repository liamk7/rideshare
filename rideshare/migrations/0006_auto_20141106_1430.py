# -*- coding: utf-8 -*-


from django.db import models, migrations
from django.utils import timezone


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0005_split_type_data'),
    ]

    operations = [
        migrations.AddField(
            model_name='rideshare',
            name='rtype',
            field=models.ForeignKey(related_name='r_rtype', default=1, to='rideshare.RideType', help_text=b'Are you offering or looking', on_delete=models.CASCADE),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='created',
            field=models.DateTimeField(default=timezone.now(), verbose_name='created', editable=False),
            preserve_default=True,
        ),
    ]
