# -*- coding: utf-8 -*-


from django.db import models, migrations
from django.utils import timezone


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Hauling',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Hauling', max_length=200)),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Ride',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Ride', max_length=200)),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='active',
            field=models.BooleanField(default=True, help_text=b'Indicates this Rideshare is active'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='created',
            field=models.DateTimeField(default=timezone.now(), verbose_name='created', editable=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='riders',
            field=models.PositiveSmallIntegerField(default=0, help_text=b'Number of open rider spots or spots wanted'),
            preserve_default=True,
        ),
    ]
