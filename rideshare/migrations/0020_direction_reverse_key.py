# -*- coding: utf-8 -*-


from django.db import models, migrations

def load_data(apps, schema_editor):
    Direction = apps.get_model("rideshare", "Direction")
    to = Direction.objects.get(key='TO')
    to.reverse_key = 'FROM'
    to.save()

class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0019_set_translation_key'),
    ]

    operations = [
        migrations.AddField(
            model_name='direction',
            name='reverse_key',
            field=models.CharField(default='TO', help_text=b'Translation key', max_length=30),
            preserve_default=False,
        ),
        migrations.RunPython(load_data)
    ]
