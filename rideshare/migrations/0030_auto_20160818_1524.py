# -*- coding: utf-8 -*-

import shortuuid

from django.db import models, migrations

def gen_uuid(apps, schema_editor):
    Rideshare = apps.get_model('rideshare', 'Rideshare')
    for row in Rideshare.objects.all():
        row.uid = shortuuid.ShortUUID().random(length=20)
        row.save()


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0029_auto_20160818_1523'),
    ]

    operations = [
        migrations.RunPython(gen_uuid),
    ]
