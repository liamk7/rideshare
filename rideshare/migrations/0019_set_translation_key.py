# -*- coding: utf-8 -*-


from django.db import models, migrations

def load_data(apps, schema_editor):
    db_alias = schema_editor.connection.alias
    Direction = apps.get_model("rideshare", "Direction")
    Hauling = apps.get_model("rideshare", "Hauling")
    Preference = apps.get_model("rideshare", "Preference")
    Ride = apps.get_model("rideshare", "Ride")
    RideTypeChoice = apps.get_model("rideshare", "RideTypeChoice")
    RoundTrip = apps.get_model("rideshare", "RoundTrip")

    #rtc_qs = RideTypeChoice.objects.using(db_alias).all()
    #for rtc in rtc_qs:
        #print 'before', rtc.id, rtc.name, rtc.key


    Direction.objects.using(db_alias).update_or_create(name='From', defaults={'id':1, 'key': 'FROM'})
    Direction.objects.using(db_alias).update_or_create(name='To', defaults={'id':0, 'key': 'TO'})
    Hauling.objects.using(db_alias).update_or_create(name='Yes', defaults={'id':1, 'key': 'YES'})
    Hauling.objects.using(db_alias).update_or_create(name='No', defaults={'id':0, 'key': 'NO'})
    Ride.objects.using(db_alias).update_or_create(name='Yes', defaults={'id':1, 'key': 'YES'})
    Ride.objects.using(db_alias).update_or_create(name='No', defaults={'id':0, 'key': 'NO'})
    RoundTrip.objects.using(db_alias).update_or_create(name='Round Trip', defaults={'id': 1, 'key': 'ROUND_TRIP'})
    RoundTrip.objects.using(db_alias).update_or_create(name='One Way', defaults={'id': 0, 'key': 'ONE_WAY'})
    RideTypeChoice.objects.using(db_alias).update_or_create(name='Offer', defaults={'id': 0, 'key': 'OFFER'})
    RideTypeChoice.objects.using(db_alias).update_or_create(name='Request', defaults={'id': 1, 'key': 'REQUEST'})
    Preference.objects.using(db_alias).update_or_create(name='non-smoking', defaults={'key': 'NON_SMOKING'})
    Preference.objects.using(db_alias).update_or_create(name='smoking', defaults={'key': 'SMOKING'})
    Preference.objects.using(db_alias).update_or_create(name='open minded', defaults={'key': 'OPEN_MINDED'})
    Preference.objects.using(db_alias).update_or_create(name='accommodate disabled passenger', defaults={'key': 'DISABLED'})

    #rtc_qs = RideTypeChoice.objects.using(db_alias).all()
    #for rtc in rtc_qs:
        #print 'after', rtc.id, rtc.name, rtc.key

class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0018_auto_20150208_2044'),
    ]

    operations = [
        migrations.RunPython(load_data)
    ]
