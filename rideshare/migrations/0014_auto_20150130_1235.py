# -*- coding: utf-8 -*-


from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('rideshare', '0013_auto_20150114_1728'),
    ]

    operations = [
        migrations.AddField(
            model_name='regionalnode',
            name='site',
            field=models.ForeignKey(blank=True, to='sites.Site', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contact',
            name='contact_info',
            field=models.TextField(help_text=b'Are there other ways (phone, post, carrier pigeon, etc) that the recipient of this message should contact you?', verbose_name=b'Your Contact Info', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 30, 20, 35, 13, 270487, tzinfo=utc), verbose_name='created', editable=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 30, 20, 35, 13, 270520, tzinfo=utc), verbose_name='modified'),
            preserve_default=True,
        ),
    ]
