# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0023_faq'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='event',
            options={'ordering': ['-featured', '-current', 'start_date']},
        ),
        migrations.AlterField(
            model_name='contact',
            name='contact_email',
            field=models.EmailField(help_text=b"What is your (the sender's) email address?", max_length=75, verbose_name=b'Sender Email', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contact',
            name='contact_info',
            field=models.TextField(help_text=b'Are there other ways (phone, post, carrier pigeon, etc) that the recipient of this message should contact you?', verbose_name=b'Sender Contact Info', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contact',
            name='contact_name',
            field=models.CharField(help_text=b"What is your (the sender's) name?", max_length=200, verbose_name=b'Sender Name', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contact',
            name='contact_phone',
            field=models.CharField(help_text=b'Enter your phone number', max_length=200, null=True, verbose_name=b'Sender Phone', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contact',
            name='message',
            field=models.TextField(help_text=b'Sender message to be sent.'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='short_name',
            field=models.CharField(help_text=b'Event short name', max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='slug',
            field=models.SlugField(unique=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='faq',
            name='answer',
            field=models.TextField(help_text=b'FAQ Answer', verbose_name=b'FAQ Answer'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='faq',
            name='language',
            field=models.CharField(help_text=b'Language', max_length=2, choices=[(b'en', b'English'), (b'es', b'Spanish'), (b'fr', b'French')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='faq',
            name='question',
            field=models.TextField(help_text=b'FAQ Question', verbose_name=b'FAQ Question'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='location',
            name='country',
            field=models.CharField(help_text=b'Country code', max_length=2, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='location',
            name='location',
            field=django.contrib.gis.db.models.fields.PointField(srid=4326, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='location',
            name='name',
            field=models.CharField(help_text=b'Name', max_length=200, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='location',
            name='region',
            field=models.CharField(help_text=b'Region', max_length=200, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='camp',
            field=models.CharField(help_text=b'DEPRECATED', max_length=200, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='contact_email',
            field=models.EmailField(max_length=75, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='contact_info',
            field=models.TextField(help_text=b'Other ways to contact you', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='nearest_city',
            field=models.CharField(help_text=b'DEPRECATED Nearest city in your region', max_length=200, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='region',
            field=models.ForeignKey(blank=True, to='rideshare.Region', help_text=b"DEPRECATED Region you're coming from", null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='riders',
            field=models.PositiveSmallIntegerField(help_text=b'Number of open rider spots or spots wanted', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='riders_weight',
            field=models.PositiveIntegerField(default=0, help_text=b'DEPRECATED FlightShare: Combined weight of riders', null=True, blank=True),
            preserve_default=True,
        ),
    ]
