# Generated by Django 2.1.7 on 2019-04-05 09:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0037_auto_20190329_0605'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Region',
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='contact_phone',
            field=models.CharField(blank=True, help_text='Enter your number for text-message (SMS) notification.', max_length=200, null=True),
        ),
    ]
