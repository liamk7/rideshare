# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0021_auto_20150217_2201'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='event',
            options={'ordering': ['-featured', '-current', '-start_date']},
        ),
        migrations.AddField(
            model_name='event',
            name='background_image',
            field=models.ImageField(null=True, upload_to=b'', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='header_image',
            field=models.ImageField(null=True, upload_to=b'', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='image',
            field=models.ImageField(null=True, upload_to=b'', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='active',
            field=models.BooleanField(default=True, help_text=b'Must be active to be displayed'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='airport',
            field=models.BooleanField(default=False, help_text=b'Airport is available'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='current',
            field=models.BooleanField(default=False, help_text=b'Make this a current event -- multiple per region'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='featured',
            field=models.BooleanField(default=False, help_text=b'Make this the featured event -- one per region'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='official',
            field=models.BooleanField(default=False, help_text=b'Official makes it visible in Rideshare site'),
            preserve_default=True,
        ),
    ]
