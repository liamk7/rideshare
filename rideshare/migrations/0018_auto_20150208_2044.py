# -*- coding: utf-8 -*-


from django.db import models, migrations

class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0017_auto_20150131_1240'),
    ]

    operations = [
        migrations.AddField(
            model_name='direction',
            name='key',
            field=models.CharField(default='TRANSLATION_KEY', help_text=b'Translation key', max_length=30),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='hauling',
            name='key',
            field=models.CharField(default='TRANSLATION_KEY', help_text=b'Translation key', max_length=30),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='preference',
            name='key',
            field=models.CharField(default='TRANSLATION_KEY', help_text=b'Translation key', max_length=20),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ride',
            name='key',
            field=models.CharField(default='TRANSLATION_KEY', help_text=b'Translation key', max_length=30),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ridetypechoice',
            name='key',
            field=models.CharField(default='TRANSLATION_KEY', help_text=b'Translation key', max_length=30),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='roundtrip',
            name='key',
            field=models.CharField(default='TRANSLATION_KEY', help_text=b'Translation key', max_length=30),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='contact',
            name='created',
            field=models.DateTimeField(help_text=b'Set automatically in save()', verbose_name='created', editable=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='created',
            field=models.DateTimeField(verbose_name='created', editable=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rideshare',
            name='modified',
            field=models.DateTimeField(verbose_name='modified'),
            preserve_default=True,
        ),
    ]
