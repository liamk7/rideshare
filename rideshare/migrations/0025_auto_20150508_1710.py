# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0024_auto_20150508_1707'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rideshare',
            name='riders_baggage_weight',
            field=models.PositiveIntegerField(help_text=b"FlightShare: Combined weight of riders' baggage", null=True, blank=True),
            preserve_default=True,
        ),
    ]
