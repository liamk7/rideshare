# Generated by Django 2.2 on 2019-04-11 21:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('rideshare', '0042_auto_20190405_0645'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='location',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='rideshare.Location'),
        ),
        migrations.AlterField(
            model_name='event',
            name='regional',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='rideshare.Regional'),
        ),
    ]
