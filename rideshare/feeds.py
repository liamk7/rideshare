import logging
from django.contrib.syndication.views import Feed
from rideshare.models import Rideshare, Event
from django.shortcuts import get_object_or_404

log = logging.getLogger(__name__)

class RideshareFeed(Feed):
    title = 'Burning Rideshares'
    link = '/rss/'
    description = "New Rideshares as they're posted!"
    feed_link = 'http://127.0.0.1/rideshare/rss/'

    def get_object(self, request, event_slug=None):
        return get_object_or_404(Event, slug=event_slug)

    def items(self, obj):
        return Rideshare.objects.filter(event=obj, active=True, deleted=False).order_by('-created')[:10]

    def item_title(self, item):
        return item.name

    def item_pubdate(self, item):
        return item.created

    def item_description(self, item):
        return item.description[:142] + "..."

    # item_link is only needed if RideshareItem has no get_absolute_url method.
    #def item_link(self, item):
        #return reverse('rideshare_detail', kwargs={'object_id':item.pk})
