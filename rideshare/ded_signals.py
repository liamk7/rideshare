import logging

from elasticsearch.exceptions import AuthorizationException
from django_elasticsearch_dsl.signals import RealTimeSignalProcessor
from django_elasticsearch_dsl.registries import registry

from .models import Rideshare

log = logging.getLogger(__name__)

class RideshareRealTimeSignalProcessor(RealTimeSignalProcessor):
    """ Custom signal handler to provide soft delete.  We are saving the Rideshare
        object with deleted flag set to True.  As far as Elasticsearch is concerned
        it should be deleted.
    """

    def handle_save(self, sender, instance, **kwargs):
            """Handle save.

            Given an individual model instance, update the object in the index.
            Update the related objects either.
            """
            try:
                if isinstance(instance, Rideshare) and instance.deleted:
                    registry.delete_related(instance)
                    registry.delete(instance, raise_on_error=False)
                else:
                    registry.update(instance)
                    registry.update_related(instance)
            except AuthorizationException as e:
                # Authorization is incorrectly configured with Elasticsearch server
                log.error(e)
