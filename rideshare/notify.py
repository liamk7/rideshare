import logging
import time

from django.conf import settings
from django.contrib.sites.models import Site
from django.core.mail.backends import smtp
from django.core.mail.backends import smtp, console
from django.core.mail import EmailMultiAlternatives, get_connection
from django.template.loader import render_to_string
from django.utils import timezone
from twilio.rest import Client
from twilio.rest import TwilioException
from validate_email import validate_email

log = logging.getLogger('notify')

# Now deprecated since using Mandrill, but leave here in case of future
# changes to email delivery system.
class RideshareBackend(smtp.EmailBackend):
    """ Custom SMTP backend so that we can set the timeout """
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('timeout', settings.SMTP_TIMEOUT)
        super(RideshareBackend, self).__init__(*args, **kwargs)

def _formatAddr(addr):
    """ Convenience function for formatting email addresses """
    return '{} <{}>'.format(addr, addr)

############################## SMS functions #############################
def _send_sms(phone, message, testing=False):
    """ Test the SMS system, independent of Django """
    if testing:
        from_number = settings.TWILIO_FROM_VALID
        acct_sid = settings.TWILIO_TEST_ACCOUNT_SID
        auth_token = settings.TWILIO_TEST_AUTH_TOKEN
    else:
        from_number = settings.TWILIO_FROM_NUMBER
        acct_sid = settings.TWILIO_ACCOUNT_SID
        auth_token = settings.TWILIO_AUTH_TOKEN

    twilio_client = Client(acct_sid, auth_token)

    try:
        ret = twilio_client.messages.create(
            to=phone,
            from_=from_number,
            body=message,
        )
        log.info('SMS to:%s from:%s', phone, from_number)
    except Exception as e:
        log.error('SMS to:%s from:%s reason:%s', phone, from_number, e)


def test_sms_system(phone, testing=False):
    """ Test the SMS system, independent of Django """
    current_domain = Site.objects.get_current().domain
    message = 'test_sms_system() from %s' % current_domain
    _send_sms(phone, message, testing) 


def send_sms(contact, request=None):
    """ Send an SMS message from person who created the Contact object to
        the Rideshare owner.
    """
    # SMS max message length
    ellipsis = '…'
    max_sms_length = 160 - len(ellipsis)

    # Two ways of making the link.  If you have the request, then better to use
    # it, but usually we don't have it.
    if request:
        link = request.build_absolute_uri(contact.rideshare.get_absolute_url())
    else:
        current_domain = Site.objects.get_current().domain
        link = 'https://{}{}'.format(
                current_domain,
                contact.rideshare.get_absolute_url()
                )

    message_preamble = 'Rideshare message! Click to view. {}\n'.format(link) 
    if len(message_preamble) + len(contact.message) > max_sms_length:
        max_length = max_sms_length - len(message_preamble)
        user_message = contact.message[:max_length] + ellipsis
    else:
        user_message = contact.message

    message = message_preamble + user_message
    phone = contact.rideshare.contact_phone

    _send_sms(phone, message)


############################## SMTP functions ############################

def _create_email_msg(contact, to_recipient=True, **kwargs):
    """ Create an EmailMessage from a Contact object.  The to_recipient
        arg indicates if this is going to the Rideshare recipient (True)
        or is a copy sent to the original sender (False).
    """
    request = kwargs.pop('request', None)
    from_email = settings.DEFAULT_FROM_EMAIL
    rideshare = contact.rideshare
    headers = {}

    # Don't know exactly how these will affect Regionals
    site = Site.objects.get_current()
    try:
        hostname = site.domain
        site_name = site.name
        regional_name = site.regional_set.all().first().name
    except AttributeError:
        log.error('Regional must have a Site associated with it')
        hostname = 'Unknown host'
        site_name = 'Unknown site'
        regional_name = 'Unknown region'


    content = {
        'name': contact.contact_name,
        'email': contact.contact_email,
        'info': contact.contact_info,
        'phone': contact.contact_phone,
        'message': contact.message,
        'request': request,
        'rideshare': rideshare,
        'to_recipient': to_recipient,
        'hostname': hostname,
        'regional_name': regional_name,
        'site_name': site_name,
        }
    html_content = render_to_string('rideshare/rideshare_email.html', content)
    text_content = render_to_string('rideshare/rideshare_email.txt', content)

    em = EmailMultiAlternatives()
    em.subject = '📧 {} {} {}'.format(regional_name, site_name, 'Contact Email')
    em.from_email = from_email
    em.body = text_content
    em.attach_alternative(html_content, 'text/html')

    if to_recipient:
        em.to = [_formatAddr(rideshare.contact_email)]
        em.reply_to = [contact.contact_email]
    else:
        em.subject += ' (Copy)'
        em.to = [_formatAddr(contact.contact_email)]

    return em

def test_email_system(recipient_email):
    """ Test the SMTP system, independent of Django.
        Note: during testing the EMAIL_BACKEND is silently changed to
        the locmem.EmailBackend, so messages are not sent.  In the test
        suite, we're overriding so that we can test if the system is
        actually set up correctly and sending emails.
    """
    from_email = settings.DEFAULT_FROM_EMAIL
    current_domain = Site.objects.get_current().domain
    subject = 'Rideshare System Test Email'

    text_content = 'Message sent from Django test framework to test deliverability.'
    html_content = """
        <html>
          <head>
            <title>Test</title>
          </head>
          <body>
            <h1>Test</h1>
            <p>{}
            <p>Time: {}</p>
            <p>Domain: {}</p>
          </body>
        </html>
        """.format(text_content, timezone.now(), current_domain)

    # Make sure we have a valid from and to email
    try:
        assert validate_email(from_email), 'Missing or invalid from address... check settings.DEFAULT_FROM_EMAIL'
        assert validate_email(recipient_email), 'Missing or invalid email address... check settings.EMAIL_TEST_ADDR'
    except AssertionError as e:
        log.warning(e)
        raise

    em = EmailMultiAlternatives()
    em.subject = subject
    em.from_email = from_email
    em.to = [recipient_email]
    em.body = text_content
    em.attach_alternative(html_content, 'text/html')

    try:
        em.send()
    except Exception as e:
        log.error(e)
        raise


def send_email(contact, *args, **kwargs):
    """ Create and send two email messages, one to the rideshare owner
        and a copy to the person contacting them.
    """
    # Let's just log the errors
    try:
        assert(contact), 'contact is a required argument'
        assert(contact.rideshare), 'contact.rideshare is missing'
    except AssertionError as e:
        log.error('Missing argument "contact" to send_email(): %s', e)
        return
    except DoesNotExist:
        log.error('Contact missing rideshare: %s', contact)
        return

    rideshare = contact.rideshare

    # Make sure we have valid email addresses
    try:
        assert validate_email(rideshare.contact_email), 'Missing or invalid recipient email: "{}"'.format(rideshare.contact_email)
        assert validate_email(contact.contact_email), 'Missing or invalid contact email: "{}"'.format(contact.contact_email)
    except AssertionError as e:
        log.warning('{} for Rideshare "{}" — not sent'.format(e, rideshare))
        return

    # No email sending in KioskMode, or when overriding (for testing?)
    #if (settings.KIOSK_MODE == False or settings.EMAIL_OVERRIDE):
    # Go ahead and send email, now that we are using cloud server!
    msg1 = _create_email_msg(contact, to_recipient=True, **kwargs)
    msg2 = _create_email_msg(contact, to_recipient=False, **kwargs)

    # Mass email to reduce number of smtp connections
    # Try twice, but we can't wait too long!  Probably should really use
    # a message queue.
    connection = get_connection(fail_silently=False)
    try:
        connection.send_messages([msg1, msg2])
        log.info('Email (and originator copy) originator:%s recipient:%s',
            contact.contact_email, rideshare.contact_email)
    except Exception as e:
        time.sleep(1)
        log.warning('First attempt failed from contact:%s (%s)', contact, e)
        try:
            connection.send_messages([msg1, msg2])
            log.info('Second attempt succeeded from contact:%s', contact)
        except Exception as e:
            log.error('Second attempt failed from contact:%s (%s)', contact, e)


