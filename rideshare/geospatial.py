#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Functions for calculating distances and bounding boxes.
Adapted from original found here:
http://janmatuschek.de/LatitudeLongitudeBoundingCoordinates
Also, contains functions for locale-specific formatting of distance values.

Commented out the locale-specific stuff that depended on babel.  This
was the only place it was referenced and wasn't even being used.
"""

import logging
from math import sin, cos, asin, sqrt, degrees, radians, pi
from django.contrib.gis.geos import LinearRing
from django.contrib.gis.measure import Distance
#from babel.core import Locale, UnknownLocaleError
#from babel.numbers import format_decimal

log = logging.getLogger(__name__)

Earth_radius_km = 6371.0
RADIUS = Earth_radius_km
MIN_LAT = radians(-90)
MAX_LAT = radians(90)
MIN_LNG = radians(-180)
MAX_LNG = radians(180)

def haversine(angle_radians):
    return sin(angle_radians / 2.0) ** 2

def inverse_haversine(h):
    return 2 * asin(sqrt(h)) # radians

def distance_between_points(lat1, lon1, lat2, lon2):
    """ All args are in degrees.
        WARNING: loss of absolute precision when points are near-antipodal
    """
    lat1 = radians(lat1)
    lat2 = radians(lat2)
    dlat = lat2 - lat1
    dlon = radians(lon2 - lon1)
    h = haversine(dlat) + cos(lat1) * cos(lat2) * haversine(dlon)
    return RADIUS * inverse_haversine(h)

def check_bounds(lat, lng):
    if lat < MIN_LAT or lat > MAX_LAT:
        raise ValueError('lat out of bounds (%s)' % (lat))
    if lng < MIN_LNG or lng > MAX_LNG:
        raise ValueError('lng out of bounds (%s)' % (lng))

def bbox_from_point(point, distance):
    """ point.y is latitude, point.x is longitude, in degrees """
    coords = bbox(point.y, point.x, distance)
    return make_bbox_geometry(*coords)

def bbox(lat, lng, distance):
    """ Input and output lats/lngs are in degrees.
        Distance arg must be in same units as RADIUS, i.e. kilometers.
        Returns a LinearRing geometry object.
    """
    lat = radians(lat)
    lng = radians(lng)

    check_bounds(lat, lng)
    rad_dist = distance / RADIUS

    min_lat = lat - rad_dist
    max_lat = lat + rad_dist

    if min_lat > MIN_LAT and max_lat < MAX_LAT:
        delta_lng = asin(sin(rad_dist) / cos(lat))
        min_lng = lng - delta_lng
        if min_lng < MIN_LNG: min_lng += 2 * pi
        max_lng = lng + delta_lng
        if max_lng > MAX_LNG: max_lng -= 2 * pi
    else:
        min_lat = max(min_lat, MIN_LAT)
        max_lat = min(max_lat, MAX_LAT)
        min_lng = MIN_LNG
        max_lng = MAX_LNG

    return list(map(degrees, (min_lat, min_lng, max_lat, max_lng)))

def make_bbox_geometry(*args):
    """ Make a LinearRing bounding box from two points.
        Note that lat/lng ordering is different for LinearRing.
    """
    (min_lat, min_lng, max_lat, max_lng) = args
    linear_ring = LinearRing(
        (min_lng, min_lat),
        (max_lng, min_lat),
        (max_lng, max_lat),
        (min_lng, max_lat),
        (min_lng, min_lat)
    )
    return linear_ring

def make_lr_geometry(*args):
    """ Create a LinearRing geometry object from a series of lat/lng values.
        Reverses lat/long coordinates, takes variable number of args.
        Haven't tested this.
    """
    coords = list(zip(args[1::2], args[::2]))
    coords.append(coords[0])
    linear_ring = LinearRing(*coords)
    return linear_ring

#def _parse_locale(locale_string):
#    """ Given an AcceptLanguage locale_string, return a Locale object """
#    try:
#        main_locale = locale_string.split(',')[0]
#        return Locale.parse(main_locale)
#    except (UnknownLocaleError, ValueError):
#        try:
#            return Locale.parse(main_locale, sep='-')
#        except:
#            return Locale.parse('en_US')
#    except AttributeError:
#        log.error('Bad value for locale_string: %s', locale_string)
#        return Locale.parse('en_US')
#    except Exception as e:
#        log.error('Exception parsing locale_string: %s (%s)', locale_string, e)
#        return Locale.parse('en_US')
#
#def distance_format(dist, arg):
#    """ Return distance formatted based on Locale.  The arg can be
#	an HttpRequest object or an AcceptLanguage string.  Normally the
#	distance is a Distance object, but it can also be a numeric
#	(in meters)
#
#        Convert a Distance object or a numeric value (in meters) to a string,
#        formatted according to the locale derived from the locale_string.
#        For short distances (< 0.1 mi or < 1 km) round off fractional
#        units and change unit to feet or meters.
#
#        If the locale is en_US then use miles and feet, otherwise metric.
#    """
#    try:
#	# if it was a HttpRequest object, get the locale string
#        locale_string = arg.META.get('HTTP_ACCEPT_LANGUAGE', 'en-US')
#    except Exception as e:
#	# assume it was a raw locale_string already
#        locale_string = arg
#
#    if not dist: return '';
#    try: assert type(dist) == Distance
#    except AssertionError: dist = Distance(m=dist)
#
#    loc = _parse_locale(locale_string)
#    if loc == Locale('en', 'US'):
#        unit = 'mi'
#        if dist.mi < 0.1:
#            unit = 'ft'
#            rdist = round(dist.ft)
#        elif dist.mi < 1.0:
#            rdist = round(dist.mi, 2)
#        elif dist.mi < 10.0:
#            rdist = round(dist.mi, 1)
#        else:
#            rdist = round(dist.mi)
#    else:
#        unit = 'km'
#        if dist.km < 1.0:
#            unit = 'm'
#            rdist = round(dist.m)
#        elif dist.km < 10.0:
#            rdist = round(dist.km, 1)
#        else:
#            rdist = round(dist.km)
#    return "%s %s" % (format_decimal(rdist, locale=loc), unit)
#
#
#if __name__ == "__main__":
#    """ Could provide better tests here. """
#    # Examples from Jan Matuschek's article
#    def test(lat, lon, dist):
#        print("test bounding box", lat, lon, dist)
#        coords = bbox(lat, lon, dist)
#        print(coords)
#        print(make_bbox_geometry(*coords))
#
#    print("liberty to eiffel, should be about 5837 km: ", end='')
#    print(distance_between_points(40.6892, -74.0444, 48.8583, 2.2945)) # about 5837 km
#    print()
#    print("calc bounding box from lat, lng, distance")
#    degs = list(map(degrees, (1.3963, -0.6981)))
#    print(degs)
#    bbox = test(degs[0], degs[1], 1000)
#    print(bbox)
#
