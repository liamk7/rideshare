from django.apps import AppConfig
from django.conf import settings

class RideshareConfig(AppConfig):
    name = 'rideshare'
    verbose_name = "Rideshare"
    signal_processor = None

    def ready(self):
        if not self.signal_processor:
            try:
                import rideshare.signals
                self.signal_processor = True
            except ImportError:
                raise
