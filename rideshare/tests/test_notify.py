import logging
import unittest

from django.test import TestCase as DjangoTestCase
from django.test.utils import override_settings
from django.conf import settings
from django.core import mail
from django.core.management import call_command

from api.factories import RideshareFactory, ContactFactory
from rideshare.models import Contact, Rideshare
from rideshare import notify

# logging.disable gives us cleaner output
log = logging.getLogger(__name__)
logging.disable(logging.WARNING)

def setUpModule():
    """ This loads initial data for testing from a fixture.  There is also
        some basic data loaded as a result of migrations, which are also run
        prior to testing.  There are no Rideshare objects created.
        Cities
        Airports
        Site
        Regional
        Event
    """
    call_command('loaddata', 'rideshare/fixtures/api_test_data.json', verbosity=0)

class NotifySystemTestCase(DjangoTestCase):
    def test_sms_system(self):
        # This actually sends a test SMS message
        notify.test_sms_system(settings.SMS_TEST_NUMBER)

    def test_sms_system_testing(self):
        # This just verifies config is okay and gets http 200 response
        notify.test_sms_system(settings.SMS_TEST_NUMBER, testing=True)

    @override_settings(EMAIL_BACKEND = 'anymail.backends.mandrill.EmailBackend')
    def test_email_system(self):
        # This actually sends a test email message
        notify.test_email_system(settings.EMAIL_TEST_ADDR)

class NotifyTestCase(DjangoTestCase):
    def test_email_notify(self):
        self.rideshare = RideshareFactory(
                contact_phone=settings.SMS_TEST_NUMBER,
                contact_email=settings.EMAIL_TEST_ADDR,
                sms_notify=False,
                )
        self.contact = ContactFactory(
                rideshare=self.rideshare,
                contact_email=settings.EMAIL_TEST_ADDR
                )
        # The setUp() method already created an email message when the Contact
        # object was saved.  Let's look to see if it has the right stuff.

        self.assertEqual(len(mail.outbox), 2,
                'Exactly two email messages should be generated')

        # outbox is a special in-memory array of messsages that
        # would have been sent if not testing.
        msg1, msg2 = mail.outbox

        self.assertIn(self.rideshare.contact_email,
                msg1.to[0], 'Check recipients')

        self.assertIn(self.contact.contact_email,
                msg1.reply_to, 'Check Reply-To')

        self.assertIn(self.contact.contact_email,
                msg2.to[0], 'Check copy recipients')

        self.assertIn('Copy', msg2.subject, 'Missing Copy?')

        # fields common to both email messages
        for msg in mail.outbox:
            self.assertIn('Contact Email',
                    msg.subject, 'Missing subject?')
            self.assertEqual('plain',
                    msg.content_subtype, 'Must be text/plain type')
            self.assertIn(settings.DEFAULT_FROM_EMAIL,
                    msg.from_email, 'Check DEFAULT_FROM_EMAIL')
            # The alternative list contains tuples: (content, mimetype)
            mime_types = [m[1] for m in msg.alternatives]
            self.assertIn('text/html', mime_types, 'Missing HTML alternative')

    def test_sms_notify(self):
        """ Note that the SMS message is sent as a side effect of saving the
            Contact object.  You should receive an SMS message on 
            settings.SMS_TEST_NUMBER when running this test.
            Email uses the locmem backend for testing (see above) so no emails
            are actually sent or checked here.
        """
        self.rideshare = RideshareFactory(
                contact_phone=settings.SMS_TEST_NUMBER,
                contact_email=settings.EMAIL_TEST_ADDR
                )
        self.contact = ContactFactory(
                rideshare=self.rideshare,
                contact_email=settings.EMAIL_TEST_ADDR
                )

