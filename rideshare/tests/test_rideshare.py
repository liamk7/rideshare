import unittest
import logging
import time

from cities.models import City
from airports.models import Airport
from django.test import TestCase as DjangoTestCase
from django.contrib.auth.models import User
from django.core.mail import outbox
from django.core.management import call_command
from rest_framework.test import APITestCase, APISimpleTestCase

from api.factories import RideshareFactory
from rideshare.documents import RideshareDocument
from rideshare.models import (
    Contact,
    Event,
    Location,
    Rideshare,
)

# Method                  Checks that
# ------                  -----------
# assertEqual(a, b)       a == b   
# assertNotEqual(a, b)    a != b   
# assertTrue(x)           bool(x) is True      
# assertFalse(x)          bool(x) is False     
# assertIs(a, b)          a is b
# assertIsNot(a, b)       a is not b
# assertIsNone(x)         x is None
# assertIsNotNone(x)      x is not None
# assertIn(a, b)          a in b
# assertNotIn(a, b)       a not in b
# assertIsInstance(a, b)  isinstance(a, b)
# assertNotIsInstance(a, b)   not isinstance(a, b)

# logging.disable gives us cleaner output
#log = logging.getLogger(__name__)
#logging.disable(logging.ERROR)

def setUpModule():
    """ This loads initial data for testing from a fixture.  There is also
        some basic data loaded as a result of migrations, which are also run
        prior to testing.  There are no Rideshare objects created.
        Cities
        Airports
        Site
        Regional
        Event
    """
    call_command('loaddata', 'rideshare/fixtures/api_test_data.json', verbosity=0)

#    # Add Location objects for all the cities and airports
#    try:
#        [Location.from_obj(city) for city in City.objects.all()]
#        [Location.from_obj(airport) for airport in Airport.objects.all()]
#    except DuplicateLocationError as e:
#        cities = Location.objects.filter(city__isnull=False)
#        airports = Location.objects.filter(airport__isnull=False)
#        if cities: log.warning('City Locations already exist: {}'.format(cities))
#        if airports: log.warning('Airport Locations already exist: {}'.format(airports))
#        pass

#def create_initial_data():
#    """ This loads initial data for testing from a fixture.  There is also
#        some basic data loaded as a result of migrations, which are also run
#        prior to testing.  There are no Rideshare objects created.
#    """
#    call_command('loaddata', 'api_test_data.json', verbosity=1)

class InitialDataTestCase(DjangoTestCase):
    """ Load initial data from json file, for tests that inherit from this.
    """
    def setUp(self):
        pass
        #create_initial_data()

#    # tracking down some kind of integrity problem in api test data json?
#    def integrity(self):
#        locations = Location.objects.filter(city__isnull=False)
#        for loc in locations:
#            print(loc.name, loc.id)
#            print(loc.city, loc.city.id)

"""
Add some tests for object indexing.
1. Creating a Rideshare to/from a Location that does not exist should
create the Location, and index (via django_elasticsearch) the Location.
Can also test the lookup via /api/autocities/.
2. Creating a Rideshare should add it to the Hayastack rideshare index.
Again, should also test the /api/autorideshares search.
"""


class EventTestCases(InitialDataTestCase):
    def setUp(self):
        super(EventTestCases, self).setUp()
        self.batch_size = 20
        #self.event = EventFactory.create()
        self.event = Event.objects.all().first()
        RideshareFactory.create_batch(self.batch_size, event=self.event)

    def test_event_delete(self):
        """ Make sure deleting event deletes all Rideshares.
            Also, for events with playa auth, delete users.
        """
        ev = Event.objects.get(id=self.event.id)
        self.assertEqual(self.event.name, ev.name)
        rideshares = Rideshare.objects.filter(event=ev)
        self.assertEqual(rideshares.count(), self.batch_size)
        user_list = [rs.user for rs in rideshares]
        users = User.objects.filter(username__in=[u.username for u in user_list])
        self.assertEqual(users.count(), len(user_list))

        # This should delete the event, its associated rideshares.
        # If the event auth type is 'playa', then it should also 
        # delete the rideshares' users.
        deleted = ev.delete()

        with self.assertRaises(Event.DoesNotExist):
            Event.objects.get(id=self.event.id)

        rideshares = Rideshare.objects.filter(event=ev)
        self.assertEqual(rideshares.count(), 0)
        users = User.objects.filter(username__in=[u.username for u in user_list])
        self.assertEqual(users.count(), 0)


class LocationTestCases(InitialDataTestCase):
    def setUp(self):
        Location.objects.all().delete()
        self.city = City.objects.all().order_by('?').first()
        self.airport = Airport.objects.all().order_by('?').first()

    def tearDown(self):
        Location.objects.all().delete()

    def test_location_create(self):
        """ Create location with params """
        params = {'name':'Test Location', 'region':'Random State', 'country':'US'}

        loc = Location.objects.create(city=self.city, **params)
        self.assertTrue(loc.name == 'Test Location')

        loc = Location.objects.create(airport=self.airport, **params)
        self.assertTrue(loc.name == 'Test Location')

    def test_location_create_obj(self):
        """ Create location from obj """
        loc = Location.objects.create(city=self.city)
        loc = Location.objects.get(city=self.city.id)
        self.assertTrue(loc.name == self.city.name)

        loc = Location.objects.create(airport=self.airport)
        loc = Location.objects.get(airport=self.airport.id)
        self.assertTrue(loc.name == self.airport.name)

    def test_location_get_or_create(self):
        """ Create location from obj """
        loc, created = Location.objects.get_or_create(city=self.city)
        self.assertTrue(created)
        loc, created = Location.objects.get_or_create(city=self.city)
        self.assertFalse(created)

        loc, created = Location.objects.get_or_create(airport=self.airport)
        self.assertTrue(created)
        loc, created = Location.objects.get_or_create(airport=self.airport)
        self.assertFalse(created)

    def test_location_get_or_create_variant(self):
        """ Create location from obj """
        loc, created = Location.objects.get_or_create(city=self.city, airport=None)
        self.assertTrue(created)
        loc, created = Location.objects.get_or_create(city=self.city, airport=None)
        self.assertFalse(created)

        loc, created = Location.objects.get_or_create(airport=self.airport, city=None)
        self.assertTrue(created)
        loc, created = Location.objects.get_or_create(airport=self.airport, city=None)
        self.assertFalse(created)

    def test_location_integrity_error(self):
        """ Test exception when incorrect combination of city/airport """
        with self.assertRaises(Location.IntegrityError):
            loc, created = Location.objects.get_or_create(city=self.city, airport=self.airport)

        with self.assertRaises(Location.IntegrityError):
            loc, created = Location.objects.get_or_create(city=None, airport=None)

    def test_location_duplicate_error(self):
        """ Test exception when duplicate city/airport """
        with self.assertRaises(Location.DuplicateError):
            loc = Location.objects.create(city=self.city)
            loc = Location.objects.create(city=self.city)

        with self.assertRaises(Location.DuplicateError):
            loc = Location.objects.create(airport=self.airport)
            loc = Location.objects.create(airport=self.airport)


class ElasticsearchRideshareTestCases(InitialDataTestCase):
    def setUp(self):
        self.batch_size = 20
        self.event = Event.objects.all().first()
        call_command('search_index', '--models=rideshare.Rideshare', '--rebuild', '-f', '--verbosity=0')
        RideshareFactory.create_batch(self.batch_size, event=self.event)

    def test_rideshare_indexing(self):
        """ Make sure the Rideshares got indexed.
        """
        matches = RideshareDocument.match('Jabberwock', event_slug=self.event.slug, size=10)
        self.assertTrue(len(matches) > 0)
        matches = RideshareDocument.match('Jabber', event_slug=self.event.slug, size=10)
        self.assertTrue(len(matches) > 0)

    def test_rideshare_deletion(self):
        """ Make sure the Rideshare gets deleted from index.
        """
        name = 'Paradigm Shift'
        description = 'A concept identified by the American physicist and philosopher Thomas Kuhn, '\
            'is a fundamental change in the basic concepts and experimental practices of '\
            'a scientific discipline.'

        # Sleep for a second so Elasticsearch has time to index the new object
        rs = RideshareFactory.create(name=name, description=description, event=self.event, flight=False)
        time.sleep(1)
        # Match in name
        matches = RideshareDocument.match('paradigm', event_slug=self.event.slug, size=10)
        self.assertEqual(len(matches), 1)

        # Match in description
        matches = RideshareDocument.match('philosopher', event_slug=self.event.slug, size=10)
        self.assertEqual(len(matches), 1)

        # Sleep for a second so Elasticsearch has time to delete object from index
        rs.delete()
        time.sleep(1)
        matches = RideshareDocument.match('Paradigm', event_slug=self.event.slug, size=10)
        self.assertEqual(len(matches), 0)

    def test_rideshare_save(self):
        """ Make sure the Rideshare gets saved to index.
        """
        name = 'Paradigm Shift'
        description = 'A concept identified by the American physicist and philosopher Thomas Kuhn, '\
            'is a fundamental change in the basic concepts and experimental practices of '\
            'a scientific discipline.'

        # Sleep for a second so Elasticsearch has time to index the new object
        rs = RideshareFactory.create(name=name, description=description, event=self.event, flight=False)
        time.sleep(1)
        # Match in name
        matches = RideshareDocument.match('paradigm', event_slug=self.event.slug, size=10)
        self.assertEqual(len(matches), 1)

        # Sleep for a second so Elasticsearch has time to save object to index
        rs.name = 'Anti-fragile'
        rs.save()
        time.sleep(1)
        matches = RideshareDocument.match('Anti-fragile', event_slug=self.event.slug, size=10)
        self.assertEqual(len(matches), 1)

    def test_rideshare_soft_delete(self):
        """ Make sure the Rideshare gets deleted from index.
        """
        name = 'Paradigm Shift'

        # Sleep for a second so Elasticsearch has time to index the new object
        rs = RideshareFactory.create(name=name, event=self.event, flight=False)
        time.sleep(1)
        # Match in name
        matches = RideshareDocument.match('paradigm', event_slug=self.event.slug, size=10)
        self.assertEqual(len(matches), 1)

        # Sleep for a second so Elasticsearch has time to save object to index
        rs.deleted = True
        rs.save()
        time.sleep(1)
        matches = RideshareDocument.match('Paradigm', event_slug=self.event.slug, size=10)
        self.assertEqual(len(matches), 0)

