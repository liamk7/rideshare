import json
import logging
import pprint
import unittest

from django.test import SimpleTestCase

from api.factories import RideshareFactory
from rideshare.documents import (
    LocationDocument,
    RideshareDocument,
    CityDocument,
    AirportDocument,
)

# logging.disable gives us cleaner output
log = logging.getLogger(__name__)
logging.disable(logging.ERROR)

def setUpModule():
    """ This loads initial data for testing from a fixture.  There is also
        some basic data loaded as a result of migrations, which are also run
        prior to testing.  There are no Rideshare objects created.
        Cities
        Airports
        Site
        Regional
        Event
    """
    log.info('Location/City/Airport/Rideshare Objects must already be indexed for this to work!')

class InitialDataTestCase(SimpleTestCase):
    """ Load initial data from json file, for tests that inherit from this.
    """
    # Expected result values, depending on country/continent.
    black_rock_city = 'Black Rock City, Nevada, US'
    gerlach = 'Gerlach, Nevada, US'
    sf_ca_us = 'San Francisco, California, US'
    sf_ba_es = 'Sant Francesc de Formentera, Balearic Islands, ES'
    ksfo = 'San Francisco International Airport, California, US (KSFO, SFO)'

    def list_options(self, options):
        """
        This function is just for debugging.
        This is the format of the options argument:
        [
            { 'text': 'San Francisco',
              '_index': 'cities',
              '_type': 'doc',
              '_id': '5391959',
              '_score': 6918528.0,
              '_source': {
                'name_composite': 'San Francisco, California, US'
              },
              'contexts': {'countrycontext': ['US']}
            }
         ]
        """
        for index, opt in enumerate(options, start=1):
            print('{}. {} (score={}, id={})'.format(
                index,
                opt._source.name_composite,
                opt._score,
                opt._id,
            ))

"""
Add some tests for object indexing.
1. Creating a Rideshare to/from a Location that does not exist should
create the Location, and index (via django_elasticsearch) the Location.
Can also test the lookup via /api/autocities/.
2. Creating a Rideshare should add it to the Hayastack rideshare index.
Again, should also test the /api/autorideshares search.
"""

class ElasticsearchCityTestCases(InitialDataTestCase):
    def test_city_suggest_sanfran(self):
        text = 'san fran'

        options = CityDocument.suggestions(text, country_ctx='US')
        self.assertEqual(options[0]._source.name_composite, self.sf_ca_us)
        self.assertEqual(len(options), 1)

        options = CityDocument.suggestions(text, continent_ctx='NA')
        self.assertEqual(options[0]._source.name_composite, self.sf_ca_us)

        options = CityDocument.suggestions(text, country_ctx='ES')
        self.assertEqual(options[0]._source.name_composite, self.sf_ba_es)
        self.assertEqual(len(options), 1)

        options = CityDocument.suggestions(text, country_ctx=['ES', 'US'])
        self.assertEqual(len(options), 2)
        self.assertEqual(options[0]._source.name_composite, self.sf_ca_us)
        self.assertEqual(options[1]._source.name_composite, self.sf_ba_es)

    def test_city_suggest_snafran(self):
        """ Test typo in san fran.  This does not match in Spain, since
            'Sant Fran' is > 1 Levenshtein substitution.
        """
        text = 'sna fran'

        options = CityDocument.suggestions(text, country_ctx='US')
        self.assertEqual(options[0]._source.name_composite, self.sf_ca_us)
        #self.assertEqual(len(options), 1)

        options = CityDocument.suggestions(text, continent_ctx='NA')
        self.assertEqual(options[0]._source.name_composite, self.sf_ca_us)
        #self.assertEqual(len(options), 1)

        options = CityDocument.suggestions(text, country_ctx='ES')
        self.assertEqual(len(options), 0)

class ElasticsearchAirportTestCases(InitialDataTestCase):
    """ Airport tests have fuzzy match turned off for better matching
        on airport codes.
    """
    def test_airport_suggest_sanfran(self):
        text = 'san fran'

        options = AirportDocument.suggestions(text, country_ctx='US', fuzzy={'fuzziness':0})
        self.assertEqual(options[0]._source.name_composite, self.ksfo)

        options = AirportDocument.suggestions(text, continent_ctx='NA', fuzzy={'fuzziness':0})
        self.assertEqual(options[0]._source.name_composite, self.ksfo)


    def test_airport_suggest_snafran(self):
        """ Bad spelling should return no airports.
        """
        text = 'sna fran'

        options = AirportDocument.suggestions(text, country_ctx='US', fuzzy={'fuzziness':0})
        self.assertEqual(options, [])

    def test_airport_suggest_ksfo(self):
        """ Test the IATA code for San Francisco International Airport
        """
        text = 'ksfo'

        options = AirportDocument.suggestions(text, country_ctx='US', fuzzy={'fuzziness':0})
        self.assertEqual(options[0]._source.name_composite, self.ksfo)


class ElasticsearchLocationTestCases(InitialDataTestCase):

    def test_location_suggest_brc_continent(self):
        """
        """
        options = LocationDocument.suggestions('Black Rock City', continent_ctx='NA')
        self.assertEqual(options[0]._source.name_composite, self.black_rock_city)

    def test_location_suggest_brc_country(self):
        """
        """
        options = LocationDocument.suggestions('Black Rock City', country_ctx='US')
        self.assertEqual(options[0]._source.name_composite, self.black_rock_city)

