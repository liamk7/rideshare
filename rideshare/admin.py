from django.contrib.gis import admin

from rideshare.models import *

admin.site.register(Regional)
admin.site.register(AuthType)

class SecureOSM(admin.OSMGeoAdmin):
    """ Override this class because the default uses HTTP, which causes 
        a Mixed Content exception in the browser.
    """
    openlayers_url = 'https://cdnjs.cloudflare.com/ajax/libs/openlayers/2.13.1/OpenLayers.js'

class LocationAdmin(SecureOSM): # OSMGeoAdmin displays map features
    list_display = ['name', 'region', 'country']
    list_filter = ['region', 'country']
    search_fields = ['name', 'region','country']
    raw_id_fields = ['city', 'airport']

    autocomplete_lookup_fields = {
        'fk': ['city', 'airport'],
    }
admin.site.register(Location, LocationAdmin)

class FaqAdmin(admin.ModelAdmin):
    list_filter = ['language']
    list_editable = ['order',]
    list_display = ['question', 'order', 'language']
    class Media:
        js = [
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/rideshare/tinymce_setup/tinymce_setup.js'
        ]
admin.site.register(FAQ, FaqAdmin)

class EventAdmin(admin.ModelAdmin):
    list_filter = ['regional']
    list_display = [
            'name',
            'slug',
            'start_date',
            'end_date',
            'featured',
            'current',
            'active',
            'official',
            'airport',
            ]
    readonly_fields = ['slug']
    list_editable = ['current', 'featured', 'active', 'official']
    view_on_site = True
    #prepopulated_fields = {"slug": ("name",)}
    filter_horizontal = ['country_search_contexts']
admin.site.register(Event, EventAdmin)

class PreferenceAdmin(admin.ModelAdmin):
    list_display = ['name','key','order']
    list_editable = ['order',]
admin.site.register(Preference, PreferenceAdmin)

class RideshareAdmin(admin.ModelAdmin):
    list_display = ['name', 'direction', 'event', 'location', 'rtype', 'round_trip', 'deleted']
    list_filter = ['event', 'direction', 'location__region',
            'location__country', 'rtype', 'flight', 'full', 'deleted']
    search_fields = ['name', 'contact_name','contact_email','contact_info',
        'description']
    view_on_site = True
admin.site.register(Rideshare, RideshareAdmin)

class ContactAdmin(admin.ModelAdmin):
    list_display = ['rideshare', 'contact_name', 'contact_email',]
    search_fields = ['contact_name', 'contact_email', 'contact_info',]
    readonly_fields = ['rideshare']
admin.site.register(Contact, ContactAdmin)
