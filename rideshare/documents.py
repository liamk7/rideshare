"""
Document definitions associated with django_elasticsearch_dsl usage.
We use Elasticsearch for Airports, Cities, Locations and Rideshare objects.

The fuzzy query can take specific fuzzy parameters. The following
parameters are supported:

fuzziness:      The fuzziness factor, defaults to AUTO. See Fuzziness edit for allowed
                settings.  The fuzziness parameter can be specified as:

                0, 1, 2
                    the maximum allowed Levenshtein Edit Distance (or number of edits) 
                AUTO
                    generates an edit distance based on the length of the term. For lengths:
                    0..2
                        must match exactly 
                    3..5
                        one edit allowed 
                    >5
                        two edits allowed 

                    AUTO should generally be the preferred value for fuzziness

transpositions: If set to true, transpositions are counted as one change instead of two,
                defaults to true

min_length:     Minimum length of the input before fuzzy suggestions are returned,
                defaults 3

prefix_length:  Minimum length of the input, which is not checked for fuzzy alternatives,
                defaults to 1

unicode_aware:  Sets all are measurements (like edit distance, transpositions and lengths)
                in unicode code points (actual letters) instead of bytes.

"""
import datetime
import logging
import pytz
import re
import collections

from airports.models import Airport
from cities.models import City
from elasticsearch_dsl import analyzer, token_filter, tokenizer
from django_elasticsearch_dsl import Document, Index, fields
from django_elasticsearch_dsl.registries import registry
from django.conf import settings
from django.utils.six import iteritems

from .models import Location, Rideshare

log = logging.getLogger(__name__)

### Custom analyzer for Location, City and Airport names ###

# We don't want to split O'Brian or Toulouse-Lautrec, but because
# the lines are formatted with ',' separating city/region/country
# it may work better like this.
comma_tokenizer = tokenizer( 
    'comma_tokenizer',
    type='char_group',
    tokenize_on_chars=[
        'whitespace',
        ',',
        '\n'
    ]
)

# I assume ascii folding will be okay for users of foreign languages.
ascii_fold = analyzer(
    'ascii_fold',
    tokenizer=comma_tokenizer,
    filter=[
        'lowercase',
        token_filter('ascii_fold', 'asciifolding')
    ]
)

### Custom analyzers for Rideshare completion match ###

# HTML strip anaylyzer was in examples, probably not necessary
# since we're not expecting users to enter html in search field
# but won't hurt.
html_strip = analyzer(
    'html_strip',
    tokenizer='standard',
    filter=['standard', 'lowercase', 'stop', 'snowball'],
    char_filter=['html_strip']
)

# EdgeNgram filter for RideshareDocument typeahead
edge_ngram_completion_filter = token_filter(
    'edge_ngram_completion_filter',
    type="edge_ngram",
    min_gram=1,
    max_gram=12,
    token_chars=[
        'letter',
        'digit',
    ],
)

# EdgeNgram analyzer for RideshareDocument typeahead
edge_ngram_completion = analyzer(
    "edge_ngram_completion",
    tokenizer="standard",
    filter=["lowercase", edge_ngram_completion_filter]
)


class CompletionSuggesterMixin(object):
    # Use these as defaults, will be merged/overwritten if fuzzy arg is supplied.
    fuzzy = {'min_length':3, 'prefix_length':1, 'unicode_aware':True}

    @classmethod
    def suggestions(cls, text, fuzzy={}, size=10,
        country_ctx=None, continent_ctx=None, location_type_ctx=None):

        fuzzy = { **cls.fuzzy, **fuzzy }
        contexts = {}

        def is_list(obj): return not isinstance(obj, str) and isinstance(obj, collections.Iterable)

        if country_ctx:
            arglist = country_ctx if is_list(country_ctx) else [country_ctx]
            contexts.update(dict(countrycontext=arglist))

        if continent_ctx:
            arglist = continent_ctx if is_list(continent_ctx) else [continent_ctx]
            assert set(arglist) <= set(['AF', 'NA', 'OC', 'AN', 'AS', 'EU', 'SA',])
            contexts.update(dict(continentcontext=arglist))

        # Hack to load the obj type and id; it's either an airport or a city
        if location_type_ctx and cls.__name__ == 'LocationDocument':
            arglist = location_type_ctx if is_list(location_type_ctx) else [location_type_ctx]
            assert set(arglist) <= set(['airport', 'city'])
            contexts.update(dict(locationtype=arglist))

        completion={
            'field':'name_suggest',
            'fuzzy':fuzzy,
            'size':size,
            'contexts':contexts,
        }

        search = cls.search().suggest('name_suggestions', text, completion=completion)
        sources = ['suggest', 'id', 'name_composite']
        if cls.__name__ == 'LocationDocument':
            sources.append('loc_type')
            sources.append('loc_obj_id')
        search = search.source(sources)

        #import json
        #log.info('SEARCH: {}'.format(json.dumps(search.to_dict(), indent=4)))
        #print('SEARCH: {}'.format(json.dumps(search.to_dict(), indent=4)))
        response = search.execute()
        #print('RESPONSE: {}'.format(response.suggest.name_suggestions[0].options))
        return response.suggest.name_suggestions[0].options


@registry.register_document
class LocationDocument(CompletionSuggesterMixin, Document):
    class Index:
        name = 'locations' + ('-test' if settings.DEBUG else '')
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 0}

    loc_obj_id = fields.IntegerField()
    loc_type = fields.KeywordField()
    name_composite = fields.KeywordField()
    name_suggest = fields.CompletionField(analyzer=ascii_fold,
                   contexts=[
                       { "name": "locationtype", "type": "category" },
                       { "name": "countrycontext", "type": "category" },
                       { "name": "continentcontext", "type": "category" },
                   ]
    )

    def prepare_loc_type(self, instance):
        return instance.location_type.lower()

    def prepare_loc_obj_id(self, instance):
        return instance.obj_id

    def prepare_name_composite(self, instance):
        return instance.output()

    def prepare_name_suggest(self, instance):
        loc = instance.city if instance.city else instance.airport
        continent_code = loc.country.continent.code

        location_type = instance.location_type.lower()

        ret = {
            'input': instance.input,
            'weight': instance.weight,
            'contexts': {
                'countrycontext': [instance.country],
                'continentcontext': [continent_code],
                'locationtype': [location_type],
            }
        }
        return ret

    class Django:
        model = Location # The model associated with this Document

        # The fields of the model indexed in Elasticsearch
        fields = [
            'name',
            'region',
            'country',
        ]

    def get_queryset(self):
        return super(LocationDocument, self).get_queryset().filter()


@registry.register_document
class CityDocument(CompletionSuggesterMixin, Document):
    class Index:
        name = 'cities'
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 0}

    name_composite = fields.KeywordField()
    name_suggest = fields.CompletionField(analyzer=ascii_fold,
                   contexts=[
                       { "name": "countrycontext", "type": "category" },
                       { "name": "continentcontext", "type": "category" },
                   ]
    )

    name = fields.StringField(attr='name_std')
    region = fields.StringField(attr='region.name_std')
    country = fields.StringField(attr='country.name')
    continent_code = fields.StringField(attr='country.continent.code')
    population = fields.IntegerField()
    #location = fields.GeoPointField()

    def _get_name_parts(self, instance):
        region_name = getattr(instance.region, 'name_std') if instance.region else ''
        country_code = getattr(instance.country, 'code') if instance.country else ''
        return region_name, country_code

    def prepare_name_suggest(self, instance):
        name = getattr(instance, 'name_std', '')
        region_name, country_code = self._get_name_parts(instance)
        # Cities get weighted by population, so larger ones appear first.  Would it be
        # better to rank them by population tiers?
        ret = {
            'input': [self.prepare_name_composite(instance)],
            'weight': instance.population,
            'contexts': {
                'countrycontext': [country_code],
                'continentcontext': [instance.country.continent.code]
            }
        }
        return ret

    def prepare_name_composite(self, instance):
        name = getattr(instance, 'name_std')
        region_name, country_code = self._get_name_parts(instance)
        identifier_list = [x for x in [name, region_name, country_code] if x]
        output_str = ', '.join(identifier_list)
        return output_str

    class Django:
        model = City

    def get_queryset(self):
        return super(CityDocument, self).get_queryset().select_related('region', 'country')


@registry.register_document
class AirportDocument(CompletionSuggesterMixin, Document):
    class Index:
        name = 'airports'
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 0}

    name_composite = fields.KeywordField()
    name_suggest = fields.CompletionField(analyzer=ascii_fold,
                   contexts=[
                       { "name": "countrycontext", "type": "category" },
                       { "name": "continentcontext", "type": "category" },
                   ]
    )
    name = fields.StringField(attr='name')
    city = fields.StringField(attr='city_name')
    region = fields.StringField(attr='region.name_std')
    country = fields.StringField(attr='country.name')
    continent_code = fields.StringField(attr='country.continent.code')

    def _get_name_parts(self, instance):
        region_name = getattr(instance.region, 'name_std') if instance.region else ''
        country_code = getattr(instance.country, 'code') if instance.country else ''
        airport_codes = sorted(list(set([c for c in 
            [instance.iata, instance.icao, instance.local, instance.ident] if c])))
        continent_code = getattr(instance.country.continent, 'code') if instance.country else ''
        return region_name, country_code, airport_codes, continent_code

    def prepare_name_composite(self, instance):
        name = getattr(instance, 'name', '')

        # No need to repeat the city name if, as is often the case, it is
        # mentioned in the name of the airport.
        try:
            if instance.city_name:
                city_name = instance.city_name
            elif instance.city:
                city_name = instance.city.name_std
            else:
                city_name = ''
            if city_name in name:
                city_name = ''
        except AttributeError as e:
            log.error('{} {} {}'.format(instance.id, instance.name, e))
            city_name = ''

        region_name, country_code, airport_codes, continent_code = self._get_name_parts(instance)

        identifier_list = [x for x in [name, city_name, region_name, country_code] if x]
        output_str = '{} ({})'.format(', '.join(identifier_list), ', '.join(airport_codes))
        return output_str

    def prepare_name_suggest(self, instance):
        name = getattr(instance, 'name', '')
        city_name = getattr(instance, 'city_name', '')
        if not city_name:
            try:
                city_name = instance.city.name_std
            except AttributeError:
                city_name = ''
        region_name, country_code, airport_codes, continent_code = self._get_name_parts(instance)

        inputs = [city_name, self.prepare_name_composite(instance)]
        # Tack on the airport codes. These are very inconsistent, so we'll use
        # what we've got.
        inputs += airport_codes
        ret = {
            'input': inputs,
            'weight': 1,
            'contexts': {
                'countrycontext': country_code,
                'continentcontext': continent_code
            }
        }
        return ret

    class Django:
        model = Airport

    def get_queryset(self):
        exclude_types = ['heliport','closed']
        log.info('Excluding airport types: {}'.format(exclude_types))
        return super(AirportDocument, self).get_queryset().select_related(
                'city', 'region', 'country__continent').exclude(
                        type__in=exclude_types)


@registry.register_document
class RideshareDocument(Document):
    class Index:
        name = 'rideshares' + ('-test' if settings.DEBUG else '')
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 0}

    name_composite = fields.KeywordField()
    event_slug = fields.KeywordField(attr='event.slug')
    flight = fields.BooleanField()

    # Index name and description for autocomplete
    name_description = fields.StringField(
        analyzer=html_strip,
        fields={
            'raw': fields.KeywordField(),
            'suggest': fields.CompletionField(),
            'autocomplete': fields.StringField(
                analyzer=edge_ngram_completion,
                search_analyzer='standard',
            ),
        }
    )

    @classmethod
    def match(cls, q, size=10, event_slug=None, flight=None):
        query = { 'query':q, 'operator':'and' }
        sources = ['id', 'name_composite']
        rs = cls.search().query('match', name_description__autocomplete=query)
        rs = rs.extra(size=size)
        if event_slug:
            rs = rs.filter('term', event_slug=event_slug)
        if flight is not None:
            rs = rs.filter('term', flight=flight)

        rs = rs.source(sources)
        matches = rs.execute()
        return matches

    def prepare_name_description(self, instance):
        # Combine the name, description and contact_name, so a search
        # may match any of them.
        return (
            instance.name + '\n'
            + instance.description + '\n'
            + instance.contact_name)

    def prepare_name_composite(self, instance):
        # This is a composed field for display in pull-down
        return self.formatted_output(instance)

    class Django:
        model = Rideshare

        # The fields of the model indexed in Elasticsearch
        fields = [
            'name',
            'description',
        ]

    def formatted_output(self, instance):
        # Make a formatted field for display in pull-down
        try:
            # Note that the lines are also truncated in browser with CSS
            max_length = 90
            name_list = [instance.name]
            #log.debug('name_list: %s', name_list)
            if instance.contact_name:
                if instance.name.lower() != instance.contact_name.lower():
                    contact_name = '(%s)' % instance.contact_name
                    contact_name = re.sub(r'\s+', '&nbsp;', contact_name)
                    name_list.append(contact_name)
            if instance.description:
                # ndash is u2012
                name_list.append('‒')

                # +1 for ellipsis character, it's u2026
                desc_length = max_length - sum(map(len, name_list)) + 1

                description = instance.description
                desc = (description[:desc_length] + '…'
                        if len(description) > desc_length
                        else description)
                desc = re.sub(r'\s+', ' ', desc)
                name_list.append(desc)
            return ' '.join(name_list)

        except (AttributeError, UnicodeDecodeError) as e:
            raise

    def get_queryset(self):
        """ Only return Rideshares that are active, not deleted or expired.
            This only gets called when re-indexing.  Normally, Rideshares are
            indexed as they are created, and removed when they are deleted.
        """
        soon = datetime.datetime.now(tz=pytz.utc) \
            + datetime.timedelta(days=getattr(settings, 'EVENT_VIABILITY_EXTENSION', 5))

        return super(RideshareDocument, self).get_queryset().filter(
                    deleted=False,
                    active=True,
                    event__end_date__gt=soon)

