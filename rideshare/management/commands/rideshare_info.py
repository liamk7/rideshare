"""
rideshare_info.py

Print various stats regarding rideshare data.
Option to print "by region" stats to CSV file.

"""
import sys
import csv
import locale
from collections import defaultdict
from collections import OrderedDict

from django.db import connection
from django.db.models import Sum, Count, Max, Avg
from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist

from airports.models import Airport
from cities.models import City, Region, Subregion
from rideshare.models import Event, Region, RideType, Rideshare, Contact, Location

# setlocale so we can print with grouping
locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

def _commas(n):
    return n
    """ convenient formatting for thousands separator """
    try:
        return '{:,}'.format(n)
    except:
        return n


class Command(BaseCommand):
    """
    Output Rideshare offers/requests by region'
    """
    help = 'Output Rideshare offers/requests by region'

    def add_arguments(self, parser):
            parser.add_argument('event',
                action='store',
                help='The name of the event',
                )
            parser.add_argument('--output',
                action='store',
                dest='csvfile',
                default=None,
                help='CSV output file name',
                )
            

    def handle(self, *args, **kwargs):
        """
        Process the command.
        """
        csvfile = kwargs['csvfile']

        try:
            event = Event.objects.get(name__contains=kwargs['event'])
        except Event.DoesNotExist:
            raise CommandError('Please enter a valid year')

        t_req = RideType.objects.get(name='Request')
        t_offer = RideType.objects.get(name='Offer')

        region_requests = Location.objects.filter(
                rideshare__rtype=t_req,
                rideshare__event=event,
                ).annotate(
                    ride_requests=Sum('rideshare__riders'),
                ).values(
                    'name', 'ride_requests', 'region', 'country'
                )
        region_offers = Location.objects.filter(
                rideshare__rtype=t_offer,
                rideshare__event=event,
                ).annotate(
                    ride_offers=Sum('rideshare__riders'),
                ).values(
                    'name', 'ride_offers', 'region', 'country'
                )

        # Merge the ride offered & requested ValueQuerySets
        default_dict = defaultdict(dict)
        for r in list(region_requests) + list(region_offers):
            key = ', '.join([r.pop('name'), r.pop('region'), r.pop('country')])
            default_dict[key].update(r)

        # Sort them
        ordered_dict = OrderedDict(
                sorted(list(default_dict.items()), key=lambda t: t[0]))
        region_count = len(ordered_dict)

        # Print out the dictionary
        offer_sum = request_sum = 0
        for k, v in list(ordered_dict.items()):
            self.stdout.write('%-30s%s\n' % (k, v))
            offer_sum += v.get('ride_offers', 0)
            request_sum += v.get('ride_requests', 0)

        # Contacts
        contacts_base = Contact.objects.filter(rideshare__event=event)
        contacts_base = contacts_base.values('contact_email')
        contacts_base = contacts_base.order_by('contact_email')
        contacts_count = contacts_base.count()
        offers_contacted = contacts_base.filter(
                rideshare__rtype=t_offer).values(
                        'rideshare').distinct().count()

        contacts_annotated = contacts_base.annotate(Count('contact_email'))
        contacts_annotated_count = contacts_annotated.count()


        # Calculate Contact max and avg
        contacts_max = 0
        wordy_user = None
        for c in contacts_annotated:
            if c['contact_email__count'] > contacts_max:
                contacts_max = c['contact_email__count']
                wordy_user = c['contact_email']

        try:
            contacts_avg = "{:.1f}".format(contacts_count / contacts_annotated_count)
        except ZeroDivisionError:
            contacts_avg = 'NA'

        # Ride offers and requests -- totals
        ride_offers = Rideshare.objects.filter(rtype=t_offer, event=event)
        vehicle_count = ride_offers.count()
        total_rides_offered = ride_offers.aggregate(
                Sum('riders'))['riders__sum'] or 0
        ride_requests = Rideshare.objects.filter(rtype=t_req, event=event)
        ride_request_count = ride_requests.count()
        total_rides_requested = ride_requests.aggregate(
                Sum('riders'))['riders__sum'] or 0

        info_template = """
For {event_name}
====================
{region_count:n} Regions had ride offers or requests.
{vehicle_count:n} vehicles offered {total_rides_offered:n} rides.
{offers_contacted:n} ride offers received at least one response ({oc_pct:.0f}%).
{ride_request_count:n} riders requested {total_rides_requested:n} rides.
At least {participant_count:n} participants used Rideshare.
{contacts_annotated_count:n} users sent a total of {contacts_count:n} emails.
The most emails sent by a single user was {contacts_max:n} (by <{wordy_user}>).
The average number of emails sent was {contacts_avg}.
"""

        # Named arguments for the info template
        arg_map = {
            'event_name': event.name,
            'region_count': region_count,
            'vehicle_count': vehicle_count,
            'total_rides_offered': total_rides_offered,
            'ride_request_count': ride_request_count,
            'offers_contacted': offers_contacted,
            'oc_pct': offers_contacted / vehicle_count * 100.0,
            'total_rides_requested': total_rides_requested,
            'participant_count': vehicle_count + ride_request_count,
            'contacts_annotated_count': contacts_annotated_count,
            'contacts_count': contacts_count,
            'contacts_max': contacts_max,
            'wordy_user': wordy_user,
            'contacts_avg': contacts_avg,
        }

        self.stdout.write(info_template.format(**arg_map))

        # Output to csvfile
        if csvfile:
            csv_write = csv.writer(open(csvfile, 'w'))
            csv_write.writerow(['Region', 'Ride Requests', 'Ride Offers'])
            for k,v in list(ordered_dict.items()):
                csv_write.writerow([k,
                    v.get('ride_requests', None),
                    v.get('ride_offers', None),
                    ])

