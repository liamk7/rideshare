from airports.models import Airport
from cities.models import City, Region, Subregion
from django.core.management.base import BaseCommand, CommandError

from rideshare.models import Location


class Command(BaseCommand):
    """
    Add Location from city or airport.
    """
    help = "Add Location from City or Airport"

    def add_arguments(self, parser):
            parser.add_argument('location-type',
                action='store',
                choices=['airport', 'city'],
                help='This is type of location, either airport or city',
                )
            parser.add_argument('name',
                action='store',
                help='This is the name of the airport or city',
                )
            parser.add_argument('country',
                action='store',
                help='This is a country code, eg. US',
                )
            parser.add_argument('--region',
                action='store',
                dest='region',
                default='',
                help='This is a region code, eg. NV',
                )

    def handle(self, *args, **kwargs):
        """
        Process the command.
        """
        location_type = kwargs['location-type'].capitalize()
        name = kwargs['name']
        region = kwargs['region']
        country = kwargs['country']

        if location_type not in ('City', 'Airport'):
            raise CommandError('A city or airport and country code is required')
        cls = eval(location_type)
        print('Class:', cls)

        try:
            obj = cls.objects.get(name__iexact=name, country__code__iexact=country)
        except cls.DoesNotExist:
            raise CommandError('Cannot find {} "{}, {}"'.format(cls.__name__, name, country))
        except cls.MultipleObjectsReturned as e:
            # If multiple objects were found, print them in error message and exit
            if not region:
                objects = cls.objects.filter(name__iexact=name, country__code__iexact=country).order_by('region__code').values('region__code', 'region__name')
                for r in objects:
                    self.stdout.write(
                        self.style.ERROR('{} {}'.format(r['region__code'], r['region__name'])))

                raise CommandError('Multiple matches for "{}, {}", try adding region code with --region'.format(name, country))

            try:
                obj = cls.objects.get(name__iexact=name, country__code__iexact=country, region__code__iexact=region)
            except cls.DoesNotExist: 
                raise CommandError('Cannot find "{}, {}, {}"'.format(name, region, country))
            except cls.MultipleObjectsReturned as e:
                raise CommandError('Multiple matches for "{}, {}, {}"'.format(name, region, country))

        try:
            loc = Location.from_obj(obj)
            self.stdout.write(self.style.SUCCESS('{}, {}'.format(obj.name, obj.region)))
        except Exception as e:
            raise CommandError(e)


