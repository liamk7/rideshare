import sys
from datetime import datetime
from io import BytesIO
from json import dump
from os import path
from tarfile import TarInfo, TarFile
from optparse import make_option

from django.apps.registry import apps
from django.conf import settings
from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError
from django.db import models
from django.db.models import Q
from django.utils.encoding import smart_bytes
from django.utils import timezone

from rest_framework.renderers import JSONRenderer

from rideshare.models import Event, Rideshare
from api.serializers import RideshareArchiveSerializer


class MixedIO(BytesIO):
    """
    A BytesIO that accepts and encodes Unicode data.
    This class was born out of a need for a BytesIO that would accept writes of
    both bytes and Unicode data - allowing identical usage from both Python 2
    and Python 3.
    """

    def rewind(self):
        """
        Seeks to the beginning and returns the size.
        """
        size = self.tell()
        self.seek(0)
        return size

    def write(self, data):
        """
        Writes the provided data, converting Unicode to bytes as needed.
        """
        BytesIO.write(self, smart_bytes(data))


class Command(BaseCommand):
    """
    Delete Rideshare, Contact and Event objects
    """
    help = "Delete Rideshare, Contact and Event objects"

    option_list = BaseCommand.option_list + (
        make_option('--event',
            action='store',
            dest='event_slug',
            default=None,
            help='Event slug, e.g. burning-man-2015',
            ),
        make_option('--delete',
            action='store_true',
            dest='delete',
            default=None,
            help='Really delete event and all its rideshares and contacts --  Unrecoverable!',
            ),
        make_option('--force',
            action='store_true',
            dest='force',
            default=None,
            help='Force delete!',
            ),
        )

    def handle(self, *args, **kwargs):
        """
        Process the command.
        """
        args = '<event>'

        event_slug = kwargs['event_slug']
        really_delete = kwargs['delete']
        force = kwargs['force']

        try:
            assert event_slug
            events = Event.objects.filter(slug=event_slug)
            # normally, we don't want to delete an event that is in the future
            if not force:
                events = events.filter(end_date__lt=timezone.now())
            event = events[0]
        except AssertionError: # IndexError, ObjectDoesNotExist:
            raise CommandError('Please enter a valid event')
        except Event.DoesNotExist:
            raise CommandError("Invalid event slug '%s'" % event_slug)
        except IndexError:
            raise CommandError("Invalid event slug '%s'" % event_slug)

        # Rideshare.delete() takes care of deleting the Rideshare's user
        # if the Event's auth_type is 'playa'.
        # We delete them here one at a time, instead of deleting via the
        # queryset.delete() so we get that behavior.
        delete_rideshares = Rideshare.objects.filter(event=event)

        if really_delete:
            sys.stdout.write('Deleting %d Rideshares\n' % delete_rideshares.count())
            counter = 0
            for rs in delete_rideshares:
                # this also deletes the Rideshare's contacts
                if really_delete:
                    counter += 1
                    rs.delete()
                    if counter % 20 == 0:
                        sys.stdout.write('.')
                        sys.stdout.flush()
                    if counter % 1200 == 0:
                        sys.stdout.write('\n')
                        sys.stdout.flush()
            sys.stdout.write('\n')
            sys.stdout.flush()

            # Now we can delete the event
            if really_delete:
                event.delete()


        else:
            sys.stdout.write('%d Rideshares\n' % delete_rideshares.count())
            sys.stdout.write('You must also use the --delete flag\n')



