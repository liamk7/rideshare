import os, sys
PATH = os.path.join(os.path.dirname(__file__), '..')
from  django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
