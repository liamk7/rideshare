# Rideshare

Rideshare is an open-source Django project designed
to facilitate ride-sharing for events, allowing users
to connect with others traveling to and from the same destination.
Originally developed for the Burning Man event, Rideshare has since
evolved to support multiple events and regional gatherings.

This repository contains the backend application, which provides
an API for managing rides, user interactions, and event-specific
configuration. The API enables integration with front-end applications
handling authentication, messaging, notifications, and search functionality.

_Features_

* Multiple events
* Multiple Regional events
* Multi-lingual (currently supports en, fr, es)
* Support for events having event-specific airports
* Private inter-user messaging
* User notifications via Email and SMS
* Help facility (sends email to administrators)
* FAQ facility
* Search by geographical location and radius, search domain customizable by country list
* Support for typeahead selection of objects via AJAX
* CORS support


The complementary frontend is [rideshare frontend](https://gitlab.com/liamk7/rideshare-frontend).

This package also provides some simple capability for reporting on usage with pretty D3 charts.

This package provides an admin interface used for adding events and other tasks.

~~View in production at [Burning Man Rideshare](https://rideshare.burningman.org).~~

## Installation

#### Requirements

* PostgreSQL 9.x with PostGIS extension (currently using AWS RDS)
* Python 3.4-3.6
* Virtualenvwrapper
* Elasticsearch 6 (currently using AWS Elasticsearch)

#### Dependencies

* Twilio — for SMS notifications
* Amazon AWS S3 — for storage of uploaded images
* Mandrill — for email delivery

Go to [Detailed Installation Instructions](docs/INSTALLATION.md).

## API

The API uses the Django Rest Framework.  It is searchable and browsable, and self-documenting;
you can access its documentation via the Django Admin interface.
> **NOTE**: We may be adding [GraphQL](https://graphql.org/), stay tuned.

_Endpoints_
* /api/autoairports/
* /api/autocities/
* /api/autorideshares/
* /api/airports/
* /api/cities/
* /api/contacts/
* /api/events/
* /api/locations/
* /api/regionals/
* /api/preferences/
* /api/faqs/
* /api/rideshares/
* /api/types/

The autoairports, autocities and autorideshares endpoints are the ones used for typeaheads,
and use Elasticsearch on the backend.

## Administration

Go to [Administration](docs/ADMINISTRATION.md) for information about administering
Rideshare.

## Contributing

See the [Contributing](docs/CONTRIBUTING.md) document for details.

## Roadmap

See the [Roadmap](docs/ROADMAP.md) document for details.

## Versioning

This project follows [Semantic Versioning 2.0.0](http://semver.org/spec/v2.0.0.html).

## License

This project follows the Apache 2.0 license. See the [LICENSE](./LICENSE.md) file for details.
